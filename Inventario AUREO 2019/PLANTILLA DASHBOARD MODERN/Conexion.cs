﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using PLANTILLA_DASHBOARD_MODERN.Properties;
namespace PLANTILLA_DASHBOARD_MODERN
{
    class Conexion
    {
        public static string obtenerdireccion()
        {
            return Settings.Default.ProyectosMultiplesConnectionString;//llamar al app config para obtener direccion
        }
        string direccion = obtenerdireccion();
        public SqlConnection conectar = new SqlConnection();//objeto para manipular la base de datos
        public SqlCommand cmd;//comandos de sql

        public Conexion()
        {
            conectar.ConnectionString = direccion;
        }

        // Funcion para ingresar Proveedores
        public void ingresarProveedores(string id, string nit, string empresa, string telefono, string contacto, string direccion, string email)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO PROVEEDORESPM (id_proveedorespm, nit_proveedorespm, nombre_proveedorespm, telefono_proveedorespm, contacto_proveedorespm, direccion_proveedorespm, email_proveedorespm) ";
                string ingresar2 = "VALUES (@id, @nit, @nombre, @telefono, @contacto, @direccion, @email)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nit", nit);
                cmd.Parameters.AddWithValue("@nombre", empresa);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@contacto", contacto);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar BODEGAS
        public void ingresarBodegas(string id, string nombre)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO BODEGASPM (id_bodegaspm, nombre_bodegaspm) ";
                string ingresar2 = "VALUES (@id, @nombre)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar Producto en BODEGAS
        public void ingresarProductoBodegas(string idBodega, string cantidad, string idInventario)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO PROBODEGASPM (BODEGASPM_id_bodegaspm, cantidad_probodegaspm, id_inventario_probodegaspm) ";
                string ingresar2 = "VALUES (@idBodega, @cantidad, @idInventario)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@idBodega", idBodega);
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.Parameters.AddWithValue("@idInventario", idInventario);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar Reportes de Entradas
        public void ingresarReportesEntradas(string id, string total,string pago, string restante, string formasp, string fecha, string idPro,string fechaalerta)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO ENTRADASPM (Id_entradaspm, total_entradaspm, pago_entradaspm, restante_entradaspm, formaspago_entradaspm, fecha_entradaspm, PROVEEDORESPM_Id_proveedorespm, fechaalerta_entradaspm) ";
                string ingresar2 = "VALUES (@id, @total, @pago, @restante ,@formasp, @fecha, @idPro, @fechaalerta)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@total", total);
                cmd.Parameters.AddWithValue("@pago", pago);
                cmd.Parameters.AddWithValue("@restante", restante);
                cmd.Parameters.AddWithValue("@formasp", formasp);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.Parameters.AddWithValue("@idPro", idPro);
                cmd.Parameters.AddWithValue("@fechaalerta",fechaalerta);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        // Funcion para ingresar la cantidad de producto en las ENTRADAS
        public void ingresarReportesEntradasProducto(string idEntra, string codigo, string cantidad, string descripcion, string categorias, string marca, string precioR)
        {
            try
            {
                //AQUI ES EL ERROR
                conectar.Open();
                string ingresar1 = "INSERT INTO PRODUCTOPME (ENTRADASPM_Id_entradaspm, codigo_productopme, cantidad_productopme, descripcion_productopme, categorias_productopme, marca_productopme, precioR_productopme) ";
                string ingresar2 = "VALUES (@idEntra, @codigo, @cantidad, @descripcion, @categoria, @marca, @precioR)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@idEntra", idEntra);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.Parameters.AddWithValue("@descripcion", descripcion);
                cmd.Parameters.AddWithValue("@categoria", categorias);
                cmd.Parameters.AddWithValue("@marca", marca);
                cmd.Parameters.AddWithValue("@precioR", precioR);

                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar Reportes de Salidas
        public void ingresarReportesSalidas(string id, string total, string debito, string ganancia, string fecha, string idCli)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO SALIDASPM (Id_salidaspm, total_salidaspm, debito_salidaspm, ganancia_salidaspm, fecha_salidaspm, CLIENTESPM_Id_clientespm) ";
                string ingresar2 = "VALUES (@id, @total, @debito, @ganancia, @fecha, @idCli)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@total", total);
                cmd.Parameters.AddWithValue("@debito", debito);
                cmd.Parameters.AddWithValue("@ganancia", ganancia);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.Parameters.AddWithValue("@idCli", idCli);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar Salidas Anuladas
        public void ingresarSalidasAnuladas(string id, string cliente, string empresa, string total, string fecha)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO ANULADASPM (id_anuladaspm, cliente_anuladaspm, empresa_anuladaspm, total_anuladaspm, fecha_anuladaspm) ";
                string ingresar2 = "VALUES (@id, @cliente, @empresa, @total, @fecha)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@cliente", cliente);
                cmd.Parameters.AddWithValue("@empresa", empresa);
                cmd.Parameters.AddWithValue("@total", total);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar Modificaciones del Inventario
        public void ingresarModificacionesInventario(string id, string codigo, string descripcion, double existencia, double costo, double venta1, double venta2, double venta3, string observacion, string fecha)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO MODIFICARPM (id_modificarpm, codigo_modificarpm, descripcion_modificarpm, existencia_modificarpm, costo_modificarpm, venta1_modificarpm, venta2_modificarpm, venta3_modificarpm, observacion_modificarpm, fecha_modificarpm) ";
                string ingresar2 = "VALUES (@id, @codigo, @descripcion, @existencia, @costo, @venta1, @venta2, @venta3, @observacion, @fecha)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                cmd.Parameters.AddWithValue("@descripcion", descripcion);
                cmd.Parameters.AddWithValue("@existencia", existencia);
                cmd.Parameters.AddWithValue("@costo", costo);
                cmd.Parameters.AddWithValue("@venta1", venta1);
                cmd.Parameters.AddWithValue("@venta2", venta2);
                cmd.Parameters.AddWithValue("@venta3", venta3);
                cmd.Parameters.AddWithValue("@observacion", observacion);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar la cantidad de producto en las SALIDAS
        public void ingresarReportesSalidasProducto(string idSalida, string codigo, string cantidad, string descripcion, string categorias, string marca, string precio)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO PRODUCTOPMS (SALIDASPM_Id_salidaspm, codigo_productopms, cantidad_productopms, descripcion_productopms, categorias_productopms, marca_productopms, precio_productopms) ";
                string ingresar2 = "VALUES (@idSalida, @codigo, @cantidad, @descripcion, @categorias, @marca, @precio)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@idSalida", idSalida);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.Parameters.AddWithValue("@descripcion", descripcion);
                cmd.Parameters.AddWithValue("@categorias", categorias);
                cmd.Parameters.AddWithValue("@marca", marca);
                cmd.Parameters.AddWithValue("@precio", precio);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar los pagos a la Tabla ABONAR
        public void ingresarAbonosProveedores(string idCompras, string proveedor, string abonar, string referencia, string fecha)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO ABONARPM (idcompras_abonopm, proveedor_abonopm, abono_abonopm, referencia_abonopm, fecha_abonopm) ";
                string ingresar2 = "VALUES (@idCompras, @proveedor, @abonar, @referencia, @fecha)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@idCompras", idCompras);
                cmd.Parameters.AddWithValue("@proveedor", proveedor);
                cmd.Parameters.AddWithValue("@abonar", abonar);
                cmd.Parameters.AddWithValue("@referencia", referencia);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para ingresar Producto de los proveedores
        public void ingresarProductoE(string id, string codigo, string barras, string descripcion, string categorias, string marca, int existencia, double precioR, double precioV, double precioV1, double precioV2)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO INVENTARIOPM (Id_inventariopm, codigo_inventariopm, barras_inventariopm, descripcion_inventariopm, categorias_inventariopm, marca_inventariopm, existencia_inventariopm, precioR_inventariopm, precioV_inventariopm, precioV1_inventariopm, precioV2_inventariopm) ";
                string ingresar2 = "VALUES (@id, @codigo, @barras, @descripcion, @categorias, @marca, @existencia, @precioR, @precioV, @precioV1, @precioV2)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                cmd.Parameters.AddWithValue("@barras", barras);
                cmd.Parameters.AddWithValue("@descripcion", descripcion);
                cmd.Parameters.AddWithValue("@categorias", categorias);
                cmd.Parameters.AddWithValue("@marca", marca);
                cmd.Parameters.AddWithValue("@existencia", existencia);
                cmd.Parameters.AddWithValue("@precioR", precioR);
                cmd.Parameters.AddWithValue("@precioV", precioV);
                cmd.Parameters.AddWithValue("@precioV1", precioV1);
                cmd.Parameters.AddWithValue("@precioV2", precioV2);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Actualizar el DataGridView de proveedores que recibe
        public void actualizarProveedores(DataGridView TablaC)
        {
            string actualizar1 = "SELECT id_proveedorespm AS 'No.',  nombre_proveedorespm AS 'Empresa', nit_proveedorespm AS 'Nit', telefono_proveedorespm AS 'Teléfono', contacto_proveedorespm AS 'Contacto', direccion_proveedorespm AS 'Dirección', email_proveedorespm AS 'Email' FROM PROVEEDORESPM ";
            string actualizar2 = "ORDER By id_proveedorespm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 180;
            TablaC.Columns[2].Width = 90;
            TablaC.Columns[3].Width = 90;
            TablaC.Columns[4].Width = 150;
            TablaC.Columns[5].Width = 150;
            TablaC.Columns[6].Width = 200;
            conectar.Close();
        }

        // Funcion para modificar Proveedor
        public void modificarProveedores(string id, string nit, string empresa, string telefono, string contacto, string direccion, string email)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE PROVEEDORESPM SET nit_proveedorespm = @nit, nombre_proveedorespm = @empresa, telefono_proveedorespm = @telefono, contacto_proveedorespm = @contacto, direccion_proveedorespm = @direccion, email_proveedorespm = @email ";
                string modificar2 = "WHERE id_proveedorespm =@id";
                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nit", nit);
                cmd.Parameters.AddWithValue("@empresa", empresa);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@contacto", contacto);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar Bodegas
        public void modificarBodegas(string id, string nombre)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE BODEGASPM SET nombre_bodegaspm = @nombre ";
                string modificar2 = "WHERE id_bodegaspm =@id";
                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar la cantidad de producto en Bodegas
        public void modificarProductoBodegas(string id, string cantidad)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE PROBODEGASPM SET cantidad_probodegaspm = @cantidad ";
                string modificar2 = "WHERE id_inventario_probodegaspm =@id";
                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar Producto de ENTRADAS
        public void modificarProductoE(string id, string codigo, string barras, string descripcion, int existencia, string categoria, string marca, double precioR, double precioV, double precioV1, double precioV2)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE INVENTARIOPM SET codigo_inventariopm = @codigo, barras_inventariopm = @barras, descripcion_inventariopm = @descripcion, existencia_inventariopm = @existencia, categorias_inventariopm = @categoria, marca_inventariopm = @marca, precioR_inventariopm = @precioR, precioV_inventariopm = @precioV, precioV1_inventariopm = @precioV1, precioV2_inventariopm = @precioV2 ";
                string modificar2 = "WHERE Id_inventariopm = @id";
                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                cmd.Parameters.AddWithValue("@barras", barras);
                cmd.Parameters.AddWithValue("@descripcion", descripcion);
                cmd.Parameters.AddWithValue("@existencia", existencia);
                cmd.Parameters.AddWithValue("@categoria", categoria);
                cmd.Parameters.AddWithValue("@marca", marca);
                cmd.Parameters.AddWithValue("@precioR", precioR);
                cmd.Parameters.AddWithValue("@precioV", precioV);
                cmd.Parameters.AddWithValue("@precioV1", precioV1);
                cmd.Parameters.AddWithValue("@precioV2", precioV2);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar el abonar de las ENTRADAS que se debe a PROVEEDORES
        public void modificarAbonarProveedores(string id, string abonar, string restante)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE ENTRADASPM SET pago_entradaspm = @abonar, restante_entradaspm = @restante ";
                string modificar2 = "WHERE Id_entradaspm = @id";
                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@abonar", abonar);
                cmd.Parameters.AddWithValue("@restante", restante);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Pago Realizado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar la fecha de alerta al POSTERGAR 
        public void modificarfechaProveedores(string id, DateTime fechaalerta)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE ENTRADASPM SET fechaalerta_entradaspm=@fechaalerta ";
                string modificar2 = "WHERE Id_entradaspm = @id";
                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@fechaalerta", fechaalerta);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Alerta postergada!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar el abonar de las SALIDAS que deben los Proveedores
        public void modificarAbonarClientes(string id, string abonar)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE SALIDASPM SET debito_salidaspm = @abonar ";
                string modificar2 = "WHERE Id_salidaspm = @id";
                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@abonar", abonar);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Pago Realizado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar Producto de ENTRADAS
        public void modificarProductoCantidad(string id, int existencia)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE INVENTARIOPM SET existencia_inventariopm = @existencia ";
                string modificar2 = "WHERE Id_inventariopm =@id";

                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@existencia", existencia);
                cmd.ExecuteNonQuery();
             //   FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
             //   if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        // FUNCION PARA ELIMINAR PROVEEDORES
        public void eliminarProveedores(string id)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM PROVEEDORESPM WHERE id_proveedorespm = @id";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Eliminado con éxito!", "2. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA ELIMINAR Bodegas
        public void eliminarBodegas(string id)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM BODEGASPM WHERE id_bodegaspm = @id";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Eliminado con éxito!", "2. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        

        // FUNCION PARA ELIMINAR el producto de las Bodegas
        public void eliminarBodegasProducto(string idBod, string idPro)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM PROBODEGASPM WHERE BODEGASPM_id_bodegaspm = @idBod AND id_inventario_probodegaspm = @idPro";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@idBod", idBod);
                cmd.Parameters.AddWithValue("@idPro", idPro);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA ELIMINAR CLIENTES
        public void eliminarClientes(string id)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM CLIENTESPM WHERE id_clientespm = @id";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Eliminado con éxito!", "2. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA ELIMINAR PRODUCTOS DE ENTRADAS 
        public void eliminarProductosE(string id)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM INVENTARIOPM WHERE Id_inventariopm = @id";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Eliminado con éxito!", "2. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA ELIMINAR ENTRADAS Y QUITAR LA CORRELACION 
        public void eliminarEntradas(string idInventario)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM ENTRADASPM WHERE INVENTARIOPM_Id_inventariopm = @id";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@id", idInventario);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA ELIMINAR SALIDASPM Y QUITAR LA CORRELACION 
        public void eliminarPMSalidas(string idSalidas)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM PRODUCTOPMS WHERE SALIDASPM_Id_salidaspm = @id";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@id", idSalidas);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA ELIMINAR SALIDAS Y QUITAR LA CORRELACION 
        public void eliminarSalidas(string idSalidas)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM SALIDASPM WHERE Id_salidaspm = @id";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@id", idSalidas);
                cmd.ExecuteNonQuery();
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        //FUNCION PARA ACTUALIZAR USUARIOS PRINCIPALES EN DGVUSUARIOS
        public void actualizarUsuarios(DataGridView TablaC)
        {
            string actualizar = "SELECT Id_usuariospm AS 'No.', nombre_usuariospm AS 'Nombre', dpi_usuariospm AS 'DPI', telefono_usuariospm AS 'Teléfono', direccion_usuariospm AS 'Dirección', email_usuariospm AS 'Email', permiso_usuariospm AS 'Permiso', usuario_usuariospm AS 'Usuario', contra_usuariospm AS 'PASSWORD' FROM USUARIOSPM ORDER By Id_usuariospm";
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 100;
            TablaC.Columns[1].Width = 100;
            TablaC.Columns[2].Width = 100;
            TablaC.Columns[3].Width = 100;
            TablaC.Columns[4].Width = 100;
            TablaC.Columns[5].Width = 100;
            TablaC.Columns[6].Width = 100;
            TablaC.Columns[7].Width = 100;
            TablaC.Columns[8].Width = 100;
            conectar.Close();
        }

        //FUNCION PARA ACTUALIZAR ABONOS DE LA TABLA ABONAR
        public void actualizarAbonosProveedores(DataGridView TablaC, string idReporte)
        {
            string actualizar1 = "SELECT proveedor_abonopm AS 'Proveedor', abono_abonopm AS 'Pagó', referencia_abonopm AS 'Referencia', fecha_abonopm AS 'Fecha' ";
            string actualizar2 = "FROM ABONARPM WHERE idcompras_abonopm = "+ idReporte +" ORDER By fecha_abonopm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 200;
            TablaC.Columns[2].Width = 70;
            TablaC.Columns[3].Width = 70;
            TablaC.Columns[4].Width = 200;
            int i = 0; 
            foreach(DataGridViewRow fila in TablaC.Rows)
            {
                i++;
                fila.Cells[0].Value = i;
            }
            conectar.Close();
        }

        //FUNCION PARA ACTUALIZAR ENTRADAS DE PRODCUTO
        public void actualizarProductoE(DataGridView TablaC)
        {
            string actualizar1 = "SELECT Id_inventariopm AS 'No.', codigo_inventariopm AS 'Código', barras_inventariopm AS 'Código de Barras', descripcion_inventariopm AS 'Descripción', existencia_inventariopm AS 'Existencia', categorias_inventariopm AS 'Categoría', marca_inventariopm AS 'Marca', precioR_inventariopm AS 'Costo', precioV_inventariopm AS 'Precio Venta', precioV1_inventariopm AS 'Precio Venta 1', precioV2_inventariopm AS 'Precio Venta 2' ";
            string actualizar2 = "FROM INVENTARIOPM ORDER By Id_inventariopm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 30; // ID
            TablaC.Columns[1].Width = 70; // Codigo
            TablaC.Columns[2].Width = 120; // Codigo de Barras
            TablaC.Columns[3].Width = 280; // Descripcion
            TablaC.Columns[4].Width = 75; // Existencia
            TablaC.Columns[5].Width = 100; // Categoria
            TablaC.Columns[6].Width = 100; // Marca
            TablaC.Columns[7].Width = 100; // Precio Real
            TablaC.Columns[8].Width = 120;  // Precio V
            TablaC.Columns[9].Width = 120;// Precio V1
            TablaC.Columns[10].Width = 120;// Precio V2
            conectar.Close();
        }

        // Funcion para actualizar el numero de reportes que existen de Entradas
        public void actualizarNoReportesEn(DataGridView TablaC)
        {
            string actualizar1 = "SELECT Id_entradaspm AS 'No.' ";
            string actualizar2 = "FROM ENTRADASPM ORDER By Id_entradaspm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            conectar.Close();
        }

        // Funcion para actualizar las bodegas
        public void actualizarBodegas(DataGridView TablaC)
        {
            string actualizar1 = "SELECT id_bodegaspm AS 'No.', nombre_bodegaspm AS 'Nombre' ";
            string actualizar2 = "FROM BODEGASPM ORDER By id_bodegaspm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 70;
            TablaC.Columns[1].Width = 340;
            conectar.Close();
        }


        // Funcion para actualizar las producto de bodegas
        public void actualizarProductoBodegas(DataGridView TablaC, string idBodega)
        {
            string actualizar1 = "SELECT BODEGASPM_id_bodegaspm AS 'No.', cantidad_probodegaspm AS 'Cantidad', id_inventario_probodegaspm AS 'Producto' ";
            string actualizar2 = "FROM PROBODEGASPM WHERE BODEGASPM_id_bodegaspm = "+ idBodega +" ORDER By BODEGASPM_id_bodegaspm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 70;
            TablaC.Columns[1].Width = 70;
            TablaC.Columns[2].Width = 70;
            conectar.Close();
        }

        // Funcion para Busqueda directa de los producto de bodegas
        public void busquedaBodegasId(DataGridView Tabla, string idProducto, string cantidad)
        {
            try
            {
                conectar.Open();
                string buscar = "SELECT *FROM INVENTARIOPM ";
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    if (lector.GetValue(0).ToString() == idProducto)
                    {
                        Tabla.Rows.Add(generarN(Tabla), lector.GetValue(1).ToString(), lector.GetValue(3).ToString(), cantidad,  lector.GetValue(6).ToString(), lector.GetValue(7).ToString(), idProducto);
                    }
                }
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        // Funcion para actualizar el numero de reportes que existen de Salidas
        public void actualizarNoReportesSal(DataGridView TablaC)
        {
            string actualizar1 = "SELECT Id_salidaspm AS 'No.' ";
            string actualizar2 = "FROM SALIDASPM ORDER By Id_salidaspm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            conectar.Close();
        }

        // Funcion para actualizar los reportes que existen de Salidas
        public void actualizarReportesSalidas(DataGridView TablaC, string fecha1, string fecha2, TextBox texto)
        {
            string actualizar1 = "SELECT Id_salidaspm AS 'No.', nombre_clientespm AS 'Contacto', apellido_clientespm AS 'Empresa',  telefono_clientespm AS 'Teléfono', total_salidaspm AS 'Total', debito_salidaspm AS 'Débito', ganancia_salidaspm AS 'Ganancia',  fecha_salidaspm AS 'Fecha' ";
            string actualizar2 = "FROM SALIDASPM INNER JOIN CLIENTESPM ON CLIENTESPM_Id_clientespm = id_clientespm ";
            string actualizar3 = "WHERE fecha_salidaspm >= '"+fecha1+ " 00:00' AND fecha_salidaspm <= '"+fecha2+" 23:59' ";
            string buscar1 = "AND nombre_clientespm like('" + texto.Text + "%') ORDER By Id_salidaspm";

            string actualizar = actualizar1 + actualizar2 + actualizar3 + buscar1;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 80;
            TablaC.Columns[4].Width = 100;
            TablaC.Columns[5].Width = 100;
            TablaC.Columns[6].Width = 100;
            TablaC.Columns[7].Width = 150;

            conectar.Close();
        }

        // Funcion para actualizar los reportes que existen de Ventas Anuladas
        public void actualizarVentasAnuladas(DataGridView TablaC, string fecha1, string fecha2, TextBox texto)
        {
            string actualizar1 = "SELECT id_anuladaspm AS 'No.', cliente_anuladaspm AS 'Contacto', empresa_anuladaspm AS 'Empresa',  total_anuladaspm AS 'Total', fecha_anuladaspm AS 'Fecha' ";
            string actualizar2 = "FROM ANULADASPM ";
            string actualizar3 = "WHERE fecha_anuladaspm >= '" + fecha1 + " 00:00' AND fecha_anuladaspm <= '" + fecha2 + " 23:59' ";
            string buscar1 = "AND cliente_anuladaspm like('" + texto.Text + "%') ORDER By id_anuladaspm";

            string actualizar = actualizar1 + actualizar2 + actualizar3 + buscar1;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 200;
            TablaC.Columns[2].Width = 200;
            TablaC.Columns[3].Width = 150; // Total
            TablaC.Columns[4].Width = 200;
            conectar.Close();
        }

        // Funcion para actualizar las modificaciones que se realizaron en el inventario
        public void actualizarProductoModificado(DataGridView TablaC, string fecha1, string fecha2, TextBox texto)
        {
            string actualizar1 = "SELECT id_modificarpm AS 'Id', codigo_modificarpm AS 'Código', descripcion_modificarpm AS 'Descripción',  existencia_modificarpm AS 'Existencia', costo_modificarpm AS 'Costo', venta1_modificarpm AS 'Precio Mínimo', venta2_modificarpm AS 'Precio Medio', venta3_modificarpm AS 'Precio Máximo', observacion_modificarpm AS 'Observación', fecha_modificarpm AS 'Fecha' ";
            string actualizar2 = "FROM MODIFICARPM ";
            string actualizar3 = "WHERE fecha_modificarpm >= '" + fecha1 + " 00:00' AND fecha_modificarpm <= '" + fecha2 + " 23:59' ";
            string buscar1 = "AND descripcion_modificarpm like('" + texto.Text + "%') ORDER By fecha_modificarpm";

            string actualizar = actualizar1 + actualizar2 + actualizar3 + buscar1;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 80;
            TablaC.Columns[2].Width = 200;
            TablaC.Columns[3].Width = 70;
            TablaC.Columns[4].Width = 70;
            TablaC.Columns[5].Width = 70;
            TablaC.Columns[6].Width = 70;
            TablaC.Columns[7].Width = 70;
            TablaC.Columns[8].Width = 200;
            TablaC.Columns[9].Width = 100;

            conectar.Close();
        }

        // Funcion para actualizar los reportes que existen de Salidas
        public void actualizarReportesEntradas(DataGridView TablaC, string fecha1, string fecha2, TextBox texto)
        {
            string actualizar1 = "SELECT Id_entradaspm AS 'No.', nombre_proveedorespm AS 'Proveedor', contacto_proveedorespm AS 'Contacto',  telefono_proveedorespm AS 'Teléfono', total_entradaspm AS 'Total', pago_entradaspm AS 'Abono', restante_entradaspm AS 'Restante', formaspago_entradaspm AS 'Forma de pago', fecha_entradaspm AS 'Fecha', fechaalerta_entradaspm AS 'Alerta' ";
            string actualizar2 = "FROM ENTRADASPM INNER JOIN PROVEEDORESPM ON PROVEEDORESPM_Id_proveedorespm = id_proveedorespm ";
            string actualizar3 = "WHERE fecha_entradaspm >= '" + fecha1 + " 00:00' AND fecha_entradaspm <= '" + fecha2 + " 23:59' ";
            string buscar1 = "AND nombre_proveedorespm like('" + texto.Text + "%') ORDER By Id_entradaspm";

            string actualizar = actualizar1 + actualizar2 + actualizar3 + buscar1;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 90;
            TablaC.Columns[4].Width = 80;
            TablaC.Columns[5].Width = 80;
            TablaC.Columns[6].Width = 80;
            TablaC.Columns[7].Width = 120;
            TablaC.Columns[8].Width = 150;
            TablaC.Columns[9].Width = 120;
            conectar.Close();
        }
        //FUNCION PARA ACTUALIZAR LAS ALERTAS
        public void actualizarAlertasCompras(DataGridView TablaC, string fecha1, TextBox texto)
        {
            string actualizar1 = "SELECT Id_entradaspm AS 'No.', nombre_proveedorespm AS 'Proveedor', contacto_proveedorespm AS 'Contacto',  telefono_proveedorespm AS 'Teléfono', total_entradaspm AS 'Total', pago_entradaspm AS 'Abono', restante_entradaspm AS 'Restante', formaspago_entradaspm AS 'Forma de pago', fecha_entradaspm AS 'Fecha',fechaalerta_entradaspm AS 'Alerta' ";
            string actualizar2 = "FROM ENTRADASPM INNER JOIN PROVEEDORESPM ON PROVEEDORESPM_Id_proveedorespm = id_proveedorespm ";
            string actualizar3 = "WHERE  fechaalerta_entradaspm <= '" + fecha1 + " 23:59' AND restante_entradaspm > '0'  ";
            string buscar1 = "AND nombre_proveedorespm like('" + texto.Text + "%') ORDER By Id_entradaspm";

            string actualizar = actualizar1 + actualizar2 + actualizar3 + buscar1;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 90;
            TablaC.Columns[4].Width = 80;
            TablaC.Columns[5].Width = 80;
            TablaC.Columns[6].Width = 80;
            TablaC.Columns[7].Width = 120;
            TablaC.Columns[8].Width = 120;
            TablaC.Columns[9].Width = 120;
            conectar.Close();
        }
        // Funcion para actualizar los productos que se encuentran en las entradas de los reportes
        public void actualizarProductoReportesEntradas(DataGridView TablaC, string id)
        {
            string actualizar1 = "SELECT codigo_productopme AS 'Código', cantidad_productopme AS 'Cantidad', descripcion_productopme AS 'Descripción',  categorias_productopme AS 'Categoría', marca_productopme AS 'Marca', precioR_productopme AS 'Costo' ";
            string actualizar2 = "FROM PRODUCTOPME WHERE ENTRADASPM_Id_entradaspm = " + id + " ORDER By codigo_productopme";

            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 30; // No.
            TablaC.Columns[1].Width = 60; // Codigo
            TablaC.Columns[2].Width = 55; // Cantidad
            TablaC.Columns[3].Width = 240; // Descripcion
            TablaC.Columns[4].Width = 75; // Categoria
            TablaC.Columns[5].Width = 75; // Marca
            TablaC.Columns[6].Width = 85; // Costo

            TablaC.Columns.Add("Sub-Total", "Sub-Total");
            int i = 1; 
            foreach(DataGridViewRow fila in TablaC.Rows)
            {
                fila.Cells[0].Value = i++;
                double subtotal = Convert.ToInt16(fila.Cells[2].Value) * Convert.ToDouble(fila.Cells[6].Value);
                fila.Cells[7].Value = subtotal.ToString("N2");
            }
            conectar.Close();
        }

        // Funcion para actualizar los productos que se encuentran en las entradas de los reportes
        public void actualizarProductoReportesSalidas(DataGridView TablaC, string id)
        {
            string actualizar1 = "SELECT codigo_productopms AS 'Código', cantidad_productopms AS 'Cantidad', descripcion_productopms AS 'Descripción',  categorias_productopms AS 'Categoría', marca_productopms AS 'Marca', precio_productopms AS 'Precio Unidad' ";
            string actualizar2 = "FROM PRODUCTOPMS WHERE SALIDASPM_Id_salidaspm = " + id + " ORDER By codigo_productopms";

            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 30; // No.
            TablaC.Columns[1].Width = 60; // Codigo
            TablaC.Columns[2].Width = 55; // Cantidad
            TablaC.Columns[3].Width = 240; // Descripcion
            TablaC.Columns[4].Width = 75; // Categoria
            TablaC.Columns[5].Width = 75; // Marca
            TablaC.Columns[6].Width = 85; // Costo

            TablaC.Columns.Add("Sub-Total", "Sub-Total");
            int i = 1;
            foreach (DataGridViewRow fila in TablaC.Rows)
            {
                fila.Cells[0].Value = i++;
                double subtotal = Convert.ToInt16(fila.Cells[2].Value) * Convert.ToDouble(fila.Cells[6].Value);
                fila.Cells[7].Value = subtotal.ToString("N2");
            }
            conectar.Close();
        }

        // Funcion para ingresar Clientes
        public void ingresarClientes(string id, string nit, string nombres, string apellidos, string tipo, string telefono, string direccion, string email)
        {
            try
            {
                conectar.Open();
                string ingresar = "INSERT INTO CLIENTESPM (id_clientespm, nit_clientespm, nombre_clientespm, apellido_clientespm, tipo_clientepm, telefono_clientespm, direccion_clientespm, email_clientespm) VALUES (@id, @nit, @nombre, @apellido, @tipo, @telefono, @direccion, @email)";
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nit", nit);
                cmd.Parameters.AddWithValue("@nombre", nombres);
                cmd.Parameters.AddWithValue("@apellido", apellidos);
                cmd.Parameters.AddWithValue("@tipo", tipo);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Actualizar el DataGridView de clientes que recibe
        public void actualizarClientes(DataGridView TablaC, string tipo)
        {
            string actualizar1 = "SELECT id_clientespm AS 'No.', apellido_clientespm AS 'Empresa', nombre_clientespm AS 'Contacto', nit_clientespm AS 'NIT', tipo_clientepm AS 'Tipo', telefono_clientespm AS 'Teléfono', direccion_clientespm AS 'Dirección', email_clientespm AS 'Email' FROM CLIENTESPM ";
            string actualizar2 = "";
            if (tipo != "TODO")
                actualizar2 = "WHERE tipo_clientepm = '" + tipo + "'";

            string actualizar3 = "ORDER By id_clientespm";
            string actualizar = actualizar1 + actualizar2 + actualizar3;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 30;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 80;
            TablaC.Columns[4].Width = 80;
            TablaC.Columns[5].Width = 90;
            TablaC.Columns[6].Width = 180;
            TablaC.Columns[7].Width = 200;
            conectar.Close();
        }
        // Funcion para modificar Clientes
        public void modificarClientes(string id, string nit, string nombres, string apellidos, string telefono, string direccion, string email)
        {
            try
            {
                conectar.Open();
                string modificar = "UPDATE CLIENTESPM SET nombre_clientespm = @nombre, apellido_clientespm = @apellido, telefono_clientespm = @telefono, direccion_clientespm = @direccion, email_clientespm = @email, nit_clientespm =@nit WHERE id_clientespm =@id";
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombres);
                cmd.Parameters.AddWithValue("@apellido", apellidos);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@nit", nit);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        // Buscar Clientes automaticamente
        public void busqueda(DataGridView tablaC, TextBox texto, string columna, string tipo)
        {
            try
            {
                conectar.Open();
                string buscar1 = "SELECT id_clientespm AS 'No.', apellido_clientespm AS 'Empresa',nombre_clientespm AS 'Contacto', nit_clientespm AS 'NIT', tipo_clientepm AS 'Tipo', telefono_clientespm AS 'Teléfono', direccion_clientespm AS 'Dirección', email_clientespm AS 'Email' ";
                string buscar2 = "FROM CLIENTESPM WHERE tipo_clientepm = '" + tipo + "' AND " + columna + " like ('" + texto.Text + "%')";
                string buscar = buscar1 + buscar2;
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                System.Data.DataTable tabla = new System.Data.DataTable();
                adaptador.Fill(tabla);
                tablaC.DataSource = tabla;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Buscar Proveedores por txt automaticamente
        public void busquedaProveedores(DataGridView tablaC, TextBox texto, string columna)
        {
            try
            {
                conectar.Open();
                string buscar1 = "SELECT id_proveedorespm AS 'No.',  nombre_proveedorespm AS 'Empresa', nit_proveedorespm AS 'Nit', telefono_proveedorespm AS 'Teléfono', contacto_proveedorespm AS 'Contacto', direccion_proveedorespm AS 'Dirección', email_proveedorespm AS 'Email' ";
                string buscar2 = "FROM PROVEEDORESPM WHERE "+ columna + " like('" + texto.Text + "%') ORDER By id_proveedorespm";
                string buscar = buscar1 + buscar2;
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                System.Data.DataTable tabla = new System.Data.DataTable();
                adaptador.Fill(tabla);
                tablaC.DataSource = tabla;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Buscar Bodegas directas a un comboBox
        public void busquedaBodegas(ComboBox cb)
        {
            try
            {
                cb.Items.Clear(); 
                conectar.Open();
                string buscar = "SELECT *FROM BODEGASPM ";
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    cb.Items.Add(lector.GetValue(1).ToString());
                }
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Buscar Id directa de las ventas anuladas
        public string busquedaIdAnuladas()
        {
            int elementos = 0;
            try
            {
                conectar.Open();
                string buscar = "SELECT *FROM ANULADASPM ";
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                SqlDataReader lector = cmd.ExecuteReader();

                while (lector.Read())
                {
                    elementos++;
                }
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
            return Convert.ToString(elementos + 1); 
        }


        // Buscar Id para poder modificar el producto del Inventario cuando se anula una venta
        public string busquedaProductoModificarId(string codigo, string descripcion)
        {
            string idProducto = ""; 
            try
            {
                conectar.Open();
                string buscar = "SELECT *FROM INVENTARIOPM ";
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                SqlDataReader lector = cmd.ExecuteReader();

                while (lector.Read())
                {
                    if (lector.GetValue(1).ToString() == codigo && lector.GetValue(3).ToString() == descripcion)
                    {
                        idProducto = lector.GetValue(0).ToString();
                    }
                }
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
            return idProducto;
        }

        // Buscar existencia para poder modificar el producto del Inventario cuando se anula una venta
        public string busquedaProductoModificarExistencia(string codigo, string descripcion)
        {
            string exisProducto = "";
            try
            {
                conectar.Open();
                string buscar = "SELECT *FROM INVENTARIOPM ";
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                SqlDataReader lector = cmd.ExecuteReader();

                while (lector.Read())
                {
                    if (lector.GetValue(1).ToString() == codigo && lector.GetValue(3).ToString() == descripcion)
                    {
                        exisProducto = lector.GetValue(4).ToString();
                    }
                }
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
            return exisProducto;
        }

        // Buscar Producto directo del inventario a una DataGridView
        public void busquedaProductoId(string id, DataGridView T1, DataGridView T2)
        {
            try
            {
                conectar.Open();
                string buscar = "SELECT *FROM INVENTARIOPM" ;
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                SqlDataReader lector = cmd.ExecuteReader();

                while (lector.Read())
                {
                    if (lector.GetValue(0).ToString() == id)
                    {
                        T1.Rows.Add(lector.GetValue(0).ToString(), lector.GetValue(1).ToString(), lector.GetValue(2).ToString(), lector.GetValue(3).ToString(), lector.GetValue(4).ToString());
                        T2.Rows.Add(lector.GetValue(5).ToString(), lector.GetValue(6).ToString(), lector.GetValue(7).ToString(), lector.GetValue(8).ToString(), lector.GetValue(9).ToString(), lector.GetValue(10).ToString());
                    }
                }
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        //FUNCION PARA ACTUALIZAR ENTRADAS DE PRODCUTO
        public void busquedaProductoE(DataGridView TablaC, TextBox texto1, TextBox texto2, TextBox texto3, TextBox texto4, TextBox texto5)
        {
            string buscar1 = "SELECT Id_inventariopm AS 'No.', codigo_inventariopm AS 'Código.', barras_inventariopm AS 'Código de Barras', descripcion_inventariopm AS 'Descripción', existencia_inventariopm AS 'Existencia', categorias_inventariopm AS 'Categoría', marca_inventariopm AS 'Marca', precioR_inventariopm AS 'Costo', precioV_inventariopm AS 'Precio Venta', precioV1_inventariopm AS 'Precio Venta 1', precioV2_inventariopm AS 'Precio Venta 2' ";
            string buscar2 = "FROM INVENTARIOPM ";
            string buscar3 = "WHERE codigo_inventariopm like('" + texto1.Text + "%') AND ";
            string buscar4 = "barras_inventariopm like('" + texto2.Text + "%') AND ";
            string buscar5 = "descripcion_inventariopm like('" + texto3.Text + "%') AND ";
            string buscar6 = "categorias_inventariopm like('" + texto4.Text + "%') AND ";
            string buscar7 = "marca_inventariopm like('" + texto5.Text + "%') ORDER By Id_inventariopm";
            string buscar = buscar1 + buscar2 + buscar3 + buscar4 + buscar5 + buscar6 + buscar7;

            SqlCommand cmd = new SqlCommand(buscar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 30; // ID
            TablaC.Columns[1].Width = 70; // Codigo
            TablaC.Columns[2].Width = 120; // Codigo de Barras
            TablaC.Columns[3].Width = 280; // Descripcion
            TablaC.Columns[4].Width = 75; // Existencia
            TablaC.Columns[5].Width = 100; // Categoria
            TablaC.Columns[6].Width = 100; // Marca
            TablaC.Columns[7].Width = 100; // Precio Real
            TablaC.Columns[8].Width = 120;  // Precio V
            TablaC.Columns[9].Width = 120;// Precio V1
            TablaC.Columns[10].Width = 120;// Precio V2
            conectar.Close();
        }

        // Función que se creo para poder saber el número faltante en la tabla de correlativos
        public int generarN(DataGridView tabla)
        {
            int x;
            int contador = tabla.RowCount;
            int valor;
            int encon = 0;
            for (x = 1; x <= contador; x++)
            {
                encon = 0;
                for (int y = 0; y < contador; y++)
                {
                    valor = Convert.ToInt16(tabla.Rows[y].Cells[0].Value);
                    if (valor != x)
                        encon++;
                }
                if (encon == contador)
                    return x;
            }
            return x;
        }

        // Función que se creo para generar el total de un datagridview
        public double generarTotal(DataGridView tabla, int columna)
        {
            double t = 0;
            
            foreach(DataGridViewRow fila in tabla.Rows)
            {
                t += Convert.ToDouble(fila.Cells[columna].Value);
            }

            return t;
        }
        public void ingresarrecibo1(int correlativo, DateTime fecha1)
        {
            try
            {
                conectar.Open();
                string ingresar = "INSERT INTO RECIBO1PM (Id_recibo1pm, Fecha_recibo1pm) VALUES (@correlativo, @fecha1)";
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@correlativo", correlativo);
                cmd.Parameters.AddWithValue("@fecha1", fecha1);
                
                cmd.ExecuteNonQuery();
               
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        public void actualizardgvrecibo1(DataGridView TablaC)
        {
            string actualizar1 = "SELECT Id_recibo1pm AS 'No.' ";
            string actualizar2 = "FROM RECIBO1PM ORDER By Id_recibo1pm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            conectar.Close();
        }
        public void ingresarrecibo2(int correlativo, DateTime fecha2)
        {
            try
            {
                conectar.Open();
                string ingresar = "INSERT INTO RECIBO2PM (Id_recibo2pm, Fecha_recibo2pm) VALUES (@correlativo, @fecha2)";
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@correlativo", correlativo);
                cmd.Parameters.AddWithValue("@fecha2", fecha2);

                cmd.ExecuteNonQuery();
               
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        public void actualizardgvrecibo2(DataGridView TablaC)
        {
            string actualizar1 = "SELECT Id_recibo2pm AS 'No.' ";
            string actualizar2 = "FROM RECIBO2PM ORDER By Id_recibo2pm";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            conectar.Close();
        }
    }


}
