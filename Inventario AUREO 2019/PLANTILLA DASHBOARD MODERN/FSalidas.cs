﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FSalidas : Form
    {
        public FSalidas()
        {
            InitializeComponent();
        }
        // variables globales para recibir los valores del FORM CLIENTES
        public static string idCliente = "";
        public static string nombreCliente = "";
        public static string apellidoCliente = "";
        public static string nitCliente = "";
        public static string telefonoCliente = "";
        public static string emailCliente = "";
        Conexion conect = new Conexion();
        Conversion convers = new Conversion();

        public static int informacionSalidas = 0;
        private void FSalidas_Load(object sender, EventArgs e)
        {
            try
            {
                if (informacionSalidas == 1)
                {
                    idCliente = "";
                    nombreCliente = "";
                    apellidoCliente = "";
                    nitCliente = "";
                    telefonoCliente = "";
                    emailCliente = "";
                    informacionSalidas = 0;
                }

                if (nombreCliente == apellidoCliente)
                {
                    label6.Visible = false;
                    Lbapellido.Visible = false;
                }
                else
                {
                    label6.Visible = true;
                    Lbapellido.Visible = true;
                }

                Lbnombre.Text = nombreCliente;
                Lbapellido.Text = apellidoCliente;
                Lbnit.Text = nitCliente;
                Lbtelefono.Text = telefonoCliente;
                Lbemail.Text = emailCliente;
                if (Lbnombre.Text != "" && Lbapellido.Text != "")
                {
                    btnBuscarCliente.Enabled = false;
                    gbSalidas.Visible = true;
                    btnSeleccionarP.Visible = true; 
                }
                cbprecioP1.Items.Clear();
                conect.actualizarNoReportesSal(dgvReportes);
                lbSalida.Text = Convert.ToString(conect.generarN(dgvReportes));
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error SA0: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            if (Lbnombre.Text != "")
            {
                gbSalidas.Visible = true; 
            }
            Form1.acceso = 4;
        }

        public static string Sid = "";
        public static string Sexistencia = "";
        public static string Scodigo = "";
        public static string Sdescripcion = "";
        public static string Scategorias = "";
        public static string Smarca = "";
        public static string Sprecio1 = "";
        public static string Sprecio2 = "";
        public static string Sprecio3 = "";
        public static string Scosto = "";

        private void btnSeleccionarP_Click(object sender, EventArgs e)
        {
            FInventario obj = new FInventario(2);
            FInventario.formulariestado = 2;
            if (obj.ShowDialog() == DialogResult.OK)
            {
                cbprecioP1.Items.Clear();
                lbExistenciaP1.Text = Sexistencia;
                lbCodigoP1.Text = Scodigo;
                lbDescripcionP1.Text = Sdescripcion;
                cbprecioP1.Items.Add(Sprecio3);
                cbprecioP1.Items.Add(Sprecio2);
                cbprecioP1.Items.Add(Sprecio1);
                nudCantidad.Maximum = Convert.ToInt16(Sexistencia);
                nudCantidad.Minimum = 0; 

                btnAgregar.Visible = true;
                nudCantidad.Enabled = true;
                cbprecioP1.Enabled = true;
                this.ActiveControl = nudCantidad;
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarTextbox())
                { 
                    int eIdProductos = conect.generarN(dgvProductos);
                    int ecantidad = Convert.ToInt16(nudCantidad.Text);
                    int eexistencia = Convert.ToInt16(lbExistenciaP1.Text);
                    string ecodigo = lbCodigoP1.Text;
                    string edescripcion = lbDescripcionP1.Text;
                    double ecosto = Convert.ToDouble(cbprecioP1.Text);
                    double esub = (ecantidad*ecosto);
                    bool repetido = false; 
                    btnEliminar.Visible = true;
                    btnFinalizar.Visible = false;

                    foreach (DataGridViewRow fila in dgvProductos.Rows)
                    {
                        if (Sid == fila.Cells[6].Value.ToString())
                        {
                            int cantidadPro = Convert.ToInt16(fila.Cells[1].Value);
                            if ((cantidadPro + ecantidad) <= eexistencia)
                            {
                                fila.Cells[1].Value = cantidadPro + ecantidad;
                                fila.Cells[5].Value = Convert.ToDouble((cantidadPro + ecantidad) * ecosto);
                                repetido = true;
                                break;
                            }
                            else
                            {
                                FMessageBox obj = new FMessageBox("La cantidad debe ser menor a la existencia", "¡Error de Datos!", 1);
                                if (obj.ShowDialog() == DialogResult.OK) ;
                                repetido = true;
                                break;
                            }
                        }
                    }

                    double costoFinal = Convert.ToDouble(Scosto) * Convert.ToInt16(nudCantidad.Value);
                    if (!repetido)
                        dgvProductos.Rows.Add(eIdProductos,ecantidad,ecodigo,edescripcion,ecosto,esub,Sid,eexistencia,Scategorias,Smarca,costoFinal);
                    
                    lbTotal.Text = (conect.generarTotal(dgvProductos, 5)).ToString("N2");
                    lbGanancia.Text = Convert.ToString(Convert.ToDouble(lbTotal.Text) - conect.generarTotal(dgvProductos, 10));
                    txtAcredito.Text = lbTotal.Text;

                    cbprecioP1.Items.Clear();
                    lbExistenciaP1.Text = "";
                    lbCodigoP1.Text = "";
                    lbDescripcionP1.Text = "";
                    nudCantidad.Value = 0;

                    Sid = "";
                    Sexistencia = "";
                    Scodigo = "";
                    Sdescripcion = "";
                    Scategorias = "";
                    Smarca = ""; 
                    Sprecio1 = "";
                    Sprecio2 = "";
                    Sprecio3 = "";

                    btnAgregar.Visible = false;
                    nudCantidad.Enabled = false;
                    cbprecioP1.Enabled = false;
                    btnFinalizar.Visible = true;
                    txtReferencia.Enabled = true;
                    txtAcredito.Enabled = true; 

                    this.ActiveControl = nudCantidad;
                    Form1.acceso = 10;
                }
                else
                {
                    FMessageBox obj = new FMessageBox("Se deben ingresar todos los campos que se solicitan.", "ERROR PR0:", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error SA1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private bool validarTextbox2()
        {
            bool estado = true; 
            if (txtReferencia.Text == "")
            {
                errorP1.SetError(txtReferencia, "Ingrese el número de referencia.");
                estado = false;
            }
            if (txtAcredito.Text == "")
            {
                errorP1.SetError(txtAcredito, "Ingrese la cantidad que acredito el usuario.");
                estado = false;
            }

            if (Convert.ToDouble(txtAcredito.Text) > Convert.ToDouble(lbTotal.Text))
            {
                errorP1.SetError(txtAcredito, "La cantidad debe ser menor o igual al total final.");
                estado = false;
            }

            if (Convert.ToDouble(txtAcredito.Text) < 0)
            {
                errorP1.SetError(txtAcredito, "La cantidad debe ser mayor o igual a cero.");
                estado = false;
            }

            return estado; 
        }
        private bool validarTextbox()
        {
            bool estado = true;
            if (cbprecioP1.Text == "")
            {
                errorP1.SetError(cbprecioP1, "Seleccione un precio de la lista.");
                estado = false;
            }

            if (nudCantidad.Text == "")
            {
                errorP1.SetError(nudCantidad, "Ingrese una cantidad en números.");
                estado = false;
            }

            return estado;
        }
        

        private void cbprecioP1_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorP1.SetError(cbprecioP1, "");
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarTextbox2())
                {
                    lbSalida.Text = Convert.ToString(conect.generarN(dgvReportes));

                    DataGridViewRow fila1 = new DataGridViewRow();
                    DataGridViewRow fila11 = new DataGridViewRow();
                    DataGridViewRow fila2 = new DataGridViewRow();
                    DataGridViewRow fila3 = new DataGridViewRow();
                    DataGridViewRow fila4 = new DataGridViewRow();

                    fila1.CreateCells(dgvDatos);
                    fila1.Cells[0].Value = "Contacto: " + Lbnombre.Text;
                    fila1.Cells[1].Value = "Nit: " + Lbnit.Text;
                    dgvDatos.Rows.Add(fila1);

                    fila11.CreateCells(dgvDatos);
                    fila11.Cells[0].Value = "Empresa: " + Lbapellido.Text;
                    fila11.Cells[1].Value = "Teléfono: " + Lbtelefono.Text;
                    dgvDatos.Rows.Add(fila11);

                    fila2.CreateCells(dgvTotal);
                    fila2.Cells[0].Value = "Total en letras: ";
                    fila2.Cells[1].Value = convers.numerosLetras(lbTotal.Text);
                    dgvTotal.Rows.Add(fila2);

                    fila3.CreateCells(dgvTotal);
                    fila3.Cells[0].Value = "Referencia:";
                    fila3.Cells[1].Value = txtReferencia.Text;
                    dgvTotal.Rows.Add(fila3);

                    fila4.CreateCells(dgvTotal);
                    fila4.Cells[0].Value = "Atendido Por:" ;
                    fila4.Cells[1].Value = Form1.nombreUsuario;
                    dgvTotal.Rows.Add(fila4);

                    // Se guarda el reporte de salida de la venta de producto.
                    double totalFinal = Convert.ToDouble(lbTotal.Text);
                    double AcreditoFinal = Convert.ToDouble(txtAcredito.Text); 
                    conect.ingresarReportesSalidas(lbSalida.Text, totalFinal.ToString(), AcreditoFinal.ToString(), lbGanancia.Text, dtfecha1.Value.ToString(), idCliente);

                    foreach (DataGridViewRow fila in dgvProductos.Rows)
                    {
                        string ino = fila.Cells[0].Value.ToString();
                        int icantidad = Convert.ToInt16(fila.Cells[1].Value);
                        string icodigo = fila.Cells[2].Value.ToString();
                        string inombre = fila.Cells[3].Value.ToString();
                        string iprecioR = fila.Cells[4].Value.ToString();
                        string isubtotal = fila.Cells[5].Value.ToString();
                        string idPro = fila.Cells[6].Value.ToString();
                        int iexiPro = Convert.ToInt16(fila.Cells[7].Value);
                        string icategorias = fila.Cells[8].Value.ToString();
                        string imarca = fila.Cells[9].Value.ToString();

                        // Se enlazan los productos a las SALIDAS en SQL
                        conect.ingresarReportesSalidasProducto(lbSalida.Text, icodigo, icantidad.ToString(), inombre, icategorias, imarca, iprecioR);
                        // Se agregan los productos al dgvImprimir para su Impresion
                        dgvImprimir.Rows.Add(ino, icantidad, icodigo, inombre, iprecioR, isubtotal);
                        // Se modifica la cantidad de productos existentes
                        conect.modificarProductoCantidad(idPro, (iexiPro - icantidad));
                    }

                    if (Lbnombre.Text == Lbapellido.Text)
                        exportarPdf(lbSalida.Text, 1);
                    else
                        exportarPdf(lbSalida.Text, 0);

                    btnFinalizar.Visible = false;
                    btnEliminar.Visible = false;
                    btnAgregar.Visible = false;
                    dgvProductos.Rows.Clear();
                    dgvImprimir.Rows.Clear();
                    dgvTotal.Rows.Clear();
                    dgvDatos.Rows.Clear();

                    cbprecioP1.Items.Clear();
                    lbExistenciaP1.Text = "";
                    lbCodigoP1.Text = "";
                    lbDescripcionP1.Text = "";
                    nudCantidad.Value = 0;
                    Lbapellido.Text = "";
                    Lbnombre.Text = "";
                    Lbtelefono.Text = "";
                    Lbemail.Text = "";
                    Lbnit.Text = "";
                    txtReferencia.Clear();
                    txtReferencia.Enabled = false; 
                    btnSeleccionarP.Visible = false;
                    gbSalidas.Visible = false;
                    btnBuscarCliente.Enabled = true;

                    Sid = "";
                    Sexistencia = "";
                    Scodigo = "";
                    Sdescripcion = "";
                    Scategorias = "";
                    Smarca = "";
                    Sprecio1 = "";
                    Sprecio2 = "";
                    Sprecio3 = "";

                    idCliente = "";
                    nombreCliente = "";
                    apellidoCliente = "";
                    nitCliente = "";
                    telefonoCliente = "";
                    emailCliente = "";

                    Form1.acceso = 11;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error SA2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                FMessageBoxP obj = new FMessageBoxP("¿Está seguro en eliminar el producto seleccionado?", "¡Alerta de eliminación!", 1);
                if (obj.ShowDialog() == DialogResult.OK)
                {
                    string idSelect = dgvProductos.SelectedCells[6].Value.ToString();
                    int cantSelect = Convert.ToInt16(dgvProductos.SelectedCells[1].Value);
                    int exiSelect = Convert.ToInt16(dgvProductos.SelectedCells[7].Value);

                    conect.modificarProductoCantidad(idSelect, cantSelect + exiSelect);
                    dgvProductos.Rows.RemoveAt(dgvProductos.CurrentRow.Index);
                }
                if (dgvProductos.RowCount <= 0)
                {
                    btnEliminar.Visible = false;
                    btnFinalizar.Visible = false;
                    btnAgregar.Visible = false;
                    txtReferencia.Enabled = false;
                    txtAcredito.Enabled = false;
                    txtAcredito.Clear();
                    txtReferencia.Clear();
                    Form1.acceso = 11;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Verifique el dato a eliminar", "¡Alerta de eliminación!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para generar los PDF 
        public void exportarPdf(string nombreS, int cc)
        {
            int m = dtfecha1.Value.Month;
            int d = dtfecha1.Value.Day;
            int a = dtfecha1.Value.Year;
            string ff = Convert.ToString(d) + "-" + Convert.ToString(m) + "-" + Convert.ToString(a);
         
            string escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string cliente = Lbnombre.Text;
            string carpeta = escritorio + "\\" + "PM Inventario";
            // string carpeta = @"C:\Proyectos Multiples\" + mes;
            string subcarpeta = carpeta + "\\Salida de Producto";

            if (!(Directory.Exists(carpeta)))
            {
                Directory.CreateDirectory(carpeta);
            }
            if (!(Directory.Exists(subcarpeta)))
            {
                Directory.CreateDirectory(subcarpeta);
            }


            string ruta = subcarpeta + "\\" + nombreS + " Reporte " + ff + ".pdf";
            float porcentaje = 0.0f;

            Document doc = new Document(iTextSharp.text.PageSize.A4);
            // Para bajar el contenido es el tercer valor
            // Margenes de pdf izquierda-derecha-arriba-abajo
            doc.SetMargins(10, 10, 120, 180);
            PdfWriter escribir = PdfWriter.GetInstance(doc, new FileStream(ruta, FileMode.Create));
            doc.Open();

            // Agregar Imagen de Encabezado al PDF
            iTextSharp.text.Image encabezado = iTextSharp.text.Image.GetInstance("EncabezadoVentas.jpg");
            encabezado.SetAbsolutePosition(50, 740);
            porcentaje = 500 / encabezado.Width;
            encabezado.ScalePercent(porcentaje * 100);

            // Agregar la DATAGRIDVIEW al PDF
            PdfPTable pdf = new PdfPTable(dgvImprimir.Columns.Count);
            PdfPTable pdf2 = new PdfPTable(dgvDatos.Columns.Count);
            PdfPTable pdf3 = new PdfPTable(dgvTotal.Columns.Count);
            //                              Es para el tipo de fuente
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
            PdfContentByte cb = escribir.DirectContent;

            float[] ancho = new float[6] { 2f, 3f, 4f, 14f, 5f, 5f};
            pdf.SetWidths(ancho);

            float[] ancho2;
            ancho2 = new float[2] { 15f, 5f };
            pdf2.SetWidths(ancho2);

            float[] ancho3 = new float[2] { 5f, 15f };
            pdf3.SetWidths(ancho3);

            //pdf.DefaultCell.Padding = 1;
            //pdf.HorizontalAlignment = Element.ALIGN_CENTER;
            pdf.DefaultCell.Padding = 2;

            iTextSharp.text.Font text1 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
            iTextSharp.text.Font text2 = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font text3 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL, BaseColor.GRAY);
            iTextSharp.text.Font text4 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            // Informacion de filas del Cliente Nombre-Empresa-Nit-Telefono
            foreach (DataGridViewRow row in dgvDatos.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdf2.DefaultCell.BorderWidth = 1;
                    pdf2.DefaultCell.BackgroundColor = new iTextSharp.text.BaseColor(227, 233, 252);
                    pdf2.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(20, 32, 40);
                    pdf2.AddCell(new Phrase(cell.Value.ToString(), text4));
                }
            }

            // Agregar intermedio blanco
            foreach (DataGridViewColumn columna in dgvDatos.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(Color.White);
                cell.BorderWidth = 0;
                pdf2.AddCell(cell);
            }

            // Agregar encabezado de los productos a imprimir
            foreach (DataGridViewColumn columna in dgvImprimir.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 1;
                pdf.AddCell(cell);
            }

            // Informacion de filas de los productos a imprimir
            foreach (DataGridViewRow row in dgvImprimir.Rows)
            {
                int i = 0;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    i++;
                    if (i % 5 == 0 || i % 6 == 0)
                    {
                        double totalDecimal = Convert.ToDouble(cell.Value);
                        pdf.AddCell(new Phrase("Q. " + totalDecimal.ToString("N2"), text2));
                    }
                    else
                        pdf.AddCell(new Phrase(cell.Value.ToString(), text2));
                }
            }
            iTextSharp.text.Font text5 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            // Para colocar el final hasta abajo
            pdf.DefaultCell.BorderWidthLeft = 1;
            pdf.DefaultCell.BorderWidthRight = 0;
            pdf.AddCell(new Phrase(" ", text5));
            pdf.DefaultCell.BorderWidthLeft = 0;
            pdf.AddCell(new Phrase(" ", text5));
            pdf.AddCell(new Phrase(" ", text5));
            pdf.AddCell(new Phrase(" ", text5));
            pdf.DefaultCell.BorderWidthRight = 1;
            pdf.AddCell(new Phrase("  TOTAL", text5));
            pdf.AddCell(new Phrase("Q. " + lbTotal.Text, text2));
            pdf.DefaultCell.BorderWidthRight = 0;

            // Agregar para dejar un espacio en blanco XD
            foreach (DataGridViewColumn columna in dgvTotal.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(Color.White);
                cell.BorderWidthRight = 0;
                cell.BorderWidthLeft = 0;
                pdf3.AddCell(cell);
            }

            // Informacion de filas sobre el total, observaciones.
            foreach (DataGridViewRow row in dgvTotal.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdf3.DefaultCell.BorderWidth = 0;
                   // pdf3.DefaultCell.BackgroundColor = new iTextSharp.text.BaseColor(227, 233, 252);
                    pdf3.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.White);
                    pdf3.AddCell(new Phrase(cell.Value.ToString(), text3));
                }
            }
            // FIN de la tabla

            
            // Agregar titulos
            cb.BeginText();
            //  bf.FontType()
            cb.SetFontAndSize(bf, 10);
            cb.SetTextMatrix(350, 730);
            cb.ShowText("Fecha: " + dtfecha1.Value.ToString());
            cb.SetTextMatrix(67, 730);
            cb.ShowText("Reporte No. " + lbSalida.Text);
            cb.EndText();

            // Agregar objetos al PDF

            doc.Add(encabezado);
            doc.Add(pdf2);
            doc.Add(pdf);
            doc.Add(pdf3);
            doc.Close();
            Process.Start(ruta);
        }

        private void dgvProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dgvProductos_MultiSelectChanged(object sender, EventArgs e)
        {
        }

        private void txtReferencia_KeyUp(object sender, KeyEventArgs e)
        {
            errorP1.SetError(txtReferencia, "");
        }

        private void txtAcredito_KeyUp(object sender, KeyEventArgs e)
        {
            double c;
            if (!double.TryParse(txtAcredito.Text, out c))
                errorP1.SetError(txtAcredito, "Ingrese la cantidad en números.");
            else
            {
                if (c >= 0 && c <= Convert.ToDouble(lbTotal.Text))
                    errorP1.SetError(txtAcredito, "");
                else
                    errorP1.SetError(txtAcredito, "Ingrese una cantidad valida");
            }
        }
    }
}
