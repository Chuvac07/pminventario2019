﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FInventario : Form
    {
        public FInventario(int formula)
        {
            InitializeComponent();
            formulario = formula;
        }

        int formulario = 0;
        Conexion conect = new Conexion();
        private void FproductoI_Load(object sender, EventArgs e)
        {
            conect.actualizarProductoE(dgvProductos);

            if (formulario == 2)
            {
                btnNuevo.Visible = false;
                btnModificar.Visible = false;
                btnEliminar.Visible = false; 
            }
            if (formulario == 1)
            {
                btnNuevo.Visible = true;
                btnModificar.Visible = false;
                btnEliminar.Visible = false;
            }
            if (formulario == 3)
            {
                dgvProductos.ReadOnly = false;
                dgvProductos.Columns[0].ReadOnly = true; // numero de Id del producto
            }
            this.ActiveControl = txtcodigoB1;
        }

        public static int formulariestado = 0; 
        private void dgvProductos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (formulariestado == 1)
            {
                FEntradas.Eid = dgvProductos.SelectedCells[0].Value.ToString();
                FEntradas.Eexistencia = dgvProductos.SelectedCells[4].Value.ToString();
                FEntradas.Ecodigo = dgvProductos.SelectedCells[1].Value.ToString(); ;
                FEntradas.Edescripcion = dgvProductos.SelectedCells[3].Value.ToString();
                FEntradas.Ecategorias = dgvProductos.SelectedCells[5].Value.ToString();
                FEntradas.Emarca = dgvProductos.SelectedCells[6].Value.ToString();
                FEntradas.Ecosto = dgvProductos.SelectedCells[7].Value.ToString();

                DialogResult = DialogResult.OK;
            }
            if (formulariestado == 2)
            {
                FSalidas.Sid = dgvProductos.SelectedCells[0].Value.ToString();
                FSalidas.Scodigo = dgvProductos.SelectedCells[1].Value.ToString();
                FSalidas.Sdescripcion = dgvProductos.SelectedCells[3].Value.ToString();
                FSalidas.Sexistencia = dgvProductos.SelectedCells[4].Value.ToString();
                FSalidas.Scategorias = dgvProductos.SelectedCells[5].Value.ToString();
                FSalidas.Smarca = dgvProductos.SelectedCells[6].Value.ToString();
                FSalidas.Scosto = dgvProductos.SelectedCells[7].Value.ToString();
                FSalidas.Sprecio1 = dgvProductos.SelectedCells[8].Value.ToString();
                FSalidas.Sprecio2 = dgvProductos.SelectedCells[9].Value.ToString();
                FSalidas.Sprecio3 = dgvProductos.SelectedCells[10].Value.ToString();

                DialogResult = DialogResult.OK;
            }
            if (formulariestado == 3)
            {
                FBodegas.BidInventario = dgvProductos.SelectedCells[0].Value.ToString();
                FBodegas.Bexistencia = dgvProductos.SelectedCells[4].Value.ToString();
                FBodegasCantidad obj2 = new FBodegasCantidad(dgvProductos.SelectedCells[3].Value.ToString(), dgvProductos.SelectedCells[4].Value.ToString());
                if (obj2.ShowDialog() == DialogResult.OK)
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void txtcodigoB1_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaProductoE(dgvProductos, txtcodigoB1, txtbarrasB1, txtnombreB1, txtCateB1, txtMarcaB1);
        }

        private void txtnombreB1_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaProductoE(dgvProductos, txtcodigoB1, txtbarrasB1, txtnombreB1, txtCateB1, txtMarcaB1);
        }

        private void txtCateB1_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaProductoE(dgvProductos, txtcodigoB1, txtbarrasB1, txtnombreB1, txtCateB1, txtMarcaB1);
        }

        private void txtMarcaB1_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaProductoE(dgvProductos, txtcodigoB1, txtbarrasB1, txtnombreB1, txtCateB1, txtMarcaB1);
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FNuevoProducto obj = new FNuevoProducto(conect.generarN(dgvProductos), formulario);
            if (obj.ShowDialog() == DialogResult.OK)
            {
                conect.actualizarProductoE(dgvProductos);
                DialogResult = DialogResult.OK;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            string Id = dgvProductos.SelectedCells[0].Value.ToString();

            FMessageBoxP obj = new FMessageBoxP("¿Está seguro en eliminar el producto seleccionado?", "¡Alerta de eliminación!", 1);
            if (obj.ShowDialog() == DialogResult.OK)
            {
                conect.eliminarProductosE(Id);
                conect.actualizarProductoE(dgvProductos);
            }
        }


        private void btnModificar_Click(object sender, EventArgs e)
        {
            string Id = dgvProductos.SelectedCells[0].Value.ToString();
            string codigo = dgvProductos.SelectedCells[1].Value.ToString();
            string barras = dgvProductos.SelectedCells[2].Value.ToString();
            string descripcion = dgvProductos.SelectedCells[3].Value.ToString();
            int existencia = Convert.ToInt16(dgvProductos.SelectedCells[4].Value);
            string categoria = dgvProductos.SelectedCells[5].Value.ToString();
            string marca = dgvProductos.SelectedCells[6].Value.ToString();
            double precioCosto = Convert.ToDouble(dgvProductos.SelectedCells[7].Value);
            double precioV = Convert.ToDouble(dgvProductos.SelectedCells[8].Value);
            double precioV1 = Convert.ToDouble(dgvProductos.SelectedCells[9].Value);
            double precioV2 = Convert.ToDouble(dgvProductos.SelectedCells[10].Value);

            conect.busquedaProductoId(Id, dgvOriginal1, dgvOriginal2);
            dgvImprimir1.Rows.Add(Id, codigo, barras, descripcion, existencia);
            dgvImprimir2.Rows.Add(categoria, marca, precioCosto, precioV, precioV1, precioV2);

            int jexistencia = Convert.ToInt16(dgvOriginal1.Rows[0].Cells[4].Value);
            double jprecioCosto = Convert.ToDouble(dgvOriginal2.Rows[0].Cells[2].Value);
            double jprecioV = Convert.ToDouble(dgvOriginal2.Rows[0].Cells[3].Value);
            double jprecioV1 = Convert.ToDouble(dgvOriginal2.Rows[0].Cells[4].Value);
            double jprecioV2 = Convert.ToDouble(dgvOriginal2.Rows[0].Cells[5].Value);

            if (existencia != jexistencia || precioCosto != jprecioCosto || precioV != jprecioV || precioV1 != jprecioV1 || precioV2 != jprecioV2)
            {
                FMessageBoxP obj = new FMessageBoxP("", "Observación:", 0);
                FMessageBoxP.bandera = true;
                if (obj.ShowDialog() == DialogResult.OK)
                {
                    // Buscar producto y agregarlo al DataGridView con los valores originales
                    conect.modificarProductoE(Id, codigo, barras, descripcion, existencia, categoria, marca, precioCosto, precioV, precioV1, precioV2);
                    exportarPdf(dgvImprimir1, dgvImprimir2, dgvOriginal1, dgvOriginal2);
                    //////****************OJO LUIS FALTA GUARDAR LAS MODIFICACIONES
                    conect.ingresarModificacionesInventario(Id, codigo, descripcion, existencia - jexistencia, precioCosto - jprecioCosto, precioV - jprecioV, precioV1 - jprecioV1, precioV2 - jprecioV2, InObservaciones , dtpFechaHoy.Value.ToString("dd/MM/yyyy"));
                }
            }
            else
            {
                conect.modificarProductoE(Id, codigo, barras, descripcion, existencia, categoria, marca, precioCosto, precioV, precioV1, precioV2);
            }
            dgvImprimir1.Rows.Clear();
            dgvImprimir2.Rows.Clear();
            dgvOriginal1.Rows.Clear();
            dgvOriginal2.Rows.Clear();
            conect.actualizarProductoE(dgvProductos);
        }

        // Funcion para generar los PDF 
        static public string InObservaciones = ""; 
        public void exportarPdf(DataGridView Tabla1, DataGridView Tabla2, DataGridView Tablai1, DataGridView Tablai2)
        {
            string ff = dtpFechaHoy.Value.ToString("dd-MM-yyyy HH-mm-ss");

            string escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string carpeta = escritorio + "\\" + "PM Inventario";
            // string carpeta = @"C:\Proyectos Multiples\" + mes;
            string subcarpeta = carpeta + "\\Modificación Inventario";

            if (!(Directory.Exists(carpeta)))
            {
                Directory.CreateDirectory(carpeta);
            }
            if (!(Directory.Exists(subcarpeta)))
            {
                Directory.CreateDirectory(subcarpeta);
            }


            string ruta = subcarpeta + "\\Reporte " + ff + ".pdf";
            float porcentaje = 0.0f;

            Document doc = new Document(iTextSharp.text.PageSize.A4);

            // Para bajar el contenido es el tercer valor
            // Margenes de pdf izquierda-derecha-arriba-abajo
            doc.SetMargins(5, 5, 130, 10);
            PdfWriter escribir = PdfWriter.GetInstance(doc, new FileStream(ruta, FileMode.Create));
            doc.Open();

            // Agregar Imagen de Encabezado al PDF
            iTextSharp.text.Image encabezado = iTextSharp.text.Image.GetInstance("EncabezadoEditadas.jpg");
            encabezado.SetAbsolutePosition(50, 740);
            porcentaje = 500 / encabezado.Width;
            encabezado.ScalePercent(porcentaje * 100);
            doc.Add(encabezado);

            // Agregar la DATAGRIDVIEW al PDF
            PdfPTable pdf = new PdfPTable(Tabla1.Columns.Count);
            PdfPTable pdf2 = new PdfPTable(Tabla2.Columns.Count);
            PdfPTable pdfi1 = new PdfPTable(Tablai1.Columns.Count);
            PdfPTable pdfi2 = new PdfPTable(Tablai2.Columns.Count);
            PdfPTable pdf3 = new PdfPTable(dgvObservacion.Columns.Count);
            PdfPTable pdf4 = new PdfPTable(dgvObservacion.Columns.Count);
            //                              Es para el tipo de fuente
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
            PdfContentByte cb = escribir.DirectContent;

            // Agregar titulos
            cb.BeginText();
            //  bf.FontType()
            cb.SetFontAndSize(bf, 9);
            cb.SetTextMatrix(62, 730);
            cb.ShowText("Producto Original \t\t\t Fecha: " + dtpFechaHoy.Value.ToString("dd/MM/yyyy"));
            cb.EndText();

            float[] ancho = new float[5] { 2f, 5f, 5f, 15f, 5f };
            pdf.SetWidths(ancho);
            pdfi1.SetWidths(ancho);

            float[] ancho2 = new float[6] { 5f, 5f, 5f, 5f, 5f, 5f };
            pdf2.SetWidths(ancho2);
            pdfi2.SetWidths(ancho2);

            float[] ancho3 = new float[1] { 10f };
            pdf3.SetWidths(ancho3);
            pdf4.SetWidths(ancho3);

            //pdf.HorizontalAlignment = Element.ALIGN_CENTER;

            iTextSharp.text.Font text1 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
            iTextSharp.text.Font text2 = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            // ********************** Agregar encabezado del producto original
            foreach (DataGridViewColumn columna in Tablai1.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 1;
                pdfi1.AddCell(cell);
            }

            // Informacion de filas
            foreach (DataGridViewRow row in Tablai1.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdfi1.AddCell(new Phrase(cell.Value.ToString(), text2));
                }
            }

            iTextSharp.text.Font text5 = new iTextSharp.text.Font(bf, 15, iTextSharp.text.Font.BOLD, BaseColor.WHITE);
            // Para colocar el final hasta abajo
            pdfi1.DefaultCell.BorderWidthLeft = 0;
            pdfi1.DefaultCell.BorderWidthRight = 0;
            pdfi1.AddCell(new Phrase(" ", text5)); // 1
            pdfi1.AddCell(new Phrase(" ", text5)); // 2
            pdfi1.AddCell(new Phrase(" ", text5)); // 3
            pdfi1.AddCell(new Phrase(" ", text5)); // 4
            pdfi1.AddCell(new Phrase(" ", text5)); // 5

            doc.Add(pdfi1);

            // Agregar encabezado
            foreach (DataGridViewColumn columna in Tablai2.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 1;
                pdfi2.AddCell(cell);
            }

            // Informacion de filas
            foreach (DataGridViewRow row in Tablai2.Rows)
            {
                int i = 0;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    i++;

                    if (i % 3 == 0 || i % 4 == 0 || i % 5 == 0 || i % 6 == 0)
                    {
                        double totalDecimal = Convert.ToDouble(cell.Value);
                        pdfi2.AddCell(new Phrase("Q. " + totalDecimal.ToString("N2"), text2));
                    }
                    else
                        pdfi2.AddCell(new Phrase(cell.Value.ToString(), text2));
                }
            }
            doc.Add(pdfi2);

            //********************************************************************

            pdf3.DefaultCell.BorderWidthLeft = 0;
            pdf3.DefaultCell.BorderWidthRight = 0;
            pdf3.DefaultCell.BorderWidthBottom = 0;
            pdf3.DefaultCell.BorderWidthTop = 0;
            pdf3.AddCell(new Phrase(" ", text2)); // 0
            pdf3.AddCell(new Phrase(" ", text2)); // 1
            pdf3.AddCell(new Phrase(" ", text2)); // 2
            pdf3.DefaultCell.BorderWidthTop = 1;
            pdf3.AddCell(new Phrase("Producto Modificado", text2)); // 3
            pdf3.DefaultCell.BorderWidthTop = 0;
            pdf3.AddCell(new Phrase(" ", text2)); // 4
            doc.Add(pdf3);

            // ********************** Agregar encabezado del producto modificado 
            foreach (DataGridViewColumn columna in Tabla1.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 1;
                pdf.AddCell(cell);
            }

            // Informacion de filas
            foreach (DataGridViewRow row in Tabla1.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdf.AddCell(new Phrase(cell.Value.ToString(), text2));
                }
            }

            // Para colocar el final hasta abajo
            pdf.DefaultCell.BorderWidthLeft = 0;
            pdf.DefaultCell.BorderWidthRight = 0;
            pdf.AddCell(new Phrase(" ", text5)); // 1
            pdf.AddCell(new Phrase(" ", text5)); // 2
            pdf.AddCell(new Phrase(" ", text5)); // 3
            pdf.AddCell(new Phrase(" ", text5)); // 4
            pdf.AddCell(new Phrase(" ", text5)); // 5

            doc.Add(pdf);

            // Agregar encabezado
            foreach (DataGridViewColumn columna in Tabla2.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 1;
                pdf2.AddCell(cell);
            }

            // Informacion de filas
            foreach (DataGridViewRow row in Tabla2.Rows)
            {
                int i = 0;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    i++;

                       if (i % 3 == 0 || i % 4 == 0 || i % 5 == 0 || i % 6 == 0)
                    {
                             double totalDecimal = Convert.ToDouble(cell.Value);
                           pdf2.AddCell(new Phrase("Q. " + totalDecimal.ToString("N2"), text2));
                    }
                     else
                    pdf2.AddCell(new Phrase(cell.Value.ToString(), text2));
                }
            }
            doc.Add(pdf2);
            // *************************Observaciones

            pdf4.DefaultCell.BorderWidthLeft = 0;
            pdf4.DefaultCell.BorderWidthRight = 0;
            pdf4.DefaultCell.BorderWidthBottom = 0;
            pdf4.DefaultCell.BorderWidthTop = 0;
            pdf4.AddCell(new Phrase(" ", text2)); // 1
            pdf4.AddCell(new Phrase(" ", text2)); // 2
            pdf4.AddCell(new Phrase(" ", text2)); // 3
            pdf4.DefaultCell.BorderWidthTop = 1;
            pdf4.AddCell(new Phrase("OBSERVACIONES: " + InObservaciones, text2)); // 4
            pdf4.DefaultCell.BorderWidthTop = 0;
            pdf4.AddCell(new Phrase(" ", text2)); // 5
            pdf4.AddCell(new Phrase(" ", text2)); // 6
            pdf4.DefaultCell.BorderWidthTop = 1;
            pdf4.AddCell(new Phrase("Generado por: " + Form1.nombreUsuario, text2)); // 7
            doc.Add(pdf4);

            // FIN de la tabla
            // Agregar objetos al PDF



            doc.Close();

            Process.Start(ruta);
        }

        private void txtbarras_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaProductoE(dgvProductos, txtcodigoB1, txtbarrasB1, txtnombreB1, txtCateB1, txtMarcaB1);
        }

        private void txtcodigoB1_Layout(object sender, LayoutEventArgs e)
        {

        }
    }
}
