﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FRecibos : Form
    {
        public FRecibos()
        {
            InitializeComponent();
        }

        Conexion conect = new Conexion();
        Conversion conver = new Conversion();
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }
        int pdf = 0;
        private void btnrecibocaja_Click(object sender, EventArgs e)
        {
             pdf = 1;
            gbrecibocaja.Visible = true;
            gbrecibo2.Visible = false;
            txtRecibide1.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
             pdf = 2;
            gbrecibo2.Visible=true;
            gbrecibocaja.Visible = false;
            txtrecibide2.Focus();
        }

        public void crearPdf()
        {
            try
            {
                int m = dtFecha1.Value.Month;
                int d = dtFecha1.Value.Day;
                int a = dtFecha1.Value.Year;
                string ff = Convert.ToString(d) + "-" + Convert.ToString(m) + "-" + Convert.ToString(a);
                string mes = "";
                switch (m)
                {
                    case 1: mes = "Enero"; break;
                    case 2: mes = "Febrero"; break;
                    case 3: mes = "Marzo"; break;
                    case 4: mes = "Abril"; break;
                    case 5: mes = "Mayo"; break;
                    case 6: mes = "Junio"; break;
                    case 7: mes = "Julio"; break;
                    case 8: mes = "Agosto"; break;
                    case 9: mes = "Septiembre"; break;
                    case 10: mes = "Octubre"; break;
                    case 11: mes = "Noviembre"; break;
                    case 12: mes = "Diciembre"; break;
                }
                string escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                string cliente = ff;
                string carpeta = escritorio + "\\" + "PM Recibos" + "\\" + "Recibos De Caja" + "\\"+ mes;
                
                if (!(Directory.Exists(carpeta)))
                {
                    Directory.CreateDirectory(carpeta);
                }
               

                string ruta = carpeta + "\\" + " ReciboCaja " + lbcorrelativo.Text + ".pdf";
                float porcentaje = 0.0f;

                Document doc = new Document(iTextSharp.text.PageSize.A4);



                // Para bajar el contenido es el tercer valor
                // Margenes de pdf izquierda-derecha-arriba-abajo
                doc.SetMargins(10, 10, 180, 180);
                PdfWriter escribir = PdfWriter.GetInstance(doc, new FileStream(ruta, FileMode.Create));
                doc.Open();
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA /*TIMES_ROMAN*/, BaseFont.CP1252, BaseFont.EMBEDDED);
                PdfContentByte cb = escribir.DirectContent;

                //medidas


                // Agregar Imagen de Encabezado al PDF
                iTextSharp.text.Image encabezado = iTextSharp.text.Image.GetInstance("Encabezadorecibo.jpg");
                encabezado.SetAbsolutePosition(50, 720);
                porcentaje = 425 / encabezado.Width;
                encabezado.ScalePercent(porcentaje * 100);

                // Agregar Imagen de recibide al PDF
                iTextSharp.text.Image recibide = iTextSharp.text.Image.GetInstance("recibide.jpg");
                //encabezado.SetAbsolutePosition(50, 690);
                recibide.SetAbsolutePosition(25, 596);
                porcentaje = 560 / recibide.Width;
                recibide.ScalePercent(porcentaje * 100);

                // Agregar Imagen de recibide al PDF
                iTextSharp.text.Image recibocaja = iTextSharp.text.Image.GetInstance("recibocaja.jpg");
                //encabezado.SetAbsolutePosition(50, 690);
                recibocaja.SetAbsolutePosition(25, 434);
                porcentaje = 560 / recibocaja.Width;
                recibocaja.ScalePercent(porcentaje * 100);

                //agregar imagenes de copia

                // Agregar Imagen de Encabezado al PDF
                iTextSharp.text.Image encabezado2 = iTextSharp.text.Image.GetInstance("Encabezadorecibo.jpg");
                encabezado2.SetAbsolutePosition(50, 293);
                porcentaje = 425 / encabezado2.Width;
                encabezado2.ScalePercent(porcentaje * 100);

                // Agregar Imagen de recibide al PDF
                iTextSharp.text.Image recibide2 = iTextSharp.text.Image.GetInstance("recibide.jpg");
                //encabezado.SetAbsolutePosition(50, 690);
                recibide2.SetAbsolutePosition(25, 170);
                porcentaje = 560 / recibide2.Width;
                recibide2.ScalePercent(porcentaje * 100);

                // Agregar Imagen de recibide al PDF
                iTextSharp.text.Image recibocaja2 = iTextSharp.text.Image.GetInstance("recibocaja.jpg");
                //encabezado.SetAbsolutePosition(50, 690);
                recibocaja2.SetAbsolutePosition(25, 7);
                porcentaje = 560 / recibocaja2.Width;
                recibocaja2.ScalePercent(porcentaje * 100);
                string totalletras1 = conver.numerosLetras(txtCantidadde1.Text);
                
                cb.BeginText();
                cb.SetFontAndSize(bf, 10);
                cb.SetTextMatrix(20, 415);//lineas de corte
                cb.ShowText("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");//lineas de corte
                //funcion para separar texto largo
                string porcaja = txtpor.Text + "                                                                                                                                                                                                                                                          ...";
                string por1 = porcaja.Substring(0, 80);
                string por2 = porcaja.Substring(80, 80);
                string por3 = porcaja.Substring(160, 80);
                //agregar texto
                cb.SetTextMatrix(450, 696);
                cb.ShowText(dtFecha1.Value.ToString());
                cb.SetTextMatrix(470, 720);
                cb.ShowText("No. " + lbcorrelativo.Text);
                cb.SetTextMatrix(90, 675);
                cb.ShowText(txtRecibide1.Text);
                cb.SetTextMatrix(126, 650);
                cb.ShowText(lbtotal.Text);//cantidad de
                cb.SetTextMatrix(72, 625);
                cb.ShowText(por1);//por:
                cb.SetTextMatrix(72, 615);
                cb.ShowText(por2);
                cb.SetTextMatrix(72, 605);
                cb.ShowText(por3);
                cb.SetTextMatrix(125, 574);
                cb.ShowText(totalletras1);
                cb.SetTextMatrix(80, 533);
                cb.ShowText(txtFactura.Text);
                cb.SetTextMatrix(283, 533);
                cb.ShowText(lbtotal.Text);
                //colocar monto y total
                cb.SetTextMatrix(499, 533);
                cb.ShowText(lbtotal.Text);
                cb.SetTextMatrix(499, 516);
                cb.ShowText(lbtotal.Text);
                // colocar voucher etc 
                cb.SetTextMatrix(80, 481);
                cb.ShowText(txtBanco.Text);
                cb.SetTextMatrix(283, 481);
                cb.ShowText(txtCheque.Text);
                cb.SetTextMatrix(499, 481);
                cb.ShowText(lbtotalcheque.Text);
                cb.SetTextMatrix(499, 465);
                cb.ShowText(lbtotalcheque.Text);
                cb.SetTextMatrix(110, 463);
                cb.ShowText(txtObservaciones.Text);


                //creacion de la copia de los textos
                cb.SetTextMatrix(450, 270);
                cb.ShowText(dtFecha1.Value.ToString());
                cb.SetTextMatrix(470, 294);
                cb.ShowText("No. " + lbcorrelativo.Text);
                cb.SetTextMatrix(90, 248);
                cb.ShowText(txtRecibide1.Text);
                cb.SetTextMatrix(126, 224);
                cb.ShowText(lbtotal.Text);//cantidad de
                cb.SetTextMatrix(72, 199);
                cb.ShowText(por1);//por
                cb.SetTextMatrix(72, 189);
                cb.ShowText(por2);//por
                cb.SetTextMatrix(72, 179);
                cb.ShowText(por3);//por
                cb.SetTextMatrix(125, 146);
                cb.ShowText(totalletras1);
                cb.SetTextMatrix(80, 106);
                cb.ShowText(txtFactura.Text);
                cb.SetTextMatrix(283, 106);
                cb.ShowText(lbtotal.Text);
                //colocar monto y total COPIA
                cb.SetTextMatrix(499, 106);
                cb.ShowText(lbtotal.Text);
                cb.SetTextMatrix(499, 90);
                cb.ShowText(lbtotal.Text);
                // colocar voucher etc 
                cb.SetTextMatrix(80, 54);
                cb.ShowText(txtBanco.Text);
                cb.SetTextMatrix(283, 54);
                cb.ShowText(txtCheque.Text);
                cb.SetTextMatrix(499, 54);
                cb.ShowText(lbtotalcheque.Text);
                cb.SetTextMatrix(499, 38);
                cb.ShowText(lbtotalcheque.Text);
                cb.SetTextMatrix(110, 37);
                cb.ShowText(txtObservaciones.Text);



                cb.EndText();
                doc.Add(encabezado);
                doc.Add(recibide);
                doc.Add(recibocaja);
                doc.Add(encabezado2);
                doc.Add(recibide2);
                doc.Add(recibocaja2);

                //CREACION DE LA COPIA PDF 

                doc.Close();

                FMessageBox obj = new FMessageBox("Recibo creado con exito.", "Recibo de Caja", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conect.ingresarrecibo1(Convert.ToInt16( lbcorrelativo.Text), dtFecha1.Value);
                conect.actualizardgvrecibo1(dgvRecibo1);

                int nocorrelativo = Convert.ToInt32(lbcorrelativo.Text);
                nocorrelativo++;
                lbcorrelativo.Text = Convert.ToString(nocorrelativo);
                txtRecibide1.Clear();
                txtCantidadde1.Clear();
                txtFactura.Clear();
                txtValor.Clear();
                txtBanco.Clear();
                txtpor.Clear();
                txtCheque.Clear();
                txtvalorcheque.Clear();
                txtObservaciones.Clear();
                lbtotal.Text = "00";
                lbtotalcheque.Text = "00";
                Process.Start(ruta);
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CL1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        decimal texto1 = 1;
        decimal texto2 = 1;
        decimal textoc1 = 1;
        decimal textoc2 = 1;
        string por = " ";
        int SEGURO1 = 0;
        int SEGURO2 = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (txtValor.Text != "")
                {
                    if (SEGURO1 == 0)
                    {
                       
                        double valor = Convert.ToDouble(txtValor.Text);
                        lbtotal.Text = valor.ToString("N2");
                        

                    }
                }

                if (txtvalorcheque.Text != "")
                {
                    if (SEGURO2 == 0)
                    {
                       
                        double valorc = Convert.ToDouble(txtvalorcheque.Text);
                        lbtotalcheque.Text= valorc.ToString("N2");
                        
                       
                    }

                }
            }
            catch(Exception ex)
            {
               
            }

        }

       
        
        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            if (pdf == 1)
            {
                if (txtRecibide1.Text != "" && txtCantidadde1.Text != "" && txtValor.Text != "" && txtvalorcheque.Text != "")
                {
                    crearPdf();
                }
                else
                {
                    FMessageBox obj = new FMessageBox("LLene los campos basicos por favor", "ERROR", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            if (pdf == 2)
            {
                if (txtrecibide2.Text!="" && txtcantidadde2.Text!="" && txtpor2.Text!="" && txtvobo2.Text!="")
                {
                    crearPdf2();
                }
                else
                {
                    FMessageBox obj = new FMessageBox("LLene los campos basicos por favor", "ERROR", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
        }

       

        //RECIBO INTERNO
        public void crearPdf2()
        {
            try
            {
                int m = dtFecha2.Value.Month;
                int d = dtFecha2.Value.Day;
                int a = dtFecha2.Value.Year;
                string ff = Convert.ToString(d) + "-" + Convert.ToString(m) + "-" + Convert.ToString(a);
                string mes = "";
                switch (m)
                {
                    case 1: mes = "Enero"; break;
                    case 2: mes = "Febrero"; break;
                    case 3: mes = "Marzo"; break;
                    case 4: mes = "Abril"; break;
                    case 5: mes = "Mayo"; break;
                    case 6: mes = "Junio"; break;
                    case 7: mes = "Julio"; break;
                    case 8: mes = "Agosto"; break;
                    case 9: mes = "Septiembre"; break;
                    case 10: mes = "Octubre"; break;
                    case 11: mes = "Noviembre"; break;
                    case 12: mes = "Diciembre"; break;
                }
                string escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                string cliente = ff;
                string carpeta = escritorio + "\\" + "PM Recibos" + "\\" + "Recibos Internos" + "\\" + mes;

                if (!(Directory.Exists(carpeta)))
                {
                    Directory.CreateDirectory(carpeta);
                }


                string ruta = carpeta + "\\" + " ReciboInterno " + lbnumero.Text + ".pdf";
                float porcentaje = 0.0f;

                Document doc2 = new Document(iTextSharp.text.PageSize.A4);
                // Para bajar el contenido es el tercer valor
                // Margenes de pdf izquierda-derecha-arriba-abajo
                doc2.SetMargins(10, 10, 180, 180);
                PdfWriter escribir = PdfWriter.GetInstance(doc2, new FileStream(ruta, FileMode.Create));
                doc2.Open();
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA /*TIMES_ROMAN*/, BaseFont.CP1252, BaseFont.EMBEDDED);
                PdfContentByte cb = escribir.DirectContent;

                //medidas


                // Agregar Imagen de Encabezado al PDF
                iTextSharp.text.Image encabezado = iTextSharp.text.Image.GetInstance("Encabezadorecibo.jpg");
                encabezado.SetAbsolutePosition(50, 720);
                porcentaje = 425 / encabezado.Width;
                encabezado.ScalePercent(porcentaje * 100);

                // Agregar Imagen de recibpcaja al PDF
                iTextSharp.text.Image recibointerno = iTextSharp.text.Image.GetInstance("recibointerno.jpg");
                recibointerno.SetAbsolutePosition(25, 470);
                porcentaje = 560 / recibointerno.Width;
                recibointerno.ScalePercent(porcentaje * 100);
                //agregar imagenes de copia

                // Agregar Imagen de Encabezado al PDF
                iTextSharp.text.Image encabezado2 = iTextSharp.text.Image.GetInstance("Encabezadorecibo.jpg");
                encabezado2.SetAbsolutePosition(50, 303);
                porcentaje = 425 / encabezado2.Width;
                encabezado2.ScalePercent(porcentaje * 100);

                // Agregar Imagen de recibpcaja al PDF
                iTextSharp.text.Image recibointerno2 = iTextSharp.text.Image.GetInstance("recibointerno.jpg");
                recibointerno2.SetAbsolutePosition(25, 54);
                porcentaje = 560 / recibointerno2.Width;
                recibointerno2.ScalePercent(porcentaje * 100);

                string totalletras = conver.numerosLetras(txtcantidadde2.Text);
                //funcion para textos largos
                string porinterno = txtpor2.Text + "                                                                                                                                                                                                                                                         ...";
                string por1 = porinterno.Substring(0, 80);
                string por2 = porinterno.Substring(80, 80);
                string por3 = porinterno.Substring(160, 80);

                cb.BeginText();
                cb.SetFontAndSize(bf, 10);
                cb.SetTextMatrix(20, 435);//lineas de corte
                cb.ShowText("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");//lineas de corte
                double cantidadde = Convert.ToDouble(txtcantidadde2.Text);
                //AGREGAMOS EL TEXTO 1
                cb.SetTextMatrix(445, 706);
                cb.ShowText(dtFecha2.Value.ToString());
                cb.SetTextMatrix(498, 680);
                cb.ShowText(cantidadde.ToString("N2"));
                cb.SetTextMatrix(410, 680);
                cb.ShowText(lbnumero.Text);
                cb.SetTextMatrix(88, 656);
                cb.ShowText(txtrecibide2.Text);
                cb.SetTextMatrix(88, 637);
                cb.ShowText(txtlugar2.Text);
                cb.SetTextMatrix(128, 615);
                cb.ShowText(cantidadde.ToString("N2"));
                cb.SetTextMatrix(128, 593);
                cb.ShowText(totalletras);
                cb.SetTextMatrix(65, 573);
                cb.ShowText(por1);
                cb.SetTextMatrix(65, 563);
                cb.ShowText(por2);
                cb.SetTextMatrix(65, 553);
                cb.ShowText(por3);
                cb.SetTextMatrix(120, 532);
                cb.ShowText(txtobservaciones2.Text);
                cb.SetTextMatrix(45, 496);
                cb.ShowText(txtvobo2.Text);

                //AGREGAMOS EL TEXTO PARA COPIA
                cb.SetTextMatrix(445, 289);
                cb.ShowText(dtFecha2.Value.ToString());
                cb.SetTextMatrix(498, 265);
                cb.ShowText(cantidadde.ToString("N2"));
                cb.SetTextMatrix(410, 265);
                cb.ShowText(lbnumero.Text);
                cb.SetTextMatrix(88, 241);
                cb.ShowText(txtrecibide2.Text);
                cb.SetTextMatrix(88, 221);
                cb.ShowText(txtlugar2.Text);
                cb.SetTextMatrix(128, 199);
                cb.ShowText(cantidadde.ToString("N2"));
                cb.SetTextMatrix(128, 177);
                cb.ShowText(totalletras);
                cb.SetTextMatrix(65, 156);
                cb.ShowText(por1);
                cb.SetTextMatrix(65, 146);
                cb.ShowText(por2);
                cb.SetTextMatrix(65, 136);
                cb.ShowText(por3);
                cb.SetTextMatrix(120, 116);
                cb.ShowText(txtobservaciones2.Text);
                cb.SetTextMatrix(45, 84);
                cb.ShowText(txtvobo2.Text);

                cb.EndText();
                doc2.Add(encabezado);
                doc2.Add(encabezado2);
                doc2.Add(recibointerno);
                doc2.Add(recibointerno2);
                doc2.Close();

                FMessageBox obj = new FMessageBox("Recibo creado con exito.", "Recibo interno", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conect.ingresarrecibo2(Convert.ToInt32(lbnumero.Text), dtFecha2.Value);
                conect.actualizardgvrecibo2(dgvRecibo2);

                int nocorrelativo2 = Convert.ToInt32(lbnumero.Text);
                nocorrelativo2++;
                lbnumero.Text = Convert.ToString(nocorrelativo2);

                txtrecibide2.Clear();
                txtcantidadde2.Clear();
                txtlugar2.Clear();
                txtpor2.Clear();
                txtobservaciones2.Clear();
                txtvobo2.Clear();
                Process.Start(ruta);
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CL1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        private void FRecibos_Load(object sender, EventArgs e)
        {
            conect.actualizardgvrecibo1(dgvRecibo1);
            conect.actualizardgvrecibo2(dgvRecibo2);

            lbcorrelativo.Text = Convert.ToString(dgvRecibo1.Rows.Count + 1);// correlativo recibo caja
            lbnumero.Text = Convert.ToString(dgvRecibo2.Rows.Count + 1);// correlativo recibo interno


        }

        private void rbAbono_CheckedChanged_1(object sender, EventArgs e)
        {
            por = "ABONO";
        }

        private void rbCancelacion_CheckedChanged_1(object sender, EventArgs e)
        {
            por = "CANCELACION";

        }

        private void rbAnticipo_CheckedChanged_1(object sender, EventArgs e)
        {
            por = "ANTICIPO";

        }

        private void txtValor_KeyUp_1(object sender, KeyEventArgs e)
        {
            double c;
            if (!double.TryParse(txtValor.Text, out c))
            {
                errorP1.SetError(txtValor, "Ingrese un costo en números.");
                SEGURO1 = 1;
            }
            else
            {
                SEGURO1 = 0;
                errorP1.SetError(txtValor, "");
            }
        }

        private void txtValor2_KeyUp_1(object sender, KeyEventArgs e)
        {

        }

        private void txtValor2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtvalorcheque_KeyUp_1(object sender, KeyEventArgs e)
        {
            double c;
            if (!double.TryParse(txtvalorcheque.Text, out c))
            {
                errorP1.SetError(txtvalorcheque, "Ingrese un costo en números.");
                SEGURO2 = 1;
            }
            else
            {
                errorP1.SetError(txtvalorcheque, "");
                SEGURO2 = 0;
            }
        }

        private void txtvalorcheque2_KeyUp_1(object sender, KeyEventArgs e)
        {

        }
    }
}
