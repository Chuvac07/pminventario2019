﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FUsuarios : Form
    {
        public FUsuarios()
        {
            InitializeComponent();
            this.ActiveControl = txtUsuario;
        }
        Conexion conect = new Conexion();
        private void FUsuarios_Load(object sender, EventArgs e)
        {
            try
            {   
                conect.actualizarUsuarios(dgvUsuarios);
                this.ActiveControl = txtUsuario;
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error US2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            try
            {
                string usuario = txtUsuario.Text;
                string password = txtpassword.Text;
                string usuarioDatos = "";
                string passwordDatos = "";
                int idDatos = 0;
                string nombreDatos = "";
                string permisodatos = "";
                bool acceso = false;
                foreach(DataGridViewRow columna in dgvUsuarios.Rows)
                {
                    permisodatos = columna.Cells[6].Value.ToString();
                    usuarioDatos = columna.Cells[7].Value.ToString();
                    passwordDatos = columna.Cells[8].Value.ToString();
                    idDatos = Convert.ToInt16(columna.Cells[0].Value.ToString());
                    nombreDatos = columna.Cells[1].Value.ToString();
                    if (usuario == usuarioDatos && password == passwordDatos)
                    {
                       acceso = true;
                       Form1.nombreUsuario = nombreDatos;
                       Form1.idUsuario = idDatos;
                       Form1.permiso = permisodatos;
                        break;
                    }
                }

                if (acceso)
                {
                  
                    btnInicio.Image = pictureBox3.Image;
                    Form1.acceso = 1;
                    DialogResult = DialogResult.OK;
                    FMessageBox obj = new FMessageBox("¡Acceso Permitido!", "Login Principal", 0); 
                    if (obj.ShowDialog() == DialogResult.OK)
                    this.Close();

                   
                }
                else
                {
                        DialogResult = DialogResult.Cancel;
                        txtUsuario.Clear();
                        txtpassword.Clear();
                        this.ActiveControl = txtUsuario;
                        btnInicio.Image = pictureBox2.Image;
                        FMessageBox obj = new FMessageBox("¡Acceso Denegado!", "Login Principal", 1);
                        if (obj.ShowDialog() == DialogResult.OK) ;
                        
                    
                }
               
            }
            catch(Exception ex)
            {

                FMessageBox obj = new FMessageBox("Error US1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
    }
}
