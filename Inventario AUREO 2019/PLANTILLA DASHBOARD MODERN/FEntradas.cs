﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FEntradas : Form
    {
        public FEntradas()
        {
            InitializeComponent();
        }

        // variables globales para recibir los valores del FORM CLIENTES
        public static string idProveedor = "";
        public static string empresaProveedor = "";
        public static string nitProveedor = "";
        public static string telefonoProveedor = "";
        public static string emailProveedor = "";
        public static string contactosProveedor = ""; 

        Conexion conect = new Conexion();
        Conversion convers = new Conversion();

        public static int informacionEntrada = 0; 
        // LOAD DONDE SE ACTUALIZA EL DATAGRIDVIEW DE TODOS LOS PRODUCTOS DEL INVENTARIO
        private void FEntradas_Load(object sender, EventArgs e)
        {
            if (informacionEntrada == 1)
            {
                idProveedor = "";
                empresaProveedor = "";
                nitProveedor = "";
                telefonoProveedor = "";
                emailProveedor = "";
                contactosProveedor = "";
                informacionEntrada = 0; 
            }
            //PARA DEFINIR FECHA DE ALERTA
            DateTime fechaAlerta;
            fechaAlerta = dtFechaAlerta.Value;
            fechaAlerta = fechaAlerta.AddDays(30);
            dtFechaAlerta.Value = fechaAlerta;

            lbEmpresa.Text = empresaProveedor;
            lbnit.Text = nitProveedor;
            Lbtelefono.Text = telefonoProveedor;
            lbemail.Text = emailProveedor;
            txtCyT.Text = contactosProveedor;
            conect.actualizarNoReportesEn(dgvReportes);
            lbEntrada.Text = Convert.ToString(conect.generarN(dgvReportes));
            if (lbEmpresa.Text != "")
            {
                gbEntradas.Visible = true;
                btnBuscarProveedor.Enabled = false;
                btnBuscarP.Visible = true;
                this.ActiveControl = btnBuscarP;
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarTextbox())
                {
                    int noP = conect.generarN(dgvProductos);
                    int cantidadP = Convert.ToInt16(txtCantidadP1.Text);
                    double costoP = Convert.ToDouble(Ecosto);
                    double subtotal = cantidadP * costoP;
                    bool repetido = false; 

                    foreach(DataGridViewRow fila in dgvProductos.Rows)
                    {
                        if (Eid == fila.Cells[6].Value.ToString())
                        {
                            int cantidadPro = Convert.ToInt16(fila.Cells[1].Value);
                            fila.Cells[1].Value = cantidadPro + cantidadP;
                            fila.Cells[5].Value = Convert.ToDouble((cantidadPro + cantidadP)*costoP);
                            repetido = true;
                            break;
                        }
                    }

                    if (!repetido)
                        dgvProductos.Rows.Add(noP,cantidadP,Ecodigo,Edescripcion,Ecosto,subtotal,Eid,Eexistencia,Ecategorias,Emarca);
                    
                    double totalReporte = conect.generarTotal(dgvProductos, 5);
                    lbTotal1.Text = totalReporte.ToString("N2");

                    txtCantidadP1.Clear();
                    lbCodigoP1.Text = "";
                    lbDescripcionP1.Text = "";
                    lbExistenciaP1.Text = "";
                    lbCosto.Text = "";

                    Eid = "";
                    Eexistencia = "";
                    Ecodigo = "";
                    Edescripcion = "";
                    Ecosto = "";
                    Ecategorias = "";
                    Emarca = ""; 

                    btnFinalizar.Visible = true;
                    btnAgregar.Visible = false;
                    btnEliminar.Visible = true;
                    txtCantidadP1.Enabled = false;
                    txtAcreditar.Enabled = true;
                    txtFactura.Enabled = true;
                    cbpago.Enabled = true; 
                    Form1.acceso = 10; 
    }
                else
                {
                    FMessageBox obj = new FMessageBox("Se deben ingresar el campo solicitado.", "ERROR EN1:", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error EN1: " + ex.Message, "¡Alerta de conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }


        }
        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
        
        private bool validarTextbox()
        {
            bool estado = true; 
            
            if (txtCantidadP1.Text == "")
            {
                errorP1.SetError(txtCantidadP1, "Ingrese una cantidad valida en números.");
                estado = false;
            }
            if (Convert.ToInt16(txtCantidadP1.Text) <= 0)
            {
                errorP1.SetError(txtCantidadP1, "La cantidad debe ser mayor a 0.");
                estado = false;
            }
            return estado; 
        }

        private bool validarTextbox2()
        {
            bool estado = true;

            if (txtAcreditar.Text == "")
            {
                errorP1.SetError(txtAcreditar, "Ingrese un valor valida la acreditación.");
                estado = false;
            }
            if (txtFactura.Text == "")
            {
                errorP1.SetError(txtFactura, "Ingrese el número de factura.");
                estado = false;
            }
            if (cbpago.Text == "")
            {
                errorP1.SetError(cbpago, "Seleccione un método de pago.");
                estado = false;
            }
            if (Convert.ToDouble(txtAcreditar.Text) > Convert.ToDouble(lbTotal1.Text))
            {
                errorP1.SetError(txtAcreditar, "La cantidad debe ser menor o igual al total final.");
                estado = false;
            }
            if (Convert.ToDouble(txtAcreditar.Text) < 0)
            {
                errorP1.SetError(txtAcreditar, "La cantidad debe ser mayor o igual a cero.");
                estado = false;
            }
            return estado;
        }
        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            { 
                // Se generan los reportes para almacenar las ENTRADAS en SQL
                if (validarTextbox2())
                {
                    double restanteEntrada = Convert.ToDouble(lbTotal1.Text) - Convert.ToDouble(txtAcreditar.Text);
                    double totalEntrada = Convert.ToDouble(lbTotal1.Text);
                    double acreditarEntrada = Convert.ToDouble(txtAcreditar.Text);
                    conect.ingresarReportesEntradas(lbEntrada.Text, totalEntrada.ToString(), acreditarEntrada.ToString(), restanteEntrada.ToString(), cbpago.Text, dtfecha1.Value.ToString(), idProveedor,dtFechaAlerta.Value.ToString());
                    
                    foreach (DataGridViewRow fila in dgvProductos.Rows)
                    {
                        string idPro = fila.Cells[6].Value.ToString();
                        int canPro = Convert.ToInt16(fila.Cells[1].Value);
                        int exiPro = Convert.ToInt16(fila.Cells[7].Value);
                        string noPro = fila.Cells[0].Value.ToString();
                        string codigoPro = fila.Cells[2].Value.ToString();
                        string descripPro = fila.Cells[3].Value.ToString();
                        string costoPro = fila.Cells[4].Value.ToString();
                        string subtoPro = fila.Cells[5].Value.ToString();
                        string cateforiaPro = fila.Cells[8].Value.ToString();
                        string marcaPro = fila.Cells[9].Value.ToString();

                        // Se enlazan los productos a las ENTRADAS en SQL
                        conect.ingresarReportesEntradasProducto(lbEntrada.Text, codigoPro, canPro.ToString(), descripPro,cateforiaPro, marcaPro, costoPro);
                        // Se modifica la cantidad de productos existentes
                        conect.modificarProductoCantidad(idPro, (canPro + exiPro));
                        // Se agregan los nuevos valores al dgvImprimir
                        dgvImprimir.Rows.Add(noPro, canPro, codigoPro, descripPro, costoPro, subtoPro);
                    }

                    // Informacion de la empresa a guardar
                    DataGridViewRow fila1 = new DataGridViewRow();
                    DataGridViewRow fila2 = new DataGridViewRow();
                    DataGridViewRow fila3 = new DataGridViewRow();
                    DataGridViewRow fila4 = new DataGridViewRow();
                    DataGridViewRow fila5 = new DataGridViewRow();
                    DataGridViewRow fila6 = new DataGridViewRow();
                    DataGridViewRow fila7 = new DataGridViewRow();
                    DataGridViewRow fila8 = new DataGridViewRow();

                    fila1.CreateCells(dgvDatos);
                    fila1.Cells[0].Value = "EMPRESA: ";
                    fila1.Cells[1].Value = lbEmpresa.Text;
                    dgvDatos.Rows.Add(fila1);

                    fila2.CreateCells(dgvDatos);
                    fila2.Cells[0].Value = "NIT: ";
                    fila2.Cells[1].Value = lbnit.Text;
                    dgvDatos.Rows.Add(fila2);

                    fila3.CreateCells(dgvDatos);
                    fila3.Cells[0].Value = "TELÉFONO: ";
                    fila3.Cells[1].Value = Lbtelefono.Text;
                    dgvDatos.Rows.Add(fila3);

                    fila4.CreateCells(dgvDatos);
                    fila4.Cells[0].Value = "E-MAIL: ";
                    fila4.Cells[1].Value = lbemail.Text;
                    dgvDatos.Rows.Add(fila4);

                    fila5.CreateCells(dgvDatos);
                    fila5.Cells[0].Value = "CONTACTO: ";
                    fila5.Cells[1].Value = txtCyT.Text;
                    dgvDatos.Rows.Add(fila5);

                    fila6.CreateCells(dgvTotal);
                    double totalDecimal = Convert.ToDouble(lbTotal1.Text);
                    fila6.Cells[0].Value = "Total en Letras: ";
                    fila6.Cells[1].Value = convers.numerosLetras(totalDecimal.ToString());
                    dgvTotal.Rows.Add(fila6);

                    fila7.CreateCells(dgvTotal);
                    fila7.Cells[0].Value = "No. de Factura:";
                    fila7.Cells[1].Value = txtFactura.Text;
                    dgvTotal.Rows.Add(fila7);

                    fila8.CreateCells(dgvTotal);
                    fila8.Cells[0].Value = "Atendido por: ";
                    fila8.Cells[1].Value = Form1.nombreUsuario;
                    dgvTotal.Rows.Add(fila8);

                    exportarPdf(lbEntrada.Text);

                    // Se empieza a limpiar todo el formulairo para poder ingresar otro. 
                    dgvProductos.Rows.Clear();
                    Form1.acceso = 11;

                    idProveedor = "";
                    empresaProveedor = "";
                    nitProveedor = "";
                    telefonoProveedor = "";
                    emailProveedor = "";
                    contactosProveedor = "";

                    lbEmpresa.Text = "";
                    lbnit.Text = "";
                    Lbtelefono.Text = "";
                    lbemail.Text = "";
                    txtCyT.Clear();
                    gbEntradas.Visible = false;
                    btnBuscarProveedor.Enabled = true;

                    btnFinalizar.Visible = false;
                    btnAgregar.Visible = false;
                    btnEliminar.Visible = false;
                    btnBuscarP.Visible = false;
                } 
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error EN2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        
        // Funcion para generar los PDF 
        public void exportarPdf(string numeroE)
        {
            int m = dtfecha1.Value.Month;
            int d = dtfecha1.Value.Day;
            int a = dtfecha1.Value.Year;
            string ff = Convert.ToString(d) + "-" + Convert.ToString(m) + "-" + Convert.ToString(a);
            
            string escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string carpeta = escritorio + "\\" + "PM Inventario";
            // string carpeta = @"C:\Proyectos Multiples\" + mes;
            string subcarpeta = carpeta + "\\Entrada de Producto";

            if (!(Directory.Exists(carpeta)))
            {
                Directory.CreateDirectory(carpeta);
            }
            if (!(Directory.Exists(subcarpeta)))
            {
                Directory.CreateDirectory(subcarpeta);
            }


            string ruta = subcarpeta + "\\" + numeroE + " Reporte " + ff + ".pdf";
            float porcentaje = 0.0f;

            Document doc = new Document(iTextSharp.text.PageSize.A4);
            
            // Para bajar el contenido es el tercer valor
            // Margenes de pdf izquierda-derecha-arriba-abajo
            doc.SetMargins(5, 5, 110, 10);
            PdfWriter escribir = PdfWriter.GetInstance(doc, new FileStream(ruta, FileMode.Create));
            doc.Open();

            // Agregar Imagen de Encabezado al PDF
            iTextSharp.text.Image encabezado = iTextSharp.text.Image.GetInstance("EncabezadoCompras.jpg");
            encabezado.SetAbsolutePosition(50, 740);
            porcentaje = 500 / encabezado.Width;
            encabezado.ScalePercent(porcentaje * 100);


            // Agregar la DATAGRIDVIEW al PDF
            PdfPTable pdf = new PdfPTable(dgvImprimir.Columns.Count);
            PdfPTable pdf2 = new PdfPTable(dgvDatos.Columns.Count);
            PdfPTable pdf3 = new PdfPTable(dgvTotal.Columns.Count);

            //                              Es para el tipo de fuente
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
            PdfContentByte cb = escribir.DirectContent;

            float[] ancho = new float[6] { 2f, 2f, 3f, 12f, 5f, 5f };
            pdf.SetWidths(ancho);

            float[] ancho2 = new float[2] { 3f, 17f };
            pdf2.SetWidths(ancho2);

            float[] ancho3 = new float[2] { 5f, 15f };
            pdf3.SetWidths(ancho3);

            //pdf.HorizontalAlignment = Element.ALIGN_CENTER;
            pdf.DefaultCell.Padding = 2;

            iTextSharp.text.Font text1 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
            iTextSharp.text.Font text2 = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font text3 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL, BaseColor.GRAY);
            iTextSharp.text.Font text4 = new iTextSharp.text.Font(bf, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            // Informacion de filas del Proveedor Empresa-Nit-Telefono-Email-Contacto
            foreach (DataGridViewRow row in dgvDatos.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdf2.DefaultCell.BorderWidth = 0;
                    pdf2.DefaultCell.BackgroundColor = new iTextSharp.text.BaseColor(Color.White);//227, 233, 252);
                    pdf2.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(20, 32, 40);
                    pdf2.AddCell(new Phrase(cell.Value.ToString(), text4));
                }
            }

            // Agregar intermedio blanco para separar el producto
            foreach (DataGridViewColumn columna in dgvDatos.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(Color.White);
                cell.BorderWidth = 0;
                pdf2.AddCell(cell);
            }
            
          
            // Agregar encabezado
            foreach (DataGridViewColumn columna in dgvImprimir.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 1;
                pdf.AddCell(cell);
            }

            // Informacion de filas
            foreach (DataGridViewRow row in dgvImprimir.Rows)
            {
                int i = 0;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    i++;
                    if (i % 5 == 0 || i % 6 == 0)
                    {
                        double totalDecimal = Convert.ToDouble(cell.Value); 
                        pdf.AddCell(new Phrase("Q. " + totalDecimal.ToString("N2"), text2));
                    }
                    else
                        pdf.AddCell(new Phrase(cell.Value.ToString(), text2));
                }
            }

            iTextSharp.text.Font text5 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            // Para colocar el final hasta abajo
            pdf.DefaultCell.BorderWidthLeft = 1;
            pdf.DefaultCell.BorderWidthRight = 0;
            pdf.AddCell(new Phrase(" ", text5));
            pdf.DefaultCell.BorderWidthLeft = 0;
            pdf.AddCell(new Phrase(" ", text5));
            pdf.AddCell(new Phrase(" ", text5));
            pdf.AddCell(new Phrase(" ", text5));
            pdf.DefaultCell.BorderWidthRight = 1;
            pdf.AddCell(new Phrase("  TOTAL", text5));
            double totalEscribir = Convert.ToDouble(lbTotal1.Text);
            pdf.AddCell(new Phrase("Q. " + totalEscribir.ToString("N2"), text5));
            pdf.DefaultCell.BorderWidthRight = 0;

            // Agregar para dejar un espacio en blanco XD
            foreach (DataGridViewColumn columna in dgvTotal.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(Color.White);
                cell.BorderWidthRight = 0;
                cell.BorderWidthLeft = 0;
                pdf3.AddCell(cell);
            }

            // Informacion de filas sobre el total, observaciones.
            foreach (DataGridViewRow row in dgvTotal.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdf3.DefaultCell.BorderWidth = 0;
                    pdf3.DefaultCell.BorderColor = new iTextSharp.text.BaseColor(Color.White);
                    pdf3.AddCell(new Phrase(cell.Value.ToString(), text3));
                }
            }
            // FIN de la tabla

            

            // Agregar objetos al PDF

            doc.Add(encabezado);
            doc.Add(pdf2);
            doc.Add(pdf);
            doc.Add(pdf3);

            // Agregar titulos
            cb.BeginText();
            //  bf.FontType()
            cb.SetFontAndSize(bf, 9);
            cb.SetTextMatrix(350, 710);
            cb.ShowText("Fecha: " + dtfecha1.Value.ToString());
            cb.SetTextMatrix(350, 720);
            cb.ShowText("Reporte No. " + numeroE);
            cb.SetTextMatrix(350, 700);
            cb.ShowText("Forma de pago: " + cbpago.Text);
            cb.EndText();

            doc.Close();

            Process.Start(ruta);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                FMessageBoxP obj = new FMessageBoxP("¿Está seguro en eliminar el producto seleccionado?", "¡Alerta de eliminación!", 1);
                if (obj.ShowDialog() == DialogResult.OK)
                {
                    dgvProductos.Rows.RemoveAt(dgvProductos.CurrentRow.Index);
                }
                if (dgvProductos.RowCount <= 0)
                {
                    btnEliminar.Visible = false;
                    btnFinalizar.Visible = false;
                    btnAgregar.Visible = false;
                    txtAcreditar.Enabled = false;
                    cbpago.Enabled = false; 
                    Form1.acceso = 11;
                }
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Verifique el dato a eliminar", "¡Alerta de eliminación!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            if (lbEmpresa.Text != "")
            {
                gbEntradas.Visible = true;
            }
            Form1.acceso = 5;
        }

        public static string Eid = "";
        public static string Eexistencia = "";
        public static string Ecodigo = "";
        public static string Edescripcion = "";
        public static string Ecosto = "";
        public static string Ecategorias = "";
        public static string Emarca = "";

        private void btnBuscarP_Click(object sender, EventArgs e)
        {
            FInventario obj = new FInventario(1);
            FInventario.formulariestado = 1;
            if (obj.ShowDialog() == DialogResult.OK)
            {
                lbExistenciaP1.Text = Eexistencia;
                lbCodigoP1.Text = Ecodigo;
                lbDescripcionP1.Text = Edescripcion;
                lbCosto.Text = Ecosto; 
                btnAgregar.Visible = true;
                txtCantidadP1.Enabled = true; 
                this.ActiveControl = txtCantidadP1;
            }
        }

        private void txtCantidadP1_KeyUp(object sender, KeyEventArgs e)
        {
            int c;
            if (!int.TryParse(txtCantidadP1.Text, out c))
                errorP1.SetError(txtCantidadP1, "Ingrese un cantidad en números.");
            else
            {
                int vcantidad = Convert.ToInt16(txtCantidadP1.Text);
                if (vcantidad < 1)
                    errorP1.SetError(txtCantidadP1, "la cantidad debe ser mayor a 0");
                else
                    errorP1.SetError(txtCantidadP1, "");
            }
        }

        private void FEntradas_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void cbpago_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorP1.SetError(cbpago, "");
        }

        private void txtAcreditar_KeyUp(object sender, KeyEventArgs e)
        {
            double c;
            if (!double.TryParse(txtAcreditar.Text, out c))
                errorP1.SetError(txtAcreditar, "Ingrese un cantidad en números.");
            else
            {
                double vpago = Convert.ToDouble(txtAcreditar.Text);
                if (vpago <= Convert.ToDouble(lbTotal1.Text))
                   errorP1.SetError(txtAcreditar, "");
            }
        }

        private void txtFactura_KeyUp(object sender, KeyEventArgs e)
        {
            errorP1.SetError(txtAcreditar, "");
        }
    }
}
