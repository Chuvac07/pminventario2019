﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;
using System.Collections;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FComprasVentas : Form
    {
        public FComprasVentas()
        {
            InitializeComponent();
        }

        private void FComprasVentas_Load(object sender, EventArgs e)
        {
            dtpInicio.Value = Convert.ToDateTime("1/" + dtpFechaHoy.Value.Month + "/" + dtpFechaHoy.Value.Year); ;
            funcionBusqueda();
            funcionGrafica();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnCompras_Click(object sender, EventArgs e)
        {
            bandera = true;
            bandera2 = false; 
            lbBusqueda.Text = "Busqueda Proveedor:";
            txtBusqueda.Clear();
            lbEncabezado.Text = "CONTROL DE COMPRAS";
            btnReporte.Visible = true;
            btnReporte1.Visible = false;
            lbGanancia.Text = "Restante";
            funcionBusqueda();

            btnCompras.BackColor = Color.Turquoise;
            btnVentas.BackColor = Color.FromArgb(30, 30, 46);
            btnAnuladas.BackColor = Color.FromArgb(30, 30, 46);
            btnAlterar.BackColor = Color.FromArgb(30, 30, 46);
            dgvSalidas.Visible = false;
            dgvEntradas.Visible = true;
            dgvAnuladas.Visible = false;
            dgvModificadas.Visible = false; 
            lbTotal.Visible = txtTotal.Visible = true;
            lbDebito.Visible = txtDebito.Visible = true;
            lbGanancia.Visible = txtGanancia.Visible = true;
        }


        Conexion conect = new Conexion();
        bool bandera = true;
        bool bandera2 = false; 
        private void btnVentas_Click(object sender, EventArgs e)
        {
            bandera = false;
            bandera2 = false; 
            lbBusqueda.Text = "Busqueda Contacto:";
            txtBusqueda.Clear();
            lbEncabezado.Text = "CONTROL DE VENTAS";
            btnReporte.Visible = true;
            btnReporte1.Visible = false;
            lbGanancia.Text = "Ganancia";
            funcionBusqueda();

            btnCompras.BackColor = Color.FromArgb(30, 30, 46);
            btnVentas.BackColor = Color.FromArgb(30, 175, 75);
            btnAlterar.BackColor = Color.FromArgb(30, 30, 46);
            btnAnuladas.BackColor = Color.FromArgb(30, 30, 46);
            dgvSalidas.Visible = true;
            dgvEntradas.Visible = false;
            dgvAnuladas.Visible = false;
            dgvModificadas.Visible = false; 
            lbTotal.Visible = txtTotal.Visible = true;
            lbDebito.Visible = txtDebito.Visible = true;
            lbGanancia.Visible = txtGanancia.Visible = true;
        }

        private void funcionBusqueda()
        {
            conect.actualizarReportesEntradas(dgvEntradas, dtpInicio.Value.ToString("dd/MM/yyyy"), dtpFinal.Value.ToString("dd/MM/yyyy"), txtBusqueda);
            conect.actualizarReportesSalidas(dgvSalidas, dtpInicio.Value.ToString("dd/MM/yyyy"), dtpFinal.Value.ToString("dd/MM/yyyy"), txtBusqueda);
            conect.actualizarVentasAnuladas(dgvAnuladas, dtpInicio.Value.ToString("dd/MM/yyyy"), dtpFinal.Value.ToString("dd/MM/yyyy"), txtBusqueda);
            conect.actualizarProductoModificado(dgvModificadas, dtpInicio.Value.ToString("dd/MM/yyyy"), dtpFinal.Value.ToString("dd/MM/yyyy"), txtBusqueda);
            dgvEntradas.Columns[9].Visible = false;
            if (bandera)
            {
                txtTotal.Text = conect.generarTotal(dgvEntradas, 4).ToString("N2");
                txtDebito.Text = conect.generarTotal(dgvEntradas, 5).ToString("N2");
                txtGanancia.Text = conect.generarTotal(dgvEntradas, 6).ToString("N2");
            }
            else
            {
                foreach(DataGridViewRow fila in dgvSalidas.Rows)
                {
                    int porcentaje = Convert.ToInt16(Convert.ToDouble(fila.Cells[5].Value) / Convert.ToDouble(fila.Cells[4].Value) * 100);
                    double ganaciaFija = Convert.ToDouble(fila.Cells[6].Value) * porcentaje / 100;
                    fila.Cells[6].Value = ganaciaFija.ToString("N2");
                }

                txtTotal.Text = conect.generarTotal(dgvSalidas, 4).ToString("N2");
                txtDebito.Text = conect.generarTotal(dgvSalidas, 5).ToString("N2");
                txtGanancia.Text = conect.generarTotal(dgvSalidas, 6).ToString("N2");
            }
            if (bandera2)
            {
                txtTotal.Text = conect.generarTotal(dgvAnuladas, 3).ToString("N2");
            }
        }
        private void funcionGrafica()
        {
            ArrayList estado = new ArrayList();
            ArrayList cant = new ArrayList();
            estado.Add("Compras\nProveedores"); cant.Add(conect.generarTotal(dgvEntradas, 4));
            estado.Add("Ventas"); cant.Add(conect.generarTotal(dgvSalidas, 4));
            estado.Add("Ganancias"); cant.Add(conect.generarTotal(dgvSalidas, 6));
            chart3.Series[0].Points.DataBindXY(estado, cant);
        }

        private void txtBusqueda_KeyUp(object sender, KeyEventArgs e)
        {
            funcionBusqueda();
        }

        private void dtpInicio_ValueChanged(object sender, EventArgs e)
        {
            funcionBusqueda();
            funcionGrafica();
        }

        private void dtpFinal_ValueChanged(object sender, EventArgs e)
        {
            funcionBusqueda();
            funcionGrafica();
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            dgvEntradas.Columns.RemoveAt(9);
            if (bandera)
                exportarPdf(dgvEntradas);
            else
                exportarPdf(dgvSalidas);
            funcionBusqueda();
        }

        // Funcion para generar los PDF 
        public void exportarPdf(DataGridView Tabla)
        {
            string ff = dtpFechaHoy.Value.ToString("dd-MM-yyyy HH-mm-ss");

            string escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string carpeta = escritorio + "\\" + "PM Inventario";
            // string carpeta = @"C:\Proyectos Multiples\" + mes;
            string subcarpeta = carpeta + "\\Reporte Administrativo";

            if (!(Directory.Exists(carpeta)))
            {
                Directory.CreateDirectory(carpeta);
            }
            if (!(Directory.Exists(subcarpeta)))
            {
                Directory.CreateDirectory(subcarpeta);
            }


            string ruta = subcarpeta + "\\Reporte " + ff + ".pdf";
            float porcentaje = 0.0f;

            Document doc = new Document(iTextSharp.text.PageSize.A4);

            // Para bajar el contenido es el tercer valor
            // Margenes de pdf izquierda-derecha-arriba-abajo
            doc.SetMargins(5, 5, 130, 10);
            PdfWriter escribir = PdfWriter.GetInstance(doc, new FileStream(ruta, FileMode.Create));
            doc.Open();

            // Agregar Imagen de Encabezado al PDF
            iTextSharp.text.Image encabezado;
            if (bandera)
                 encabezado = iTextSharp.text.Image.GetInstance("EncabezadoCompras.jpg");
            else
                encabezado = iTextSharp.text.Image.GetInstance("EncabezadoVentas.jpg");

            encabezado.SetAbsolutePosition(50, 740);
            porcentaje = 500 / encabezado.Width;
            encabezado.ScalePercent(porcentaje * 100);
            doc.Add(encabezado);

            // Agregar la DATAGRIDVIEW al PDF
            PdfPTable pdf =  new PdfPTable(Tabla.Columns.Count);
            PdfPTable pdfU = new PdfPTable(dgvUsuario.Columns.Count);
            // Es para el tipo de fuente
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
            PdfContentByte cb = escribir.DirectContent;

            // Agregar titulos
            cb.BeginText();
            //  bf.FontType()
            cb.SetFontAndSize(bf, 9);
            cb.SetTextMatrix(70, 730);
            cb.ShowText("Fecha Inicial: " + dtpInicio.Value.ToString("dd/MM/yyyy") + "          Fecha Final: " + dtpFinal.Value.ToString("dd/MM/yyyy"));
            cb.EndText();

            float[] ancho;
            if (bandera)
               ancho = new float[9] { 2f, 6f, 6f, 5f, 6f, 6f, 6f ,5f, 5f };
            else
                ancho = new float[8] { 2f, 5f, 5f, 5f, 5f, 5f, 5f, 5f };

            pdf.SetWidths(ancho);

            float[] anchoU = new float[1] { 25f };
            pdfU.SetWidths(anchoU);

            //pdf.HorizontalAlignment = Element.ALIGN_CENTER;
            pdf.DefaultCell.Padding = 2;

            iTextSharp.text.Font text1 = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
            iTextSharp.text.Font text2 = new iTextSharp.text.Font(bf, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            
            // Agregar encabezado
            foreach (DataGridViewColumn columna in Tabla.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 1;
                pdf.AddCell(cell);
            }

            // Informacion de filas
            foreach (DataGridViewRow row in Tabla.Rows)
            {
                int i = 0;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    i++;

                    if (i % 5 == 0 || i % 6 == 0 || i % 7 == 0)
                    {
                        double totalDecimal = Convert.ToDouble(cell.Value);
                        pdf.AddCell(new Phrase("Q. " + totalDecimal.ToString("N2"), text2));
                    }
                    else
                    {
                        pdf.AddCell(new Phrase(cell.Value.ToString(), text2));
                    }
                }
            }
            
            iTextSharp.text.Font text5 = new iTextSharp.text.Font(bf, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            // Para colocar el final hasta abajo
            pdf.DefaultCell.BorderWidthLeft = 1;
            pdf.DefaultCell.BorderWidthRight = 0;
            pdf.AddCell(new Phrase(" ", text5)); // 0
            pdf.DefaultCell.BorderWidthLeft = 0;  
            pdf.AddCell(new Phrase(" ", text5)); // 1
            pdf.AddCell(new Phrase(" ", text5)); // 2
            pdf.DefaultCell.BorderWidthRight = 1;
            pdf.AddCell(new Phrase(" TOTAL", text5));  //3
            double totalEscribir = Convert.ToDouble(txtTotal.Text);
            pdf.AddCell(new Phrase("Q. " + totalEscribir.ToString("N2"), text5)); // 4
            totalEscribir = Convert.ToDouble(txtDebito.Text);
            pdf.AddCell(new Phrase("Q. " + totalEscribir.ToString("N2"), text5)); // 5

            if (bandera)
            {
                totalEscribir = Convert.ToDouble(txtGanancia.Text);
                pdf.AddCell(new Phrase("Q. " + totalEscribir.ToString("N2"), text5)); //6
                pdf.AddCell(new Phrase(" ", text5));//7
            }
            else
            { 
                totalEscribir = Convert.ToDouble(txtGanancia.Text);
                pdf.AddCell(new Phrase("Q. " + totalEscribir.ToString("N2"), text5)); //6
            }
            pdf.AddCell(new Phrase(" ", text5));//7
            pdf.DefaultCell.BorderWidthRight = 0;
            // Agregar objetos al PDF
            doc.Add(pdf);

            // Agregar al Usuario que lo genero
            pdfU.DefaultCell.BorderWidth = 0; 
            pdfU.AddCell(new Phrase(" ", text5)); // 0
            pdfU.AddCell(new Phrase(" ", text5)); // 1
            pdfU.DefaultCell.BorderWidthTop = 1; 
            pdfU.AddCell(new Phrase("Generado por: " + Form1.nombreUsuario, text5)); // 2
            doc.Add(pdfU);

            // FIN de la tabla
            doc.Close();

            Process.Start(ruta);
        }

        private void dgvEntradas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            FAgregarPago.noRep = dgvEntradas.SelectedCells[0].Value.ToString();
            FAgregarPago.proveedor = dgvEntradas.SelectedCells[1].Value.ToString();
            FAgregarPago.contacto = dgvEntradas.SelectedCells[2].Value.ToString();
            FAgregarPago.telefono = dgvEntradas.SelectedCells[3].Value.ToString();
            FAgregarPago.total = dgvEntradas.SelectedCells[4].Value.ToString();
            FAgregarPago.abono = dgvEntradas.SelectedCells[5].Value.ToString();
            FAgregarPago.restante = dgvEntradas.SelectedCells[6].Value.ToString();
            FAgregarPago.credito = dgvEntradas.SelectedCells[7].Value.ToString();
            FAgregarPago.fecha = dgvEntradas.SelectedCells[8].Value.ToString();
            FAgregarPago.fechaalerta = dgvEntradas.SelectedCells[9].Value.ToString();
            FAgregarPago obj = new FAgregarPago();
            obj.ShowDialog();
            funcionBusqueda();
        }

        private void dgvSalidas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            FAgregarPago.noRep = dgvSalidas.SelectedCells[0].Value.ToString();
            FAgregarPago.contacto = dgvSalidas.SelectedCells[1].Value.ToString();
            FAgregarPago.empresa = dgvSalidas.SelectedCells[2].Value.ToString();
            FAgregarPago.telefono = dgvSalidas.SelectedCells[3].Value.ToString();
            FAgregarPago.total = dgvSalidas.SelectedCells[4].Value.ToString();
            FAgregarPago.abono = dgvSalidas.SelectedCells[5].Value.ToString();
            FAgregarPago.ganancia = dgvSalidas.SelectedCells[6].Value.ToString();
            FAgregarPago.fecha = dgvSalidas.SelectedCells[7].Value.ToString();
            FAgregarPago.fechaalerta = dgvSalidas.SelectedCells[7].Value.ToString();
            FAgregarPago.accion = false; 

            FAgregarPago obj = new FAgregarPago();
            obj.ShowDialog();
            funcionBusqueda();
            FAgregarPago.accion = true;
        }

        private void btnAnuladas_Click(object sender, EventArgs e)
        {
            lbBusqueda.Text = "Busqueda Contacto:";
            txtBusqueda.Clear(); 
            lbEncabezado.Text = "CONTROL DE VENTAS ANULADAS";
            btnReporte1.Visible = true;
            btnReporte.Visible = false;
            bandera2 = true; 
            funcionBusqueda();

            btnCompras.BackColor = Color.FromArgb(30, 30, 46);
            btnVentas.BackColor = Color.FromArgb(30, 30, 46);
            btnAlterar.BackColor = Color.FromArgb(30, 30, 46);
            btnAnuladas.BackColor = Color.Red;
            dgvSalidas.Visible = false;
            dgvEntradas.Visible = false;
            dgvModificadas.Visible = false;
            dgvAnuladas.Visible = true; 
            lbTotal.Visible = txtTotal.Visible = true;
            lbDebito.Visible = txtDebito.Visible = false;
            lbGanancia.Visible = txtGanancia.Visible = false;
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            lbBusqueda.Text = "Busqueda Producto:";
            txtBusqueda.Clear();
            lbEncabezado.Text = "CONTROL DE MODIFICACIONES INVENTARIO";
            btnReporte.Visible = false;
            btnReporte1.Visible = true;
            bandera2 = false;
            funcionBusqueda();

            btnCompras.BackColor = Color.FromArgb(30, 30, 46);
            btnVentas.BackColor = Color.FromArgb(30, 30, 46);
            btnAlterar.BackColor = Color.OrangeRed;
            btnAnuladas.BackColor = Color.FromArgb(30, 30, 46);
            dgvSalidas.Visible = false;
            dgvEntradas.Visible = false;
            dgvModificadas.Visible = true;
            dgvAnuladas.Visible = false;
            lbTotal.Visible = txtTotal.Visible = false;
            lbDebito.Visible = txtDebito.Visible = false;
            lbGanancia.Visible = txtGanancia.Visible = false;
        }

        private void btnReporte1_Click(object sender, EventArgs e)
        {
            if (bandera2)
                exportarPdf1(dgvAnuladas);
            else
            {
                dgvModificadas.Columns[1].HeaderText = "Co.";
                dgvModificadas.Columns[3].HeaderText = "Exis.";
                dgvModificadas.Columns[5].HeaderText = "Precio 1";
                dgvModificadas.Columns[6].HeaderText = "Precio 2";
                dgvModificadas.Columns[7].HeaderText = "Precio 3";
                exportarPdf1(dgvModificadas);
                dgvModificadas.Columns[1].HeaderText = "Código";
                dgvModificadas.Columns[3].HeaderText = "Existencia";
                dgvModificadas.Columns[5].HeaderText = "Precio Mínimo";
                dgvModificadas.Columns[6].HeaderText = "Precio Medio";
                dgvModificadas.Columns[7].HeaderText = "Precio Máximo";
            }
        }

        public void exportarPdf1(DataGridView Tabla)
        {
            string ff = dtpFechaHoy.Value.ToString("dd-MM-yyyy HH-mm-ss");

            string escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string carpeta = escritorio + "\\" + "PM Inventario";
            // string carpeta = @"C:\Proyectos Multiples\" + mes;
            string subcarpeta = carpeta + "\\Reporte Administrativo";

            if (!(Directory.Exists(carpeta)))
            {
                Directory.CreateDirectory(carpeta);
            }
            if (!(Directory.Exists(subcarpeta)))
            {
                Directory.CreateDirectory(subcarpeta);
            }


            string ruta = subcarpeta + "\\Reporte " + ff + ".pdf";
            float porcentaje = 0.0f;

            Document doc = new Document(iTextSharp.text.PageSize.A4);

            // Para bajar el contenido es el tercer valor
            // Margenes de pdf izquierda-derecha-arriba-abajo
            doc.SetMargins(5, 5, 130, 10);
            PdfWriter escribir = PdfWriter.GetInstance(doc, new FileStream(ruta, FileMode.Create));
            doc.Open();

            // Agregar Imagen de Encabezado al PDF
            iTextSharp.text.Image encabezado ;
            if (bandera2)
                encabezado = iTextSharp.text.Image.GetInstance("EncabezadoAnuladas.jpg");
            else
                encabezado = iTextSharp.text.Image.GetInstance("EncabezadoEditadas.jpg");

            encabezado.SetAbsolutePosition(50, 740);
            porcentaje = 500 / encabezado.Width;
            encabezado.ScalePercent(porcentaje * 100);
            doc.Add(encabezado);

            // Agregar la DATAGRIDVIEW al PDF
            PdfPTable pdf = new PdfPTable(Tabla.Columns.Count);
            PdfPTable pdfU = new PdfPTable(dgvUsuario.Columns.Count);

            //                              Es para el tipo de fuente
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
            PdfContentByte cb = escribir.DirectContent;

            // Agregar titulos
            cb.BeginText();
            //  bf.FontType()
            cb.SetFontAndSize(bf, 9);
            cb.SetTextMatrix(70, 730);
            cb.ShowText("Fecha Inicial: " + dtpInicio.Value.ToString("dd/MM/yyyy") + "          Fecha Final: " + dtpFinal.Value.ToString("dd/MM/yyyy"));
            cb.EndText();

            float[] ancho;
            if (bandera2)
                ancho = new float[5] { 5f, 10f, 10f, 7f, 10f };
            else
                ancho = new float[10] { 2f, 4f, 9f, 3f, 3f, 3f, 3f, 3f, 7f, 5f};
            pdf.SetWidths(ancho);

            ancho = new float[1] { 25f };
            pdfU.SetWidths(ancho);

            //pdf.HorizontalAlignment = Element.ALIGN_CENTER;
            pdf.DefaultCell.Padding = 2;

            iTextSharp.text.Font text1 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
            iTextSharp.text.Font text2 = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            // Agregar encabezado
            foreach (DataGridViewColumn columna in Tabla.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 1;
                pdf.AddCell(cell);
            }

            // Informacion de filas
            foreach (DataGridViewRow row in Tabla.Rows)
            {
                int i = 0;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    i++;
                    if (bandera2)
                    {
                        if (i % 4 == 0)
                        {
                            double totalDecimal = Convert.ToDouble(cell.Value);
                            pdf.AddCell(new Phrase("Q. " + totalDecimal.ToString("N2"), text2));
                        }
                        else
                        {
                            if (i % 5 == 0)
                            {
                                DateTime fechaF = Convert.ToDateTime(cell.Value);
                                pdf.AddCell(new Phrase(fechaF.ToString("dd/MM/yyyy"), text2));
                            }
                            else
                            { 
                                pdf.AddCell(new Phrase(cell.Value.ToString(), text2));
                            }
                        }
                    }
                    else
                    {
                        if (i % 10 == 0)
                        {
                            DateTime fechaF = Convert.ToDateTime(cell.Value);
                            pdf.AddCell(new Phrase(fechaF.ToString("dd/MM/yyyy"), text2));
                        }
                        else
                        {
                            pdf.AddCell(new Phrase(cell.Value.ToString(), text2));
                        }
                    }
                }
            }
            iTextSharp.text.Font text5 = new iTextSharp.text.Font(bf, 9, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

            if (bandera2)
            {
                // Para colocar el final hasta abajo
                pdf.DefaultCell.BorderWidthLeft = 1;
                pdf.DefaultCell.BorderWidthRight = 0;
                pdf.AddCell(new Phrase(" ", text5)); // 0
                pdf.DefaultCell.BorderWidthLeft = 0;
                pdf.AddCell(new Phrase(" ", text5)); // 1
                pdf.DefaultCell.BorderWidthRight = 1;
                pdf.AddCell(new Phrase(" TOTAL", text5));  //2
                double totalEscribir = Convert.ToDouble(txtTotal.Text);
                pdf.AddCell(new Phrase("Q. " + totalEscribir.ToString("N2"), text5)); // 3
                pdf.AddCell(new Phrase(" ", text5));//4
                pdf.DefaultCell.BorderWidthRight = 0;
            }
            // Agregar objetos al PDF
            doc.Add(pdf);
            
            // Agregar al Usuario que lo genero
            pdfU.DefaultCell.BorderWidth = 0;
            pdfU.AddCell(new Phrase(" ", text5)); // 0
            pdfU.AddCell(new Phrase(" ", text5)); // 1
            pdfU.DefaultCell.BorderWidthTop = 1;
            pdfU.AddCell(new Phrase("Generado por: " + Form1.nombreUsuario, text5)); // 2
            doc.Add(pdfU);

            // FIN de la tabla
            doc.Close();

            Process.Start(ruta);
        }

    }
}
