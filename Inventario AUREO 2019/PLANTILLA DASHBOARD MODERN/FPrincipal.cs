﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;//libreria para mover ventana de formularios 

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FInicio obj = new FInicio();
            //if (obj.ShowDialog() == DialogResult.OK)
            {
                Abrirformhijo(new FUsuarios(),1);
                //parametros iniciales barra superior
                Sidebar.Visible = true;
                Sidebar.Width = 110;
                SidebarWrapper.Width = 1040;// ancho de barra superior 
                LineaSidebar.Width = 110;
            }
            //Funcion para saber si hay notifiaciones 
           
        }
       

        private void Salir_Click(object sender, EventArgs e)
        {
            FMessageBoxP obj = new FMessageBoxP("¿Seguro que desea salir?", "EXIT", 0);
            if (obj.ShowDialog() == DialogResult.OK) Application.Exit();

        
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Maximizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            Maximizar.Visible = false;
            Restaurar.Visible = true;
        }

        private void Restaurar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            Restaurar.Visible = false;
            Maximizar.Visible = true;
        }

        private void MenuSidebar_Click(object sender, EventArgs e)
        {
            if (Sidebar.Width == 1030)
            {
                Sidebar.Visible = false;
                Sidebar.Width = 120;
                //SidebarWrapper.Width = 1040;
                LineaSidebar.Width = 100;
                AnimacionSidebar.Show(Sidebar);
            }
            else
            {
                Sidebar.Visible = false;
                Sidebar.Width = 1030;
               // SidebarWrapper.Width = 1040;
                LineaSidebar.Width = 100;
                AnimacionSidebarBack.Show(Sidebar);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        //CODIGO PARA CREAR FUNCION QUE MUEVA LA VENTANA DEL FORMULARIO 
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void MenuTop_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);// instancia para mover el formulario de lugar
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }
        //Funcion para abrir formularios 
        public void Abrirformhijo(object formhijo,int llave)
        {
           
            if (this.Panelcontenedor.Controls.Count > 0)//si hay un control en el panel
            this.Panelcontenedor.Controls.RemoveAt(0);// de ser verdadero lo eliminamos
            this.Panelcontenedor.Controls.Clear();//limpiamos por completo el panel 
            Form fh = formhijo as Form;//creamos formulario 
            fh.TopLevel = false;//para definir que es un formulario secundario
            fh.Dock = DockStyle.None;// para que se acople al tamanio del panel
            this.Panelcontenedor.Controls.Add(fh);//agregamos el form al panel
            this.Panelcontenedor.Tag = fh; //establecemos la instancia como contenedor del panel
            if (llave == 1)
            {
                Point direccion = new Point(300, 50);
                fh.Location = direccion;
            }
            
            fh.Show();
        }
        private void btnproveedores_Click(object sender, EventArgs e)
        {
            // VARIABLE PARA BORRAR INFORMACION DE CLIENTES
            FSalidas.informacionSalidas = 1;
            //funcion para  cambiar color de botones
            btnproveedores.BackColor =Color.FromArgb(50,60,75);
            btnentradas.BackColor = Color.Transparent;
            btnsalidas.BackColor = Color.Transparent;
            btnclientes.BackColor = Color.Transparent;
            btnresportes.BackColor = Color.Transparent;
            btnrecibos.BackColor = Color.Transparent;
            //funcion para abrir formulario
            Abrirformhijo(new FProveedores(),0);
        }

        private void btnclientes_Click(object sender, EventArgs e)
        {
            // VARIABLE PARA BORRAR INFORMACION DEL PROVEEDOR
         //   FEntradas.informacionEntrada = 1;
            //Cambiar color de botones
            btnproveedores.BackColor = Color.Transparent;
            btnentradas.BackColor = Color.Transparent;
            btnsalidas.BackColor = Color.Transparent;
            btnclientes.BackColor = Color.FromArgb(50,60,75);
            btnresportes.BackColor = Color.Transparent;
            btnrecibos.BackColor = Color.Transparent;
            //funcion para abrir formulario
            Abrirformhijo(new FClientes(),0);
            FSalidas.nombreCliente = "";
            FSalidas.apellidoCliente = "";
            FSalidas.nitCliente = "";
            FSalidas.telefonoCliente = "";
            FSalidas.emailCliente = "";
        }

        private void btnentradas_Click(object sender, EventArgs e)
        {
            // VARIABLE PARA BORRAR INFORMACION DE CLIENTES
            FSalidas.informacionSalidas = 1;
            //funcion para cambiar colores de botones
            btnproveedores.BackColor = Color.Transparent;
            btnentradas.BackColor = Color.FromArgb(50,60,75);
            btnsalidas.BackColor = Color.Transparent;
            btnclientes.BackColor = Color.Transparent;
            btnresportes.BackColor = Color.Transparent;
            btnrecibos.BackColor = Color.Transparent;
            //funcion para abrir formulario
            //funcion para abrir formulario
            Abrirformhijo(new FEntradas(),0);
        }

        private void btnsalidas_Click(object sender, EventArgs e)
        {
            // VARIABLE PARA BORRAR INFORMACION DEL PROVEEDOR
            FEntradas.informacionEntrada = 1;
            //funcion para cambiar color de botones
            btnproveedores.BackColor = Color.Transparent;
            btnentradas.BackColor = Color.Transparent;
            btnsalidas.BackColor = Color.FromArgb(50,60,75);
            btnclientes.BackColor = Color.Transparent;
            btnresportes.BackColor = Color.Transparent;
            btnrecibos.BackColor = Color.Transparent;
            //funcion para abrir formulario
            Abrirformhijo(new FSalidas(),0);
        }

        private void btnresportes_Click(object sender, EventArgs e)
        {
            // VARIABLE PARA BORRAR INFORMACION DEL PROVEEDOR
            FEntradas.informacionEntrada = 1;
            // VARIABLE PARA BORRAR INFORMACION DE CLIENTES
            FSalidas.informacionSalidas = 1;
            //funcion para cambiar color de botones
            btnproveedores.BackColor = Color.Transparent;
            btnentradas.BackColor = Color.Transparent;
            btnsalidas.BackColor = Color.Transparent;
            btnclientes.BackColor = Color.Transparent;
            btnresportes.BackColor = Color.FromArgb(50,60,75);
            btnrecibos.BackColor = Color.Transparent;
            //funcion para abrir formulario
            Abrirformhijo(new FReportes(),0);
        }

        private void btnrecibos_Click(object sender, EventArgs e)
        {
            // VARIABLE PARA BORRAR INFORMACION DEL PROVEEDOR
            FEntradas.informacionEntrada = 1;
            // VARIABLE PARA BORRAR INFORMACION DE CLIENTES
            FSalidas.informacionSalidas = 1;
            //Funcion para cambiar color de botones
            btnproveedores.BackColor = Color.Transparent;
            btnentradas.BackColor = Color.Transparent;
            btnsalidas.BackColor = Color.Transparent;
            btnclientes.BackColor = Color.Transparent;
            btnresportes.BackColor = Color.Transparent;
            btnrecibos.BackColor = Color.FromArgb(50,60,75);
            //funcion para abrir formulario
            Abrirformhijo(new FRecibos(),0);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            MenuSidebar_Click(sender, e);
        }
        //variable para alerta
        public static int contalerta = 0;
        
        // Varaibles publicas para habilitar el FORM 1
        public static int acceso = 0;
        public static int llave = 0;
        public static string nombreUsuario = "";
        public static int idUsuario = 0;
        public static string permiso = "";
        private void timer1_Tick(object sender, EventArgs e)
        {
           
            if (acceso==1)
            {
                Point direccion;
                LbUsuario.Text = Convert.ToString(nombreUsuario);
                pictureBox2.Visible = true;
                LbUsuario.Visible = true;
                Sidebar.Visible = true;
                Sidebar.Enabled = true;
                btncerrarsesion.Visible = true;
                btnalertas.Visible = true;
                lbnotificacion.Visible = true;


                btnproveedores.BackColor = Color.Transparent;
                btnclientes.BackColor = Color.Transparent;
                btnentradas.BackColor = Color.Transparent;
                btnsalidas.BackColor = Color.Transparent;
                btnresportes.BackColor = Color.Transparent;
                btnrecibos.BackColor = Color.Transparent;

                //desplazamos barra
                    Sidebar.Visible = false;
                    Sidebar.Width = 1030;
                    // SidebarWrapper.Width = 1040;
                    LineaSidebar.Width = 100;
                    AnimacionSidebarBack.Show(Sidebar);
               

                if (permiso=="Alto")
                {
                    btnproveedores.Visible = true;
                    btnclientes.Visible = true;
                    btnentradas.Visible = true;
                    btnsalidas.Visible = true;
                    btnresportes.Visible = true;
                    btnrecibos.Visible = true;
                    direccion = new Point(810, 10);
                    btnrecibos.Location = direccion;
                }
                if (permiso == "Medio")
                {
                    btnproveedores.Visible = true;
                    btnclientes.Visible = true;
                    btnentradas.Visible = true;
                    btnsalidas.Visible = true;
                    btnresportes.Visible = true;
                    btnrecibos.Visible = true;
                    //direccion = new Point(680,10);
                    //btnrecibos.Location = direccion;

                }
                if (permiso == "Bajo")
                {
                    btnproveedores.Visible = false;
                    btnclientes.Visible = true;
                    btnentradas.Visible = false;
                    btnsalidas.Visible = true;
                    btnresportes.Visible = false;
                    btnrecibos.Visible = true;
                }
               
            }
            if (acceso == 2)
            {
                btnsalidas_Click(sender, e);
            }
            if (acceso == 3)
            {
                btnentradas_Click(sender, e);
            }
            if (acceso == 4)
            {
                btnclientes_Click(sender,e);
            }
            if (acceso == 5)
            {
                btnproveedores_Click(sender, e);
            }
           
            // El numero 10 es para bloquear toda la barra del MENU
            if (acceso == 10)
            {
                Sidebar.Enabled = false;
            }
            // El numero 11 es para desbloquear toda la barra del MENU
            if (acceso == 11)
            {
                Sidebar.Enabled = true;
            }
            if (acceso == 12)
            {
                lbnotificacion.Text =Convert.ToString( contalerta);
                if (contalerta == 0)
                {
                    btnalertas.Image = pb1.Image;
                    lbnotificacion.BackColor = Color.FromArgb(113, 214, 79);
                }
                if (contalerta > 0)
                {
                    btnalertas.Image = pb2.Image;
                    lbnotificacion.BackColor = Color.FromArgb(220, 26, 26);
                }

            }
            if(acceso==13)
            {
                
           
               
            }
           

            acceso = 0;
           
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
          
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            // VARIABLE PARA BORRAR INFORMACION DEL PROVEEDOR
            FEntradas.informacionEntrada = 1;
            // VARIABLE PARA BORRAR INFORMACION DE CLIENTES
            FSalidas.informacionSalidas = 1;
        }

        private void btncerrarsesion_Click(object sender, EventArgs e)
        {
            btncerrarsesion.Visible = false;
            btnalertas.Visible = false;
            lbnotificacion.Visible = false;
            Sidebar.Visible = false; 
            Abrirformhijo(new FUsuarios(),1);
        }

        private void btnalertas_Click(object sender, EventArgs e)
        {
         
            //funcion para  cambiar color de botones
            btnproveedores.BackColor = Color.Transparent;
            btnentradas.BackColor = Color.Transparent;
            btnsalidas.BackColor = Color.Transparent;
            btnclientes.BackColor = Color.Transparent;
            btnresportes.BackColor = Color.Transparent;
            btnrecibos.BackColor = Color.Transparent;
            //funcion para abrir formulario
            Abrirformhijo(new FAlertas(),0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {

                lbnotificacion.Visible = false;
                FAlertas alertas = new FAlertas();
                if (alertas.ShowDialog() == DialogResult.OK)
                {
                    lbnotificacion.Text = Convert.ToString(contalerta);
                    if (contalerta == 0)
                    {
                        btnalertas.Image = pb1.Image;
                        lbnotificacion.BackColor = Color.FromArgb(113, 214, 79);
                      
                    }
                    if (contalerta > 0)
                    {
                       
                        btnalertas.Image = pb2.Image;
                        lbnotificacion.BackColor = Color.FromArgb(220, 26, 26);
                    }
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error P01: " + ex, "Error de Conexión", 0);
                obj.ShowDialog();
            }
        }
    }
}
