﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FAlertas : Form
    {
        public FAlertas()
        {
            InitializeComponent();
            //ENVIAR SI HAY NOTIFICACIONES
           

        }
        Conexion conect = new Conexion();
       

        private void FAlertas_Load(object sender, EventArgs e)
        {
           

            DateTime fechahoy = dtpFechaHoy.Value;
           // fechahoy = fechahoy.AddMonths(-1);
            fechahoy = fechahoy.AddDays(5);
            dtpInicio.Value = Convert.ToDateTime("1/" + dtpFechaHoy.Value.Month + "/" + dtpFechaHoy.Value.Year); ;
            conect.actualizarAlertasCompras(dgvEntradas, fechahoy.ToString("dd/MM/yyyy"), txtBusqueda);
            //conect.actualizarReportesEntradas(dgvEntradas, dtpInicio.Value.ToString("dd/MM/yyyy"), dtpFinal.Value.ToString("dd/MM/yyyy"), txtBusqueda);
            
            lbnoalertas.Text = Convert.ToString(dgvEntradas.Rows.Count);
            Form1.contalerta = dgvEntradas.Rows.Count;
            DialogResult = DialogResult.OK;
           
        }

        private void dgvEntradas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            FAgregarPago.noRep = dgvEntradas.SelectedCells[0].Value.ToString();
            FAgregarPago.proveedor = dgvEntradas.SelectedCells[1].Value.ToString();
            FAgregarPago.contacto = dgvEntradas.SelectedCells[2].Value.ToString();
            FAgregarPago.telefono = dgvEntradas.SelectedCells[3].Value.ToString();
            FAgregarPago.total = dgvEntradas.SelectedCells[4].Value.ToString();
            FAgregarPago.abono = dgvEntradas.SelectedCells[5].Value.ToString();
            FAgregarPago.restante = dgvEntradas.SelectedCells[6].Value.ToString();
            FAgregarPago.credito = dgvEntradas.SelectedCells[7].Value.ToString();
            FAgregarPago.fecha = dgvEntradas.SelectedCells[8].Value.ToString();
            FAgregarPago.fechaalerta = dgvEntradas.SelectedCells[9].Value.ToString(); 
            FAgregarPago obj = new FAgregarPago();
            obj.ShowDialog();
            DateTime fechahoy = dtpFechaHoy.Value;
            //fechahoy = fechahoy.AddMonths(-1);
            fechahoy = fechahoy.AddDays(5);
            conect.actualizarAlertasCompras(dgvEntradas, fechahoy.ToString("dd/MM/yyyy"), txtBusqueda);
            // funcionBusqueda();
            lbnoalertas.Text = Convert.ToString(dgvEntradas.Rows.Count);
            Form1.contalerta = dgvEntradas.Rows.Count;
            Form1.acceso = 12;

        }
    }
}
