﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FTablaPagos : Form
    {
        public FTablaPagos()
        {
            InitializeComponent();
        }
        static public string tablaNoRep;
        static public string tablaProveedor;

        Conexion conect = new Conexion();
        private void FTablaPagos_Load(object sender, EventArgs e)
        {
            this.Text = "CUADRO DE AMORTIZACIÓN - " + tablaNoRep + " " + tablaProveedor;
            conect.actualizarAbonosProveedores(dgvPagos, tablaNoRep);
            txtTotal.Text = conect.generarTotal(dgvPagos, 2).ToString("N2");
        }
    }
}
