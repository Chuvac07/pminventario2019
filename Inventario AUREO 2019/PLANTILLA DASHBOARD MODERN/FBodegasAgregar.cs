﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FBodegasAgregar : Form
    {
        public FBodegasAgregar()
        {
            InitializeComponent();
        }
        Conexion conect = new Conexion();
        private void FBodegasAgregar_Load(object sender, EventArgs e)
        {
            conect.actualizarBodegas(dgvBodegas);
            if (dgvBodegas.Rows.Count > 0)
            {
                btnEliminar.Visible = true;
                btnModificar.Visible = true; 
            }
            dgvBodegas.Columns[0].ReadOnly = true;
            this.ActiveControl = txtNombre;
        }

        private bool validarTextBox()
        {
            bool estado = true; 
            if (txtNombre.Text == "")
            {
                errorProvider1.SetError(txtNombre,"Ingrese el nombre de la bodega.");
                estado = false;
            }
            return estado; 
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void txtNit_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (validarTextBox())
            {
                conect.ingresarBodegas(conect.generarN(dgvBodegas).ToString(), txtNombre.Text);
                conect.actualizarBodegas(dgvBodegas);
                btnEliminar.Visible = true;
                btnModificar.Visible = true;
                txtNombre.Clear();
            }
            this.ActiveControl = txtNombre;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            conect.eliminarBodegas(dgvBodegas.SelectedCells[0].Value.ToString());
            conect.actualizarBodegas(dgvBodegas);
            if (dgvBodegas.Rows.Count <= 0)
            {
                btnEliminar.Visible = false;
                btnModificar.Visible = false; 
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            conect.modificarBodegas(dgvBodegas.SelectedCells[0].Value.ToString(), dgvBodegas.SelectedCells[1].Value.ToString());
        }

        private void txtNombre_KeyUp(object sender, KeyEventArgs e)
        {
            errorProvider1.SetError(txtNombre,"");
        }
    }
}
