﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FAgregarPago : Form
    {
        static public string noRep;
        static public string proveedor;
        static public string telefono;
        static public string fecha;
        static public string contacto;
        static public string total;
        static public string abono;
        static public string restante;
        static public string credito;
        static public string empresa;
        static public string ganancia;
        static public string fechaalerta= "dd/MM/yyyy";
        static public bool accion = true;
        //variables para tratado de imagenes
        public static int bandera = 0;
        public static string escritorio;
        public static string carpeta;
        public static string subcarpeta;
        public static string ff;
        public OpenFileDialog openFileDialog = new OpenFileDialog();
        public FAgregarPago()
        {
            InitializeComponent();
        }

        Conexion conect = new Conexion();
        private void FAgregarPago_Load(object sender, EventArgs e)
        {
            dtPostergar.Value =Convert.ToDateTime( fechaalerta);
            this.Text = "No." + noRep + " " + proveedor;
            lbTelefono.Text = telefono;
            lbFecha.Text = fecha;
            lbContacto.Text = contacto;

            if (accion)
            {
                lbProveedor.Text = proveedor;

                conect.actualizarProductoReportesEntradas(dgvProducto, noRep);
                txtTotal.Text = conect.generarTotal(dgvProducto, 7).ToString("N2");
                txtAbono.Text = abono;
                txtRestante.Text = restante;
                if (Convert.ToDouble(txtTotal.Text) != Convert.ToDouble(total))
                    txtTotal.BackColor = Color.Red;
                if (Convert.ToDouble(restante) > 0)
                {
                    lbAbonar.Visible = txtPago.Visible = btnGuardar.Visible = lbpostergar.Visible = lbreferencia.Visible = dtPostergar.Visible = txtReferencia.Visible = btnpostergar.Visible = true;
                }
            }
            else
            {
                lbProveedor.Text = "Empresa:";
                lbProveedor.Text = empresa;
                btnEliminar.Visible = true; 
                conect.actualizarProductoReportesSalidas(dgvProducto, noRep);
                double totalProducto = conect.generarTotal(dgvProducto, 7); 
                txtTotal.Text = totalProducto.ToString("N2");
                txtAbono.Text = abono;
                restante = (totalProducto - Convert.ToDouble(abono)).ToString("N2");
                txtRestante.Text = restante; 
                if (Convert.ToDouble(txtTotal.Text) != Convert.ToDouble(total))
                    txtTotal.BackColor = Color.Red;
                if (Convert.ToDouble(restante) > 0)
                {
                    lbAbonar.Visible = txtPago.Visible = btnGuardar.Visible = true;
                }
            }
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private bool validarTextbox()
        {
            bool estado = true;

            if (txtPago.Text == "")
            {
                errorP1.SetError(txtPago, "Ingrese un valor valido la acreditación.");
                estado = false;
            }

            if (Convert.ToDouble(txtPago.Text) > Convert.ToDouble(restante))
            {
                errorP1.SetError(txtPago, "La cantidad debe ser menor o igual al total final.");
                estado = false;
            }
            if (Convert.ToDouble(txtPago.Text) <= 0)
            {
                errorP1.SetError(txtPago, "La cantidad debe ser mayor a cero.");
                estado = false;
            }

            if (txtReferencia.Text == "")
            {
                errorP1.SetError(txtReferencia, "Ingrese un valor de referencia.");
                estado = false;
            }
            return estado;
        }

        private bool validarTextbox2()
        {
            bool estado = true;

            if (txtPago.Text == "")
            {
                errorP1.SetError(txtPago, "Ingrese un valor valido la acreditación.");
                estado = false;
            }

            if (Convert.ToDouble(txtPago.Text) > Convert.ToDouble(txtRestante.Text))
            {
                errorP1.SetError(txtPago, "La cantidad debe ser menor o igual al total final.");
                estado = false;
            }
            if (Convert.ToDouble(txtPago.Text) <= 0)
            {
                errorP1.SetError(txtPago, "La cantidad debe ser mayor a cero.");
                estado = false;
            }
            return estado;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
               
                if (validarTextbox() && accion)
                {
                    double pagoDinero = Convert.ToDouble(txtPago.Text);
                    double agregarDinero = Convert.ToDouble(txtAbono.Text) + pagoDinero;
                    txtAbono.Text = agregarDinero.ToString("N2");
                    double quitarDinero = Convert.ToDouble(txtRestante.Text) - Convert.ToDouble(txtPago.Text);
                    txtRestante.Text = quitarDinero.ToString("N2");
                    conect.modificarAbonarProveedores(noRep, agregarDinero.ToString(), quitarDinero.ToString());
                    // Agregar el pago a la Tabla de abonar
                    conect.ingresarAbonosProveedores(noRep, proveedor, pagoDinero.ToString(), txtReferencia.Text, dtpFechaHoy.Value.ToString("dd/MM/yyyy"));
                    DialogResult = DialogResult.OK;
                    //modificar Fecha Alerta
                  
                }
                if (validarTextbox2() && !accion)
                {
                    double pagoDinero = Convert.ToDouble(txtPago.Text);
                    double agregarDinero = Convert.ToDouble(txtAbono.Text) + pagoDinero;
                    txtAbono.Text = agregarDinero.ToString("N2");
                    double quitarDinero = Convert.ToDouble(txtRestante.Text) - Convert.ToDouble(txtPago.Text);
                    txtRestante.Text = quitarDinero.ToString("N2");
                    conect.modificarAbonarClientes(noRep, agregarDinero.ToString());
                   
                }
                // tratado de imagenes 
                if (bandera == 1)
                {

                    DateTime fecha1 = DateTime.Now;
                    fecha1.Minute.ToString();
                    // textBox1.Text = fecha1.Second.ToString();
                    string extension;
                    // OpenFileDialog openFileDialog = new OpenFileDialog();
                    //openFileDialog.Multiselect = true;
                    foreach (FileInfo i in vector)
                    {
                        //ListaArchivos.Items.Add(fil);
                        extension = Path.GetExtension(Convert.ToString(i));
                        string rutadestino = subcarpeta + "\\" + cont + " Factura Q."+txtPago.Text +" " +ff + extension;
                        if (File.Exists(rutadestino))
                        {
                            rutadestino = subcarpeta + "\\" + cont + " Factura Q."+txtPago.Text +" "+ ff + fecha1.Minute.ToString() + extension;

                        }
                        if (i != null)
                        {
                            File.Move(Convert.ToString(i), rutadestino);
                            cont++;
                        }



                    }
                    FMessageBox obj = new FMessageBox("Imagenes almacenadas correctamente", "Almacenamiento", 0);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                    bandera = 0;
                    this.btnCargarFoto.BackColor = Color.FromArgb(49, 66, 62);
                   

                }
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Se debe ingresar los campos solicitados.", "ERROR AP1:", 1);
                obj.ShowDialog();
            }
        }

        int contador = 0;
        private void button1_Click(object sender, EventArgs e)//BOTON POSTERGAR
        {
            contador++;
            if (contador % 2 == 1)
            {
                btnpostergar.BackColor = Color.LightGreen;
                conect.modificarfechaProveedores(noRep, dtPostergar.Value);
                
            }
            else
            {
                
                btnpostergar.BackColor = btnGuardar.BackColor;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            FTablaPagos obj = new FTablaPagos();
            FTablaPagos.tablaNoRep = noRep;
            FTablaPagos.tablaProveedor = proveedor;
            obj.ShowDialog();
        }

        private void txtPago_KeyUp(object sender, KeyEventArgs e)
        {
            double c;
            if (!double.TryParse(txtPago.Text, out c))
                errorP1.SetError(txtPago, "Ingrese un cantidad en números.");
            else
            {
                double vpago = Convert.ToDouble(txtPago.Text);
                if (vpago <= Convert.ToDouble(restante))
                    errorP1.SetError(txtPago, "");
            }
        }

        private void txtReferencia_KeyUp(object sender, KeyEventArgs e)
        {
            errorP1.SetError(txtReferencia, "");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
             foreach(DataGridViewRow fila in dgvProducto.Rows)
             {
                 string jcodigo = fila.Cells[1].Value.ToString();
                 string jdescripcion = fila.Cells[3].Value.ToString();
                 string jcantidad = fila.Cells[2].Value.ToString();
                 string jid = conect.busquedaProductoModificarId(jcodigo, jdescripcion);
                 string jexistencia = conect.busquedaProductoModificarExistencia(jcodigo, jdescripcion);
                 int jfinal = Convert.ToInt16(jcantidad) + Convert.ToInt16(jexistencia);
                 conect.modificarProductoCantidad(jid, jfinal);
             }
             DateTime fechaDia = Convert.ToDateTime(fecha);
             conect.ingresarSalidasAnuladas(conect.busquedaIdAnuladas(), contacto, empresa, total, fechaDia.ToString("dd/MM/yyyy"));
             conect.eliminarPMSalidas(noRep);
             conect.eliminarSalidas(noRep);
             DialogResult = DialogResult.Cancel;
        }
        FileInfo[] vector = new FileInfo[100];
        int c = 0;
        int cont = 1;
        private void btnCargarFoto_Click(object sender, EventArgs e)
        {

            try
            {
                int m = dtpFechaHoy.Value.Month;
                int d = dtpFechaHoy.Value.Day;
                int a = dtpFechaHoy.Value.Year;
                ff = Convert.ToString(d) + "-" + Convert.ToString(m) + "-" + Convert.ToString(a);
                string mes = "";
                switch (m)
                {
                    case 1: mes = "Enero"; break;
                    case 2: mes = "Febrero"; break;
                    case 3: mes = "Marzo"; break;
                    case 4: mes = "Abril"; break;
                    case 5: mes = "Mayo"; break;
                    case 6: mes = "Junio"; break;
                    case 7: mes = "Julio"; break;
                    case 8: mes = "Agosto"; break;
                    case 9: mes = "Septiembre"; break;
                    case 10: mes = "Octubre"; break;
                    case 11: mes = "Noviembre"; break;
                    case 12: mes = "Diciembre"; break;
                }
                escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                string cliente = lbProveedor.Text;
                //string carpeta = @"C:\Proyectos Multiples\" + mes;
                DateTime fechaaux = Convert.ToDateTime( lbFecha.Text);
                
                carpeta = escritorio + "\\" + "PM Facturas" + "\\" + mes;

                subcarpeta = carpeta + "\\" + cliente + " " + fechaaux.Day+ "-"+fechaaux.Month+"-"+fechaaux.Year;

                if (!(Directory.Exists(carpeta)))
                {
                    Directory.CreateDirectory(carpeta);
                }
                if (!(Directory.Exists(subcarpeta)))
                {
                    Directory.CreateDirectory(subcarpeta);
                }


                DateTime fecha1 = DateTime.Now;
                fecha1.Minute.ToString();
                // textBox1.Text = fecha1.Second.ToString();

                string extension;
                //  OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Multiselect = true;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {

                    bandera = 1;
                    foreach (string f in openFileDialog.FileNames)
                    {
                        FileInfo fil = new FileInfo(f);
                        vector[c] = fil;
                        c++;
                    }
                    this.btnCargarFoto.BackColor = Color.FromArgb(112, 223, 96);
                   
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error SE2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }

        }
    }
}
