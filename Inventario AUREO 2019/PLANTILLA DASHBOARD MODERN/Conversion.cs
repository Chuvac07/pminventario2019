﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLANTILLA_DASHBOARD_MODERN
{
    class Conversion
    {
        public string numerosLetras(string num)
        {
            string valorentero, valordecimal = "", valorfinal = "";
            Int64 entero;
            int decimales;
            double nro;

            try

            {
                nro = Convert.ToDouble(num);
            }
            catch
            {
                return "";
            }

            entero = Convert.ToInt64(Math.Truncate(nro));
            decimales = Convert.ToInt32(Math.Round((nro - entero) * 100, 2));

            valorentero = toText(entero);

            if (decimales == 1)
                valordecimal = "UN CENTAVO";
            else if (decimales > 1)
                valordecimal = toText(decimales);

            if (decimales > 0)
                valorfinal = valorentero + " CON " + valordecimal + " CENTAVOS";
            else
                valorfinal = valorentero;

            return valorfinal;
        }

        private string toText(decimal valor)
        {

            string letras = "";
            if (valor == 0) letras = "CERO";
            else if (valor == 1) letras = "UNO";
            else if (valor == 2) letras = "DOS";
            else if (valor == 3) letras = "TRES";
            else if (valor == 4) letras = "CUATRO";
            else if (valor == 5) letras = "CINCO";
            else if (valor == 6) letras = "SEIS";
            else if (valor == 7) letras = "SIETE";
            else if (valor == 8) letras = "OCHO";
            else if (valor == 9) letras = "NUEVE";
            else if (valor == 10) letras = "DIEZ";
            else if (valor == 11) letras = "ONCE";
            else if (valor == 12) letras = "DOCE";
            else if (valor == 13) letras = "TRECE";
            else if (valor == 14) letras = "CATORCE";
            else if (valor == 15) letras = "QUINCE";
            else if (valor < 20) letras = "DIECI" + toText(valor - 10);
            else if (valor == 20) letras = "VEINTE";
            else if (valor < 30) letras = "VEINTI" + toText(valor - 20);
            else if (valor == 30) letras = "TREINTA";
            else if (valor < 40) letras = "TREINTA Y " + toText(valor - 30);
            else if (valor == 40) letras = "CUARENTA";
            else if (valor < 50) letras = "CUARENTA Y " + toText(valor - 40);
            else if (valor == 50) letras = "CINCUENTA";
            else if (valor < 60) letras = "CINCUENTA Y " + toText(valor - 50);
            else if (valor == 60) letras = "SESENTA";
            else if (valor < 70) letras = "SESENTA Y " + toText(valor - 60);
            else if (valor == 70) letras = "SETENTA";
            else if (valor < 80) letras = "SETENTA Y " + toText(valor - 70);
            else if (valor == 80) letras = "OCHENTA";
            else if (valor < 90) letras = "OCHENTA Y " + toText(valor - 80);
            else if (valor == 90) letras = "NOVENTA";
            else if (valor < 100) letras = "NOVENTA Y " + toText(valor - 90);



            else if (valor == 100) letras = "CIEN";
            else if (valor < 200) letras = "CIENTO " + toText(valor - 100);
            else if (valor == 200) letras = "DOSCIENTOS";
            else if (valor < 300) letras = "DOSCIENTOS " + toText(valor - 200);
            else if (valor == 300) letras = "TRESCIENTOS";
            else if (valor < 400) letras = "TRESCIENTOS " + toText(valor - 300);
            else if (valor == 400) letras = "CUATROCIENTOS";
            else if (valor < 500) letras = "CUATROCIENTOS " + toText(valor - 400);
            else if (valor == 500) letras = "QUINIENTOS";
            else if (valor < 600) letras = "QUINIENTOS " + toText(valor - 500);
            else if (valor == 600) letras = "SEISCIENTOS";
            else if (valor < 700) letras = "SEISCIENTOS " + toText(valor - 600);
            else if (valor == 700) letras = "SETECIENTOS";
            else if (valor < 800) letras = "SETECIENTOS " + toText(valor - 700);
            else if (valor == 800) letras = "OCHOCIENTOS";
            else if (valor < 900) letras = "OCHOCIENTOS " + toText(valor - 800);
            else if (valor == 900) letras = "NOVECIENTOS";
            else if (valor < 1000) letras = "NOVECIENTOS " + toText(valor - 900);

            else if (valor == 1000) letras = "MIL";
            else if (valor < 2000) letras = "MIL " + toText(valor % 1000);

            else if (valor >= 2000 && valor < 1000000)
            {
                letras = toText(Math.Truncate(valor / 1000)) + " MIL ";
                if ((valor % 1000) > 0)
                    letras += toText(Math.Truncate(valor % 1000));
            }

            else if (valor == 1000000) letras = "UN MILLÓN";
            else if (valor < 2000000) letras = "UN MILLÓN, " + toText(valor % 1000000);
            else if (valor >= 2000000 && valor < 1000000000000)
            {
                letras = toText(Math.Truncate(valor / 1000000)) + " MILLONES, ";
                if ((valor % 1000000) > 0)
                    letras += toText(Math.Truncate(valor % 1000000));
            }

            else if (valor == 1000000000000) letras = "UN BILLÓN";
            else if (valor < 2000000000000) letras = "UN BILLÓN, " + toText(valor % 1000000000000);

            else if (valor >= 2000000000000)
            {
                letras = toText(Math.Truncate(valor / 1000000000000)) + " BILLONES, ";
                if ((valor % 1000000000000) > 0)
                    letras += toText(valor % 1000000000000);
            }
            return letras;
        }
    }
}
