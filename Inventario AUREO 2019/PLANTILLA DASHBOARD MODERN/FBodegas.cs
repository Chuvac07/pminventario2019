﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FBodegas : Form
    {
        public FBodegas()
        {
            InitializeComponent();
        }

        Conexion conect = new Conexion();
        private void FBodegas_Load(object sender, EventArgs e)
        {
            conect.busquedaBodegas(cbBodegas);
            DateTime fecha = DateTime.Now;
            lbFecha.Text = "Fecha: " + fecha.ToString("dd/MM/yyyy");
        }

        private void btnEditarB_Click(object sender, EventArgs e)
        {
            FBodegasAgregar obj = new FBodegasAgregar();
            if (obj.ShowDialog() == DialogResult.OK) ;
            conect.busquedaBodegas(cbBodegas);
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        static public string Bcantidad = "";
        static public string BidInventario = "";
        static public string Bexistencia = "";
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FInventario.formulariestado = 3; 
            FInventario obj = new FInventario(2);
            if (obj.ShowDialog() == DialogResult.OK)
            {
                bool repetido = false;

                foreach (DataGridViewRow fila in dgvBodegas.Rows)
                {
                    if (fila.Cells[2].Value.ToString() == BidInventario)
                    {
                        conect.modificarProductoBodegas(BidInventario, Convert.ToString(Convert.ToInt16(Bcantidad) + Convert.ToInt16(fila.Cells[1].Value)));
                        repetido = true;
                    } 
                }
                if (!repetido)
                    conect.ingresarProductoBodegas(Convert.ToString((cbBodegas.SelectedIndex + 1)), Bcantidad, BidInventario);

                conect.modificarProductoCantidad(BidInventario, Convert.ToInt16(Bexistencia) - Convert.ToInt16(Bcantidad));

                conect.actualizarProductoBodegas(dgvBodegas, Convert.ToString(cbBodegas.SelectedIndex + 1));
                dgvProducto.Rows.Clear();
                foreach (DataGridViewRow fila in dgvBodegas.Rows)
                {
                    conect.busquedaBodegasId(dgvProducto, fila.Cells[2].Value.ToString(), fila.Cells[1].Value.ToString());
                }
            }
            Bcantidad = "";
            BidInventario = "";
            Bexistencia = "";

            if (dgvProducto.Rows.Count > 0)
            {
                btnEliminar.Visible = true;
                btnEnviarBodega.Visible = true;
            }
        }

        private void cbBodegas_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnEliminar.Visible = false;
            btnEnviarBodega.Visible = false; 

            dgvProducto.Rows.Clear();
            btnAgregar.Visible = true;
            conect.actualizarProductoBodegas(dgvBodegas, Convert.ToString(cbBodegas.SelectedIndex + 1));
            foreach (DataGridViewRow fila in dgvBodegas.Rows)
            {
                conect.busquedaBodegasId(dgvProducto, fila.Cells[2].Value.ToString(), fila.Cells[1].Value.ToString());
            }
            if (dgvProducto.Rows.Count > 0)
            {
                btnEliminar.Visible = true;
                btnEnviarBodega.Visible = true;
            }
        }

        private void dgvBodegas_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
           
        }
        private void dgvBodegas_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
        }

        private void btnEnviarBodega_Click(object sender, EventArgs e)
        {
            int ebcantidad = Convert.ToInt16(dgvProducto.SelectedCells[3].Value); // cantidad
            int ebexistencia = Convert.ToInt16(dgvProducto.SelectedCells[4].Value); // existencia
            conect.modificarProductoCantidad(dgvProducto.SelectedCells[6].Value.ToString(), ebcantidad + ebexistencia); // Id del producto 6 - 
            conect.eliminarBodegasProducto(Convert.ToString(cbBodegas.SelectedIndex + 1), dgvProducto.SelectedCells[6].Value.ToString());
            conect.actualizarProductoBodegas(dgvBodegas, Convert.ToString(cbBodegas.SelectedIndex + 1));
            dgvProducto.Rows.Clear();
            foreach (DataGridViewRow fila in dgvBodegas.Rows)
            {
                conect.busquedaBodegasId(dgvProducto, fila.Cells[2].Value.ToString(), fila.Cells[1].Value.ToString());
            }
            if (dgvProducto.Rows.Count <= 0)
            {
                btnEliminar.Visible = false;
                btnEnviarBodega.Visible = false;
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            conect.eliminarBodegasProducto(Convert.ToString(cbBodegas.SelectedIndex + 1), dgvProducto.SelectedCells[6].Value.ToString());
            conect.actualizarProductoBodegas(dgvBodegas, Convert.ToString(cbBodegas.SelectedIndex + 1));
            dgvProducto.Rows.Clear();
            foreach (DataGridViewRow fila in dgvBodegas.Rows)
            {
                conect.busquedaBodegasId(dgvProducto, fila.Cells[2].Value.ToString(), fila.Cells[1].Value.ToString());
            }
            if (dgvProducto.Rows.Count <= 0)
            {
                btnEliminar.Visible = false;
                btnEnviarBodega.Visible = false;
            }

        }
    }
}
