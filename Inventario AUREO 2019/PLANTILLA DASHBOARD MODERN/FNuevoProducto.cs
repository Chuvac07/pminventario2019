﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FNuevoProducto : Form
    {
        public FNuevoProducto(int IdProducto, int f)
        {
            InitializeComponent();
            lbNo.Text = Convert.ToString(IdProducto);
            formulario = f; 
        }

        Conexion conect = new Conexion();
        int formulario = 0; 
        private void FNuevoProducto_Load(object sender, EventArgs e)
        {
            this.ActiveControl = txtCodigo; 
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private bool validarTextbox()
        {
            bool estado = true;

            if (txtCodigo.Text == "")
            {
                errorP1.SetError(txtCodigo, "Ingrese un código valido.");
                estado = false;
            }

            if (txtDescripcion.Text == "")
            {
                errorP1.SetError(txtDescripcion, "Ingrese la descripción del producto.");
                estado = false;
            }

            if (txtCategoria.Text == "")
            {
                errorP1.SetError(txtCategoria, "Ingrese la categoría del producto.");
                estado = false;
            }

            if (txtMarca.Text == "")
            {
                errorP1.SetError(txtMarca, "Ingrese la marca del producto.");
                estado = false;
            }
            
            if (txtpreciocosto.Text == "")
            {
                errorP1.SetError(txtpreciocosto, "Ingrese el costo del producto.");
                estado = false;
            }

            return estado;
        }
        private void txtpreciocosto_KeyUp(object sender, KeyEventArgs e)
        {
            double c;
            if (!double.TryParse(txtpreciocosto.Text, out c))
                errorP1.SetError(txtpreciocosto, "Ingrese un costo en números.");
            else
            {
                double vcosto = Convert.ToDouble(txtpreciocosto.Text);
                txt10.Text = Convert.ToString(vcosto * 1.2);
                txt20.Text = Convert.ToString(vcosto * 1.3);
                txt30.Text = Convert.ToString(vcosto * 1.4);
                errorP1.SetError(txtpreciocosto, "");
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (validarTextbox())
            {
                if (txtBarras.Text != "")
                    conect.ingresarProductoE(lbNo.Text, txtCodigo.Text, txtBarras.Text, txtDescripcion.Text, txtCategoria.Text, txtMarca.Text, 0, Convert.ToDouble(txtpreciocosto.Text), Convert.ToDouble(txt10.Text), Convert.ToDouble(txt20.Text), Convert.ToDouble(txt30.Text));
                else
                    conect.ingresarProductoE(lbNo.Text, txtCodigo.Text, "-", txtDescripcion.Text, txtCategoria.Text, txtMarca.Text, 0, Convert.ToDouble(txtpreciocosto.Text), Convert.ToDouble(txt10.Text), Convert.ToDouble(txt20.Text), Convert.ToDouble(txt30.Text));
                if (formulario == 1)
                {
                    FEntradas.Eid = lbNo.Text;
                    FEntradas.Eexistencia = "0";
                    FEntradas.Ecodigo = txtCodigo.Text;
                    FEntradas.Edescripcion = txtDescripcion.Text;
                    FEntradas.Ecosto = txtpreciocosto.Text;
                }
                DialogResult = DialogResult.OK;
            }
        }

        private void txtCodigo_KeyUp(object sender, KeyEventArgs e)
        {
            errorP1.SetError(txtCodigo, "");
        }

        private void txtDescripcion_KeyUp(object sender, KeyEventArgs e)
        {
            errorP1.SetError(txtDescripcion, "");
        }

        private void txtCategoria_KeyUp(object sender, KeyEventArgs e)
        {
            errorP1.SetError(txtCategoria, "");
        }

        private void txtMarca_KeyUp(object sender, KeyEventArgs e)
        {
            errorP1.SetError(txtMarca, "");
        }
    }
}
