﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FProveedores : Form
    {
        public FProveedores()
        {
            InitializeComponent();
        }
        Conexion conect = new Conexion();
        
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {

                if (validarTextbox())
                {

                    btnModificar.Visible = false;
                    btnEliminar.Visible = false;
                    btnEntradas.Visible = true;

                    conect.actualizarProveedores(dgvProveedores);
                    string id = Convert.ToString(conect.generarN(dgvProveedores));
                    string nit = txtNit.Text;
                    string empresa = txtEmpresa.Text;
                    string telefono = txtTelefono.Text;
                    string contacto = txtCyT.Text;
                    string direccion = txtDireccion.Text;
                    string email = txtEmail.Text;

                    conect.ingresarProveedores(id, nit, empresa, telefono, contacto, direccion, email);
                    conect.actualizarProveedores(dgvProveedores);
                    txtNit.Enabled = false;
                    txtEmpresa.Enabled = false;
                    txtTelefono.Enabled = false;
                    txtDireccion.Enabled = false;
                    txtEmail.Enabled = false;
                    btnAgregar.Visible = false;
                    txtCyT.Enabled = false; 
                }
                else
                {
                     FMessageBox obj = new FMessageBox("Se deben ingresar todos los campos que se solicitan.", "ERROR PR0:", 1);
                     if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error PR1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private bool validarTextbox()
        {
            bool estado = true;
            if (txtNit.Text == "")
            {
                errorP1.SetError(txtNit, "Ingrese el nit de la empresa.");
                estado = false;
            }

            if (txtEmpresa.Text == "")
            {
                errorP1.SetError(txtEmpresa, "Ingrese el nombre de la empresa.");
                estado = false;
            }

            if (txtTelefono.Text == "")
            {
                errorP1.SetError(txtTelefono, "Ingrese el télefono de la empresa.");
                estado = false;
            }

            if (txtCyT.Text == "")
            {
                errorP1.SetError(txtCyT, "Ingrese el nombre de un representante con su número de teléfono.");
                estado = false;
            }

            if (txtDireccion.Text == "")
            {
                errorP1.SetError(txtDireccion, "Ingrese la dirección de la empresa.");
                estado = false;
            }
            return estado;
        }

        private void FProveedores_Load(object sender, EventArgs e)
        {
            conect.actualizarProveedores(dgvProveedores);
            lbCodigo.Text = Convert.ToString(dgvProveedores.Rows.Count + 1);
            this.ActiveControl = txtNit;
        }

        private void dgvProveedores_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnAgregar.Visible = false; 
            btnModificar.Visible = true;
            btnEliminar.Visible = true;
            btnEntradas.Visible = true;
            lbCodigo.Visible = true;
            label2.Visible = true; 

            lbCodigo.Text = dgvProveedores.CurrentRow.Cells[0].Value.ToString();
            txtEmpresa.Text = dgvProveedores.CurrentRow.Cells[1].Value.ToString();
            txtNit.Text = dgvProveedores.CurrentRow.Cells[2].Value.ToString();
            txtTelefono.Text = dgvProveedores.CurrentRow.Cells[3].Value.ToString();
            txtCyT.Text = dgvProveedores.CurrentRow.Cells[4].Value.ToString();
            txtDireccion.Text = dgvProveedores.CurrentRow.Cells[5].Value.ToString();
            txtEmail.Text = dgvProveedores.CurrentRow.Cells[6].Value.ToString();

            txtNit.Enabled = true;
            txtEmpresa.Enabled = true;
            txtTelefono.Enabled = true;
            txtCyT.Enabled = true;
            txtDireccion.Enabled = true;
            txtEmail.Enabled = true;
        }

        private void btnEntradas_Click(object sender, EventArgs e)
        {
            FEntradas.idProveedor = lbCodigo.Text; 
            FEntradas.empresaProveedor = txtEmpresa.Text;
            FEntradas.nitProveedor = txtNit.Text;
            FEntradas.telefonoProveedor = txtTelefono.Text;
            FEntradas.contactosProveedor = txtCyT.Text;
            FEntradas.emailProveedor = txtEmail.Text;
            FEntradas.informacionEntrada = 0; 
            Form1.acceso = 3; 
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            string id = lbCodigo.Text;
            string nit = txtNit.Text;
            string empresa = txtEmpresa.Text;
            string telefono = txtTelefono.Text;
            string contacto = txtCyT.Text; 
            string direccion = txtDireccion.Text;
            string email = txtEmail.Text;

            if (nit != "" && empresa != "" && telefono != "" && direccion != "")
            {
                conect.modificarProveedores(id, nit, empresa, telefono, contacto, direccion, email);
                conect.actualizarProveedores(dgvProveedores);
                txtNit.Enabled = false;
                txtEmpresa.Enabled = false;
                txtTelefono.Enabled = false;
                txtCyT.Enabled = false;
                txtDireccion.Enabled = false;
                txtEmail.Enabled = false;
            }
            else
            {
                FMessageBox obj = new FMessageBox("Se deben ingresar todos los campos que se solicitan.", "ERROR PR2:", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                FMessageBoxP obj = new FMessageBoxP("¿Está seguro en eliminar el producto seleccionado?", "¡Alerta de eliminación!", 1);
                if (obj.ShowDialog() == DialogResult.OK)
                {
                    string id = lbCodigo.Text;
                    conect.eliminarProveedores(id);
                    conect.actualizarProveedores(dgvProveedores);
                    txtNit.Clear();
                    txtEmpresa.Clear();
                    txtTelefono.Clear();
                    txtCyT.Clear();
                    txtDireccion.Clear();
                    txtEmail.Clear();
                    lbCodigo.Text = "0";

                    btnAgregar.Visible = true;
                    btnEliminar.Visible = false;
                    btnModificar.Visible = false;
                    btnEntradas.Visible = false; 
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Verifique el dato a eliminar", "¡Error al Eliminar!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void txtNit_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaProveedores(dgvProveedores, txtNit, "nit_proveedorespm");
        }

        private void txtEmpresa_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaProveedores(dgvProveedores, txtEmpresa, "nombre_proveedorespm");
        }

        private void txtNit_KeyPress(object sender, KeyPressEventArgs e)
        {
            errorP1.SetError(txtNit, "");
        }

        private void txtEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            errorP1.SetError(txtEmpresa, "");
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            errorP1.SetError(txtTelefono, "");
        }

        private void txtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            errorP1.SetError(txtDireccion, "");
        }

        private void txtCyT_KeyPress(object sender, KeyPressEventArgs e)
        {
            errorP1.SetError(txtCyT, "");
        }
    }
}
