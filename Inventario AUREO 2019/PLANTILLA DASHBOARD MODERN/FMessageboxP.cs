﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FMessageBoxP : Form
    {
        public FMessageBoxP(string informacion, string inicio, int c)
        {
            InitializeComponent();
            lbInicio.Text = inicio;
            txtInformacion.Text = informacion;
            if (c == 1)
            {
                txtInformacion.ForeColor = Color.Red;
            }
        }

        static public bool bandera = false; 
        private void btnSi_Click(object sender, EventArgs e)
        {
            if (bandera)
                FInventario.InObservaciones = txtInformacion.Text; 
            DialogResult = DialogResult.OK;
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void FMessageBoxP_Load(object sender, EventArgs e)
        {
            if (bandera)
                this.ActiveControl = txtInformacion;
            else
                this.ActiveControl = btnNo;
        }
    }
}
