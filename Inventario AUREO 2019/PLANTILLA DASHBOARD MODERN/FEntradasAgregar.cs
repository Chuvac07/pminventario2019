﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FEntradasAgregar : Form
    {
        int existenciaTotal;
        public FEntradasAgregar(string producto, int existencia)
        {
            InitializeComponent();
            lbProducto.Text = producto;
            lbExistencia.Text = Convert.ToString(existencia);
            existenciaTotal = existencia;
        }

        private void FEntradasAgregar_Load(object sender, EventArgs e)
        {
            this.ActiveControl = txtCantidad;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                int cantidad = Convert.ToInt16(txtCantidad.Text);
                if (cantidad >= 0)
                {
                 //   FEntradas.cantidadE = cantidad + existenciaTotal;
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    FMessageBox obj = new FMessageBox("Error ETR1: La cantidad debe ser mayor a 0", "¡Alerta de conexión!", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Se deben ingresar números unicamente.", "ERROR AGR1:", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel; 
        }
    }
}
