﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FClientes : Form
    {
        public FClientes()
        {
            InitializeComponent();
        }
        Conexion conect = new Conexion();
        //Agregar clientes a la base de datos
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                string idClie = lbClientes.Text; 
                string nit = txtNit.Text;
                string nombre = txtNombre.Text;
                string empresa = txtEmpresa.Text;
                string telefono = txtTelefono.Text;
                string direccion = txtDireccion.Text;
                string email = txtEmail.Text;
                bool bandera1 = false;
                bool bandera2 = false;

                if (rBEmpresa.Checked == true && nit != "" && nombre != "" && empresa != "" && telefono != "" && direccion != "")
                    bandera1 = true;
                else
                    bandera1 = false;

                if (rbCliente.Checked == true && nit != "" && nombre != "" && telefono != "" && direccion != "")
                    bandera2 = true;
                else
                    bandera2 = false;

                if (bandera1 || bandera2)
                {

                    if (rbCliente.Checked == true)
                    {
                        conect.ingresarClientes(idClie, nit, nombre, nombre, "Cliente", telefono, direccion, email);
                        conect.actualizarClientes(dgvClientes, "Cliente");
                    }
                    if (rBEmpresa.Checked == true)
                    {
                        conect.ingresarClientes(idClie, nit, nombre, empresa, "Empresa", telefono, direccion, email);
                        conect.actualizarClientes(dgvClientes, "Empresa");
                    }

                    txtNombre.Enabled = false;
                    txtEmpresa.Enabled = false;
                    txtNit.Enabled = false;
                    txtTelefono.Enabled = false;
                    txtDireccion.Enabled = false;
                    txtEmail.Enabled = false;

                    btnAgregar.Visible = false;
                    txtNit.Enabled = false;
                    btnContiuar.Visible = true;
                    
                    
                                    }
                else
                {
                    FMessageBox obj = new FMessageBox("Se deben ingresar todos los campos que se solicitan.", "ERROR CLI:", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CL1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void FClientes_Load(object sender, EventArgs e)
        {
            try
            {
                conect.actualizarClientes(dgvC1, "TODO");
                lbClientes.Text = Convert.ToString(conect.generarN(dgvC1));
                this.ActiveControl = txtNit;
                rBEmpresa.Checked = true;
               
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CL2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void dgvClientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnContiuar.Visible = true;
            btnAgregar.Visible = false;
            btnModificar.Visible = true;
            btnEliminar.Visible = true; 

            lbClientes.Text = dgvClientes.CurrentRow.Cells[0].Value.ToString();
            txtEmpresa.Text = dgvClientes.CurrentRow.Cells[1].Value.ToString();
            txtNombre.Text = dgvClientes.CurrentRow.Cells[2].Value.ToString();
            txtNit.Text = dgvClientes.CurrentRow.Cells[3].Value.ToString();
            txtTelefono.Text = dgvClientes.CurrentRow.Cells[5].Value.ToString();
            txtDireccion.Text = this.dgvClientes.CurrentRow.Cells[6].Value.ToString();
            txtEmail.Text = dgvClientes.CurrentRow.Cells[7].Value.ToString();
            
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string nit = txtNit.Text;
                string nombre = txtNombre.Text;
                string apellido = txtEmpresa.Text;
                string telefono = txtTelefono.Text;
                string direccion = txtDireccion.Text;
                string email = txtEmail.Text;

                if (rbCliente.Checked == true)
                {
                    conect.modificarClientes(lbClientes.Text, nit, nombre, nombre, telefono, direccion, email);
                    conect.actualizarClientes(dgvClientes, "Cliente");
                }
                if (rBEmpresa.Checked == true)
                {
                    conect.modificarClientes(lbClientes.Text, nit, nombre, apellido, telefono, direccion, email);
                    conect.actualizarClientes(dgvClientes, "Empresa");
                }
                btnAgregar.Enabled = false;
                txtNombre.Enabled = false;
                txtEmpresa.Enabled = false;
                txtNit.Enabled = false;
                txtTelefono.Enabled = false;
                txtDireccion.Enabled = false;
                txtEmail.Enabled = false;
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CL3: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void btnContiuar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text == "" && txtEmpresa.Text == "" && txtNit.Text == "" && txtTelefono.Text == "" && txtDireccion.Text == "")
            {
                FMessageBox obj = new FMessageBox("Porfavor debe llenar todos los campos para realizar la cotización.", "Error de Cotizaciones", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
            else
            {
                if (rbCliente.Checked == true)
                {
                    FSalidas.nombreCliente = txtNombre.Text;
                    FSalidas.apellidoCliente = txtNombre.Text;
                }
                if (rBEmpresa.Checked == true)
                {
                    FSalidas.nombreCliente = txtNombre.Text;
                    FSalidas.apellidoCliente = txtEmpresa.Text;
                }
                FSalidas.idCliente = lbClientes.Text;
                FSalidas.nitCliente = txtNit.Text;
                FSalidas.telefonoCliente = txtTelefono.Text;
                FSalidas.emailCliente = txtEmail.Text;
                Form1.acceso = 2;
            }
        }

        private void rbCliente_CheckedChanged(object sender, EventArgs e)
        {
            txtNit.Enabled = true;
            conect.actualizarClientes(dgvClientes, "Cliente");
            dgvClientes.Columns[1].Visible = false;
            dgvClientes.Columns[4].Visible = false;

            lbEmpresa.Visible = false;
            txtEmpresa.Visible = false;
            btnAgregar.Visible = true; 
            btnEliminar.Visible = false;
            btnModificar.Visible = false;
            btnContiuar.Visible = false;

            groupBox1.Text = "Datos del Cliente";
            groupBox1.Visible = true;
            dgvClientes.Visible = true;
            txtNombre.Clear();
            txtEmpresa.Clear();
            txtTelefono.Clear();
            txtDireccion.Clear();
            txtNit.Clear();
            txtEmail.Clear();
            this.ActiveControl = txtNit;
        }

        private void rBEmpresa_CheckedChanged(object sender, EventArgs e)
        {
            txtNit.Enabled = true;
            conect.actualizarClientes(dgvClientes, "Empresa");
            dgvClientes.Columns[1].Visible = true;
            dgvClientes.Columns[4].Visible = false;

            lbEmpresa.Visible = true;
            txtEmpresa.Visible = true;
            btnAgregar.Visible = true;
            btnEliminar.Visible = false;
            btnModificar.Visible = false;
            btnContiuar.Visible = false; 

            groupBox1.Text = "Datos de la Empresa";
            groupBox1.Visible = true;
            dgvClientes.Visible = true;
            txtNombre.Clear();
            txtEmpresa.Clear();
            txtTelefono.Clear();
            txtDireccion.Clear();
            txtNit.Clear();
            txtEmail.Clear();
            this.ActiveControl = txtNit;
        }

        private void txtNombre_KeyUp(object sender, KeyEventArgs e)
        {
            if (rbCliente.Checked == true)
            {
                conect.busqueda(dgvClientes, txtNombre, "nombre_clientespm", "Cliente");
            }
            if (rBEmpresa.Checked == true)
            {
                conect.busqueda(dgvClientes, txtNombre, "nombre_clientespm", "Empresa");
            }
        }

        private void txtNit_KeyUp(object sender, KeyEventArgs e)
        {
            if (rbCliente.Checked == true)
            {
                conect.busqueda(dgvClientes, txtNit, "nit_clientespm", "Cliente");
            }
            if (rBEmpresa.Checked == true)
            {
                conect.busqueda(dgvClientes, txtNit, "nit_clientespm", "Empresa");
            }
        }

        private void txtEmpresa_KeyUp(object sender, KeyEventArgs e)
        {
            if (rbCliente.Checked == true)
            {
                conect.busqueda(dgvClientes, txtEmpresa, "apellido_clientespm", "Cliente");
            }
            if (rBEmpresa.Checked == true)
            {
                conect.busqueda(dgvClientes, txtEmpresa, "apellido_clientespm", "Empresa");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                FMessageBoxP obj = new FMessageBoxP("¿Está seguro en eliminar el producto seleccionado?", "¡Alerta de eliminación!", 1);
                if (obj.ShowDialog() == DialogResult.OK)
                {
                    string id = lbClientes.Text;
                    conect.eliminarClientes(id);
                    if (rbCliente.Checked == true)
                        conect.actualizarClientes(dgvClientes, "Cliente");
                    else
                        conect.actualizarClientes(dgvClientes, "Empresa");
                    txtNit.Clear();
                    txtEmpresa.Clear();
                    txtTelefono.Clear();
                    txtNombre.Clear();
                    txtDireccion.Clear();
                    txtEmail.Clear();
                    lbClientes.Text = "0";

                    btnEliminar.Visible = false;
                    btnModificar.Visible = false;
                    btnContiuar.Visible = false;
                    btnAgregar.Visible = true;
                }
               
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Verifique el dato a eliminar", "¡Error al Eliminar!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
    }
}
