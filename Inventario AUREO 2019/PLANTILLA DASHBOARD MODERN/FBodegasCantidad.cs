﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLANTILLA_DASHBOARD_MODERN
{
    public partial class FBodegasCantidad : Form
    {
        public FBodegasCantidad(string descripcion, string existencia)
        {
            InitializeComponent();
            lbInfo.Text = descripcion;
            lbexistencia.Text = "de " + existencia; 
            nudCantidad.Minimum = 0;
            nudCantidad.Maximum = Convert.ToInt16(existencia);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            FBodegas.Bcantidad = nudCantidad.Value.ToString();
            DialogResult = DialogResult.OK;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void FBodegasCantidad_Load(object sender, EventArgs e)
        {
            this.ActiveControl = nudCantidad;
        }
    }
}
