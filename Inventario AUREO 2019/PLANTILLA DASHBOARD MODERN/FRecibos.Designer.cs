﻿namespace PLANTILLA_DASHBOARD_MODERN
{
    partial class FRecibos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRecibos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbrecibo2 = new System.Windows.Forms.GroupBox();
            this.bunifuGradientPanel4 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.txtvobo2 = new System.Windows.Forms.TextBox();
            this.txtobservaciones2 = new System.Windows.Forms.TextBox();
            this.txtpor2 = new System.Windows.Forms.TextBox();
            this.txtcantidadde2 = new System.Windows.Forms.TextBox();
            this.txtlugar2 = new System.Windows.Forms.TextBox();
            this.txtrecibide2 = new System.Windows.Forms.TextBox();
            this.dgvRecibo2 = new System.Windows.Forms.DataGridView();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lbnumero = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.dtFecha2 = new System.Windows.Forms.DateTimePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btnrecibocaja = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.errorP1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFecha1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbtotal = new System.Windows.Forms.Label();
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbtotalcheque = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbcorrelativo = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.dgvRecibo1 = new System.Windows.Forms.DataGridView();
            this.gbrecibocaja = new System.Windows.Forms.GroupBox();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.txtvalorcheque = new System.Windows.Forms.TextBox();
            this.txtCheque = new System.Windows.Forms.TextBox();
            this.txtBanco = new System.Windows.Forms.TextBox();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.txtFactura = new System.Windows.Forms.TextBox();
            this.txtpor = new System.Windows.Forms.TextBox();
            this.txtCantidadde1 = new System.Windows.Forms.TextBox();
            this.txtRecibide1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button6 = new System.Windows.Forms.Button();
            this.gbrecibo2.SuspendLayout();
            this.bunifuGradientPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecibo2)).BeginInit();
            this.bunifuGradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorP1)).BeginInit();
            this.bunifuGradientPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecibo1)).BeginInit();
            this.gbrecibocaja.SuspendLayout();
            this.bunifuGradientPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // gbrecibo2
            // 
            this.gbrecibo2.Controls.Add(this.bunifuGradientPanel4);
            this.gbrecibo2.Controls.Add(this.txtvobo2);
            this.gbrecibo2.Controls.Add(this.txtobservaciones2);
            this.gbrecibo2.Controls.Add(this.txtpor2);
            this.gbrecibo2.Controls.Add(this.txtcantidadde2);
            this.gbrecibo2.Controls.Add(this.txtlugar2);
            this.gbrecibo2.Controls.Add(this.txtrecibide2);
            this.gbrecibo2.Controls.Add(this.dgvRecibo2);
            this.gbrecibo2.Controls.Add(this.label28);
            this.gbrecibo2.Controls.Add(this.label27);
            this.gbrecibo2.Controls.Add(this.label22);
            this.gbrecibo2.Controls.Add(this.label23);
            this.gbrecibo2.Controls.Add(this.lbnumero);
            this.gbrecibo2.Controls.Add(this.label26);
            this.gbrecibo2.Controls.Add(this.dtFecha2);
            this.gbrecibo2.Controls.Add(this.label39);
            this.gbrecibo2.Controls.Add(this.label40);
            this.gbrecibo2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbrecibo2.ForeColor = System.Drawing.Color.White;
            this.gbrecibo2.Location = new System.Drawing.Point(278, 13);
            this.gbrecibo2.Name = "gbrecibo2";
            this.gbrecibo2.Size = new System.Drawing.Size(601, 401);
            this.gbrecibo2.TabIndex = 72;
            this.gbrecibo2.TabStop = false;
            this.gbrecibo2.Text = "Recibo Interno";
            this.gbrecibo2.Visible = false;
            // 
            // bunifuGradientPanel4
            // 
            this.bunifuGradientPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel4.BackgroundImage")));
            this.bunifuGradientPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel4.Controls.Add(this.label15);
            this.bunifuGradientPanel4.Controls.Add(this.pictureBox4);
            this.bunifuGradientPanel4.Controls.Add(this.button3);
            this.bunifuGradientPanel4.Controls.Add(this.label21);
            this.bunifuGradientPanel4.GradientBottomLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel4.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel4.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel4.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel4.Location = new System.Drawing.Point(1, 14);
            this.bunifuGradientPanel4.Margin = new System.Windows.Forms.Padding(2);
            this.bunifuGradientPanel4.Name = "bunifuGradientPanel4";
            this.bunifuGradientPanel4.Quality = 10;
            this.bunifuGradientPanel4.Size = new System.Drawing.Size(599, 31);
            this.bunifuGradientPanel4.TabIndex = 86;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(953, 46);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 16);
            this.label15.TabIndex = 19;
            this.label15.Text = "Usuario";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::PLANTILLA_DASHBOARD_MODERN.Properties.Resources.inicio;
            this.pictureBox4.Location = new System.Drawing.Point(956, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(38, 34);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 18;
            this.pictureBox4.TabStop = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(810, 10);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(125, 44);
            this.button3.TabIndex = 17;
            this.button3.Text = "RECIBOS";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(179, 5);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(260, 24);
            this.label21.TabIndex = 4;
            this.label21.Text = "DATOS RECIBO INTERNO";
            // 
            // txtvobo2
            // 
            this.txtvobo2.Location = new System.Drawing.Point(144, 326);
            this.txtvobo2.Name = "txtvobo2";
            this.txtvobo2.Size = new System.Drawing.Size(308, 22);
            this.txtvobo2.TabIndex = 85;
            // 
            // txtobservaciones2
            // 
            this.txtobservaciones2.Location = new System.Drawing.Point(144, 285);
            this.txtobservaciones2.Name = "txtobservaciones2";
            this.txtobservaciones2.Size = new System.Drawing.Size(308, 22);
            this.txtobservaciones2.TabIndex = 84;
            // 
            // txtpor2
            // 
            this.txtpor2.Location = new System.Drawing.Point(145, 195);
            this.txtpor2.Multiline = true;
            this.txtpor2.Name = "txtpor2";
            this.txtpor2.Size = new System.Drawing.Size(308, 71);
            this.txtpor2.TabIndex = 83;
            // 
            // txtcantidadde2
            // 
            this.txtcantidadde2.Location = new System.Drawing.Point(144, 153);
            this.txtcantidadde2.Name = "txtcantidadde2";
            this.txtcantidadde2.Size = new System.Drawing.Size(308, 22);
            this.txtcantidadde2.TabIndex = 82;
            // 
            // txtlugar2
            // 
            this.txtlugar2.Location = new System.Drawing.Point(145, 111);
            this.txtlugar2.Name = "txtlugar2";
            this.txtlugar2.Size = new System.Drawing.Size(308, 22);
            this.txtlugar2.TabIndex = 81;
            // 
            // txtrecibide2
            // 
            this.txtrecibide2.Location = new System.Drawing.Point(145, 72);
            this.txtrecibide2.Name = "txtrecibide2";
            this.txtrecibide2.Size = new System.Drawing.Size(308, 22);
            this.txtrecibide2.TabIndex = 80;
            // 
            // dgvRecibo2
            // 
            this.dgvRecibo2.AllowUserToAddRows = false;
            this.dgvRecibo2.AllowUserToDeleteRows = false;
            this.dgvRecibo2.AllowUserToOrderColumns = true;
            this.dgvRecibo2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvRecibo2.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvRecibo2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvRecibo2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvRecibo2.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRecibo2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvRecibo2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecibo2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvRecibo2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvRecibo2.EnableHeadersVisualStyles = false;
            this.dgvRecibo2.GridColor = System.Drawing.Color.Black;
            this.dgvRecibo2.Location = new System.Drawing.Point(523, 99);
            this.dgvRecibo2.MultiSelect = false;
            this.dgvRecibo2.Name = "dgvRecibo2";
            this.dgvRecibo2.ReadOnly = true;
            this.dgvRecibo2.RowHeadersVisible = false;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            this.dgvRecibo2.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvRecibo2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecibo2.Size = new System.Drawing.Size(38, 34);
            this.dgvRecibo2.TabIndex = 79;
            this.dgvRecibo2.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(23, 329);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(66, 16);
            this.label28.TabIndex = 76;
            this.label28.Text = "Vo. Bo. :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(21, 290);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(115, 13);
            this.label27.TabIndex = 74;
            this.label27.Text = "OBSERVACIONES:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(14, 114);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 16);
            this.label22.TabIndex = 70;
            this.label22.Text = "LUGAR:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(14, 193);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 16);
            this.label23.TabIndex = 4;
            this.label23.Text = "POR:";
            // 
            // lbnumero
            // 
            this.lbnumero.AutoSize = true;
            this.lbnumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnumero.ForeColor = System.Drawing.Color.Aqua;
            this.lbnumero.Location = new System.Drawing.Point(545, 49);
            this.lbnumero.Name = "lbnumero";
            this.lbnumero.Size = new System.Drawing.Size(24, 16);
            this.lbnumero.TabIndex = 68;
            this.lbnumero.Text = "00";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Aqua;
            this.label26.Location = new System.Drawing.Point(461, 49);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 16);
            this.label26.TabIndex = 50;
            this.label26.Text = "Recibo No.";
            // 
            // dtFecha2
            // 
            this.dtFecha2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFecha2.Location = new System.Drawing.Point(472, 70);
            this.dtFecha2.Name = "dtFecha2";
            this.dtFecha2.Size = new System.Drawing.Size(89, 22);
            this.dtFecha2.TabIndex = 32;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(14, 156);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(133, 16);
            this.label39.TabIndex = 3;
            this.label39.Text = "CANTIDAD DE: Q.";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(14, 75);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(86, 16);
            this.label40.TabIndex = 1;
            this.label40.Text = "RECIBÍ DE:";
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.btnrecibocaja);
            this.bunifuGradientPanel1.Controls.Add(this.btnFinalizar);
            this.bunifuGradientPanel1.Controls.Add(this.button1);
            this.bunifuGradientPanel1.Controls.Add(this.pictureBox1);
            this.bunifuGradientPanel1.Controls.Add(this.label1);
            this.bunifuGradientPanel1.Controls.Add(this.label4);
            this.bunifuGradientPanel1.Controls.Add(this.pictureBox5);
            this.bunifuGradientPanel1.Controls.Add(this.button2);
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(-1, -1);
            this.bunifuGradientPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(125, 528);
            this.bunifuGradientPanel1.TabIndex = 6;
            // 
            // btnrecibocaja
            // 
            this.btnrecibocaja.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.btnrecibocaja.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btnrecibocaja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnrecibocaja.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnrecibocaja.ForeColor = System.Drawing.Color.White;
            this.btnrecibocaja.Image = global::PLANTILLA_DASHBOARD_MODERN.Properties.Resources.continuar2;
            this.btnrecibocaja.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnrecibocaja.Location = new System.Drawing.Point(3, 176);
            this.btnrecibocaja.Name = "btnrecibocaja";
            this.btnrecibocaja.Size = new System.Drawing.Size(118, 60);
            this.btnrecibocaja.TabIndex = 52;
            this.btnrecibocaja.Text = "RECIBO 1";
            this.btnrecibocaja.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnrecibocaja.UseVisualStyleBackColor = false;
            this.btnrecibocaja.Click += new System.EventHandler(this.btnrecibocaja_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.btnFinalizar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(22)))), ((int)(((byte)(185)))));
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizar.ForeColor = System.Drawing.Color.White;
            this.btnFinalizar.Image = global::PLANTILLA_DASHBOARD_MODERN.Properties.Resources.archivo_pdf;
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFinalizar.Location = new System.Drawing.Point(3, 340);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(118, 60);
            this.btnFinalizar.TabIndex = 29;
            this.btnFinalizar.Text = "FINALIZAR";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFinalizar.UseVisualStyleBackColor = false;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::PLANTILLA_DASHBOARD_MODERN.Properties.Resources.continuar2;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.Location = new System.Drawing.Point(3, 249);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 60);
            this.button1.TabIndex = 28;
            this.button1.Text = "RECIBO 2";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::PLANTILLA_DASHBOARD_MODERN.Properties.Resources.recibo2;
            this.pictureBox1.Location = new System.Drawing.Point(12, 60);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 80);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(23, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 20;
            this.label1.Text = "RECIBOS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(953, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Usuario";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Image = global::PLANTILLA_DASHBOARD_MODERN.Properties.Resources.inicio;
            this.pictureBox5.Location = new System.Drawing.Point(956, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(38, 34);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 18;
            this.pictureBox5.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(810, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 44);
            this.button2.TabIndex = 17;
            this.button2.Text = "RECIBOS";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // errorP1
            // 
            this.errorP1.ContainerControl = this;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(19, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "RECIBÍ DE:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(19, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "CANTIDAD DE: Q.";
            // 
            // dtFecha1
            // 
            this.dtFecha1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFecha1.Location = new System.Drawing.Point(500, 63);
            this.dtFecha1.Name = "dtFecha1";
            this.dtFecha1.Size = new System.Drawing.Size(154, 22);
            this.dtFecha1.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(19, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 16);
            this.label3.TabIndex = 33;
            this.label3.Text = "FACTURA:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(361, 171);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 16);
            this.label8.TabIndex = 34;
            this.label8.Text = "MONTO:   Q.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(259, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 29);
            this.label9.TabIndex = 43;
            this.label9.Text = "TOTAL:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(359, 201);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 29);
            this.label10.TabIndex = 44;
            this.label10.Text = "Q.";
            // 
            // lbtotal
            // 
            this.lbtotal.AutoSize = true;
            this.lbtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtotal.ForeColor = System.Drawing.Color.White;
            this.lbtotal.Location = new System.Drawing.Point(393, 201);
            this.lbtotal.Name = "lbtotal";
            this.lbtotal.Size = new System.Drawing.Size(41, 29);
            this.lbtotal.TabIndex = 45;
            this.lbtotal.Text = "00";
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.Controls.Add(this.label13);
            this.bunifuGradientPanel3.Controls.Add(this.pictureBox2);
            this.bunifuGradientPanel3.Controls.Add(this.button4);
            this.bunifuGradientPanel3.Controls.Add(this.label14);
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(1, 235);
            this.bunifuGradientPanel3.Margin = new System.Windows.Forms.Padding(2);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(677, 31);
            this.bunifuGradientPanel3.TabIndex = 52;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(953, 46);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 16);
            this.label13.TabIndex = 19;
            this.label13.Text = "Usuario";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::PLANTILLA_DASHBOARD_MODERN.Properties.Resources.inicio;
            this.pictureBox2.Location = new System.Drawing.Point(956, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(38, 34);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(810, 10);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 44);
            this.button4.TabIndex = 17;
            this.button4.Text = "RECIBOS";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(254, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(182, 24);
            this.label14.TabIndex = 4;
            this.label14.Text = "FORMA DE PAGO";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(56, 278);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(186, 20);
            this.label19.TabIndex = 53;
            this.label19.Text = "Banco/Tarjeta de crédito:";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(268, 371);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 29);
            this.label17.TabIndex = 60;
            this.label17.Text = "TOTAL:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(362, 371);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 29);
            this.label16.TabIndex = 61;
            this.label16.Text = "Q.";
            // 
            // lbtotalcheque
            // 
            this.lbtotalcheque.AutoSize = true;
            this.lbtotalcheque.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtotalcheque.ForeColor = System.Drawing.Color.White;
            this.lbtotalcheque.Location = new System.Drawing.Point(393, 371);
            this.lbtotalcheque.Name = "lbtotalcheque";
            this.lbtotalcheque.Size = new System.Drawing.Size(41, 29);
            this.lbtotalcheque.TabIndex = 62;
            this.lbtotalcheque.Text = "00";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(15, 431);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(140, 16);
            this.label20.TabIndex = 66;
            this.label20.Text = "OBSERVACIONES:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Aquamarine;
            this.label11.Location = new System.Drawing.Point(517, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 16);
            this.label11.TabIndex = 50;
            this.label11.Text = "Recibo No.";
            // 
            // lbcorrelativo
            // 
            this.lbcorrelativo.AutoSize = true;
            this.lbcorrelativo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcorrelativo.ForeColor = System.Drawing.Color.Aquamarine;
            this.lbcorrelativo.Location = new System.Drawing.Point(601, 44);
            this.lbcorrelativo.Name = "lbcorrelativo";
            this.lbcorrelativo.Size = new System.Drawing.Size(24, 16);
            this.lbcorrelativo.TabIndex = 68;
            this.lbcorrelativo.Text = "00";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(269, 278);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(161, 20);
            this.label18.TabIndex = 71;
            this.label18.Text = "Cheque No. Voucher:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(520, 278);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(46, 20);
            this.label34.TabIndex = 81;
            this.label34.Text = "Valor";
            // 
            // dgvRecibo1
            // 
            this.dgvRecibo1.AllowUserToAddRows = false;
            this.dgvRecibo1.AllowUserToDeleteRows = false;
            this.dgvRecibo1.AllowUserToOrderColumns = true;
            this.dgvRecibo1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvRecibo1.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvRecibo1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvRecibo1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvRecibo1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRecibo1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvRecibo1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecibo1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvRecibo1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvRecibo1.EnableHeadersVisualStyles = false;
            this.dgvRecibo1.GridColor = System.Drawing.Color.Black;
            this.dgvRecibo1.Location = new System.Drawing.Point(635, 103);
            this.dgvRecibo1.MultiSelect = false;
            this.dgvRecibo1.Name = "dgvRecibo1";
            this.dgvRecibo1.ReadOnly = true;
            this.dgvRecibo1.RowHeadersVisible = false;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            this.dgvRecibo1.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvRecibo1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecibo1.Size = new System.Drawing.Size(38, 34);
            this.dgvRecibo1.TabIndex = 78;
            this.dgvRecibo1.Visible = false;
            // 
            // gbrecibocaja
            // 
            this.gbrecibocaja.Controls.Add(this.txtObservaciones);
            this.gbrecibocaja.Controls.Add(this.txtvalorcheque);
            this.gbrecibocaja.Controls.Add(this.txtCheque);
            this.gbrecibocaja.Controls.Add(this.txtBanco);
            this.gbrecibocaja.Controls.Add(this.txtValor);
            this.gbrecibocaja.Controls.Add(this.txtFactura);
            this.gbrecibocaja.Controls.Add(this.txtpor);
            this.gbrecibocaja.Controls.Add(this.txtCantidadde1);
            this.gbrecibocaja.Controls.Add(this.txtRecibide1);
            this.gbrecibocaja.Controls.Add(this.label5);
            this.gbrecibocaja.Controls.Add(this.bunifuGradientPanel2);
            this.gbrecibocaja.Controls.Add(this.dgvRecibo1);
            this.gbrecibocaja.Controls.Add(this.label34);
            this.gbrecibocaja.Controls.Add(this.label18);
            this.gbrecibocaja.Controls.Add(this.lbcorrelativo);
            this.gbrecibocaja.Controls.Add(this.label11);
            this.gbrecibocaja.Controls.Add(this.label20);
            this.gbrecibocaja.Controls.Add(this.lbtotalcheque);
            this.gbrecibocaja.Controls.Add(this.label16);
            this.gbrecibocaja.Controls.Add(this.label17);
            this.gbrecibocaja.Controls.Add(this.label19);
            this.gbrecibocaja.Controls.Add(this.bunifuGradientPanel3);
            this.gbrecibocaja.Controls.Add(this.lbtotal);
            this.gbrecibocaja.Controls.Add(this.label10);
            this.gbrecibocaja.Controls.Add(this.label9);
            this.gbrecibocaja.Controls.Add(this.label8);
            this.gbrecibocaja.Controls.Add(this.label3);
            this.gbrecibocaja.Controls.Add(this.dtFecha1);
            this.gbrecibocaja.Controls.Add(this.label2);
            this.gbrecibocaja.Controls.Add(this.label6);
            this.gbrecibocaja.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbrecibocaja.ForeColor = System.Drawing.Color.White;
            this.gbrecibocaja.Location = new System.Drawing.Point(245, 16);
            this.gbrecibocaja.Name = "gbrecibocaja";
            this.gbrecibocaja.Size = new System.Drawing.Size(679, 466);
            this.gbrecibocaja.TabIndex = 49;
            this.gbrecibocaja.TabStop = false;
            this.gbrecibocaja.Text = "Recibo de Caja";
            this.gbrecibocaja.Visible = false;
            this.gbrecibocaja.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Location = new System.Drawing.Point(161, 428);
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(493, 22);
            this.txtObservaciones.TabIndex = 129;
            // 
            // txtvalorcheque
            // 
            this.txtvalorcheque.Location = new System.Drawing.Point(475, 306);
            this.txtvalorcheque.Name = "txtvalorcheque";
            this.txtvalorcheque.Size = new System.Drawing.Size(142, 22);
            this.txtvalorcheque.TabIndex = 128;
            // 
            // txtCheque
            // 
            this.txtCheque.Location = new System.Drawing.Point(276, 306);
            this.txtCheque.Name = "txtCheque";
            this.txtCheque.Size = new System.Drawing.Size(142, 22);
            this.txtCheque.TabIndex = 127;
            // 
            // txtBanco
            // 
            this.txtBanco.Location = new System.Drawing.Point(76, 306);
            this.txtBanco.Name = "txtBanco";
            this.txtBanco.Size = new System.Drawing.Size(142, 22);
            this.txtBanco.TabIndex = 126;
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(475, 165);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(142, 22);
            this.txtValor.TabIndex = 125;
            // 
            // txtFactura
            // 
            this.txtFactura.Location = new System.Drawing.Point(148, 165);
            this.txtFactura.Name = "txtFactura";
            this.txtFactura.Size = new System.Drawing.Size(142, 22);
            this.txtFactura.TabIndex = 124;
            // 
            // txtpor
            // 
            this.txtpor.Location = new System.Drawing.Point(149, 128);
            this.txtpor.Name = "txtpor";
            this.txtpor.Size = new System.Drawing.Size(292, 22);
            this.txtpor.TabIndex = 123;
            // 
            // txtCantidadde1
            // 
            this.txtCantidadde1.Location = new System.Drawing.Point(149, 92);
            this.txtCantidadde1.Name = "txtCantidadde1";
            this.txtCantidadde1.Size = new System.Drawing.Size(292, 22);
            this.txtCantidadde1.TabIndex = 122;
            // 
            // txtRecibide1
            // 
            this.txtRecibide1.Location = new System.Drawing.Point(148, 55);
            this.txtRecibide1.Name = "txtRecibide1";
            this.txtRecibide1.Size = new System.Drawing.Size(293, 22);
            this.txtRecibide1.TabIndex = 121;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(19, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "POR:";
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.label12);
            this.bunifuGradientPanel2.Controls.Add(this.label7);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox3);
            this.bunifuGradientPanel2.Controls.Add(this.button6);
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.Black;
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(1, 14);
            this.bunifuGradientPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(677, 30);
            this.bunifuGradientPanel2.TabIndex = 114;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(220, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(254, 24);
            this.label12.TabIndex = 20;
            this.label12.Text = "DATOS RECIBO DE CAJA";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(953, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 16);
            this.label7.TabIndex = 19;
            this.label7.Text = "Usuario";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::PLANTILLA_DASHBOARD_MODERN.Properties.Resources.inicio;
            this.pictureBox3.Location = new System.Drawing.Point(956, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(38, 34);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 18;
            this.pictureBox3.TabStop = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Transparent;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(810, 10);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(125, 44);
            this.button6.TabIndex = 17;
            this.button6.Text = "RECIBOS";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // FRecibos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(60)))), ((int)(((byte)(75)))));
            this.ClientSize = new System.Drawing.Size(1040, 528);
            this.Controls.Add(this.gbrecibo2);
            this.Controls.Add(this.gbrecibocaja);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FRecibos";
            this.Text = "FRecibos";
            this.Load += new System.EventHandler(this.FRecibos_Load);
            this.gbrecibo2.ResumeLayout(false);
            this.gbrecibo2.PerformLayout();
            this.bunifuGradientPanel4.ResumeLayout(false);
            this.bunifuGradientPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecibo2)).EndInit();
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorP1)).EndInit();
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.bunifuGradientPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecibo1)).EndInit();
            this.gbrecibocaja.ResumeLayout(false);
            this.gbrecibocaja.PerformLayout();
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btnrecibocaja;
        private System.Windows.Forms.GroupBox gbrecibo2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lbnumero;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DateTimePicker dtFecha2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ErrorProvider errorP1;
        private System.Windows.Forms.DataGridView dgvRecibo2;
        private System.Windows.Forms.TextBox txtvobo2;
        private System.Windows.Forms.TextBox txtobservaciones2;
        private System.Windows.Forms.TextBox txtpor2;
        private System.Windows.Forms.TextBox txtcantidadde2;
        private System.Windows.Forms.TextBox txtlugar2;
        private System.Windows.Forms.TextBox txtrecibide2;
        private System.Windows.Forms.GroupBox gbrecibocaja;
        private System.Windows.Forms.DataGridView dgvRecibo1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lbcorrelativo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lbtotalcheque;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbtotal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtFecha1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.TextBox txtvalorcheque;
        private System.Windows.Forms.TextBox txtCheque;
        private System.Windows.Forms.TextBox txtBanco;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.TextBox txtFactura;
        private System.Windows.Forms.TextBox txtpor;
        private System.Windows.Forms.TextBox txtCantidadde1;
        private System.Windows.Forms.TextBox txtRecibide1;
        private System.Windows.Forms.Label label12;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label21;
    }
}