USE [master]
GO
/****** Object:  Database [ProyectosMultiples]    Script Date: 28/06/2019 17:29:52 ******/
CREATE DATABASE [ProyectosMultiples]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ProyectosMultiples', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLFER\MSSQL\DATA\ProyectosMultiples.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ProyectosMultiples_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLFER\MSSQL\DATA\ProyectosMultiples_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ProyectosMultiples] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ProyectosMultiples].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ProyectosMultiples] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProyectosMultiples] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProyectosMultiples] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ProyectosMultiples] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProyectosMultiples] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET RECOVERY FULL 
GO
ALTER DATABASE [ProyectosMultiples] SET  MULTI_USER 
GO
ALTER DATABASE [ProyectosMultiples] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProyectosMultiples] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProyectosMultiples] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProyectosMultiples] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ProyectosMultiples] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ProyectosMultiples', N'ON'
GO
USE [ProyectosMultiples]
GO
/****** Object:  Table [dbo].[ALERTASPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ALERTASPM](
	[id_alertaspm] [int] IDENTITY(1,1) NOT NULL,
	[fecha_alertaspm] [datetime] NOT NULL,
	[descripcion_alertaspm] [varchar](300) NOT NULL,
	[estado_alertaspm] [int] NOT NULL,
	[CLIENTESPM_id_clientespm] [int] NOT NULL,
 CONSTRAINT [PK_ALERTASPM] PRIMARY KEY CLUSTERED 
(
	[id_alertaspm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CLIENTESPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CLIENTESPM](
	[id_clientespm] [int] NOT NULL,
	[nit_clientespm] [varchar](20) NOT NULL,
	[nombre_clientespm] [varchar](75) NOT NULL,
	[apellido_clientespm] [varchar](75) NOT NULL,
	[tipo_clientepm] [varchar](50) NOT NULL,
	[telefono_clientespm] [varchar](12) NOT NULL,
	[direccion_clientespm] [varchar](75) NOT NULL,
	[email_clientespm] [varchar](75) NULL,
 CONSTRAINT [PK_CLIENTESPM] PRIMARY KEY CLUSTERED 
(
	[id_clientespm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COLABORADORESPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COLABORADORESPM](
	[id_colaboradorespm] [varchar](25) NOT NULL,
	[nombre_colaboradorespm] [varchar](75) NOT NULL,
	[dpi_colaboradoresi] [varchar](50) NOT NULL,
	[telefono_colaboradorespm] [varchar](12) NOT NULL,
	[direccion_colaboradorespm] [varchar](75) NOT NULL,
	[email_colaboradorespm] [varchar](75) NULL,
 CONSTRAINT [PK_COLABORADORESPM] PRIMARY KEY CLUSTERED 
(
	[id_colaboradorespm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COTIZACIONESPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COTIZACIONESPM](
	[id_cotizacionespm] [int] IDENTITY(1,1) NOT NULL,
	[fecha_cotizacionespm] [datetime] NOT NULL,
	[estado_cotizacionespm] [varchar](50) NOT NULL,
	[condiciones_cotizacionespm] [varchar](100) NULL,
	[tiempo_cotizacionespm] [varchar](100) NULL,
	[observaciones_cotizacionespm] [varchar](250) NULL,
	[fechafuturo_cotizacionespm] [datetime] NOT NULL,
	[total_cotizacionespm] [decimal](18, 3) NOT NULL,
	[USUARIOSPM_id_usuariospm] [int] NOT NULL,
	[CLIENTESPM_id_clientespm] [int] NOT NULL,
 CONSTRAINT [PK_COTIZACIONESPM] PRIMARY KEY CLUSTERED 
(
	[id_cotizacionespm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ENTRADASPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ENTRADASPM](
	[Id_entradaspm] [int] NOT NULL,
	[total_entradaspm] [decimal](18, 2) NOT NULL,
	[pago_entradaspm] [decimal](18, 2) NOT NULL,
	[formaspago_entradaspm] [varchar](150) NOT NULL,
	[fecha_entradaspm] [datetime] NOT NULL,
	[PROVEEDORESPM_Id_proveedorespm] [int] NULL,
 CONSTRAINT [PK_ENTRADASPM] PRIMARY KEY CLUSTERED 
(
	[Id_entradaspm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HISTORIALPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTORIALPM](
	[id_historialpm] [int] IDENTITY(1,1) NOT NULL,
	[fecha_historialpm] [datetime] NOT NULL,
	[descripcion_historialpm] [varchar](300) NOT NULL,
	[tipo_historialpm] [varchar](50) NOT NULL,
	[fechainicio_historialpm] [varchar](50) NOT NULL,
	[fechafinal_historialpm] [varchar](50) NOT NULL,
	[CLIENTESPM_id_clientespm] [int] NOT NULL,
	[COLABORADORESPM_id_colaboradorespm] [varchar](25) NOT NULL,
	[correlativo_historialpm] [int] NOT NULL,
 CONSTRAINT [PK_HISTORIALPM] PRIMARY KEY CLUSTERED 
(
	[id_historialpm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[INVENTARIOPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INVENTARIOPM](
	[Id_inventariopm] [int] NOT NULL,
	[codigo_inventariopm] [varchar](50) NOT NULL,
	[descripcion_inventariopm] [varchar](300) NOT NULL,
	[categorias_inventariopm] [varchar](50) NOT NULL,
	[marca_inventariopm] [varchar](50) NOT NULL,
	[existencia_inventariopm] [int] NOT NULL,
	[precioR_inventariopm] [decimal](18, 2) NOT NULL,
	[precioV_inventariopm] [decimal](18, 2) NOT NULL,
	[precioV1_inventariopm] [decimal](18, 2) NOT NULL,
	[precioV2_inventariopm] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_INVENTARIOPM] PRIMARY KEY CLUSTERED 
(
	[Id_inventariopm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PRODUCTOPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PRODUCTOPM](
	[id_productopm] [int] IDENTITY(1,1) NOT NULL,
	[cantidad_productopm] [int] NOT NULL,
	[descripcion_productopm] [varchar](300) NOT NULL,
	[precio_productopm] [decimal](18, 3) NOT NULL,
	[subtotal_productopm] [decimal](18, 3) NOT NULL,
	[COTIZACIONESPM_id_cotizacionespm] [int] NOT NULL,
 CONSTRAINT [PK_PRODUCTOPM] PRIMARY KEY CLUSTERED 
(
	[id_productopm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PRODUCTOPME]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCTOPME](
	[ENTRADASPM_Id_entradaspm] [int] NOT NULL,
	[INVENTARIOPM_Id_inventariopm] [int] NULL,
	[cantidad_productopme] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PRODUCTOPMS]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCTOPMS](
	[SALIDASPM_Id_salidaspm] [int] NOT NULL,
	[INVENTARIOPM_Id_inventariopm] [int] NULL,
	[cantidad_productopms] [int] NOT NULL,
	[precio_productopms] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PROVEEDORESPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PROVEEDORESPM](
	[id_proveedorespm] [int] NOT NULL,
	[nit_proveedorespm] [varchar](20) NOT NULL,
	[nombre_proveedorespm] [varchar](100) NOT NULL,
	[telefono_proveedorespm] [varchar](12) NOT NULL,
	[contacto_proveedorespm] [varchar](300) NOT NULL,
	[direccion_proveedorespm] [varchar](100) NOT NULL,
	[email_proveedorespm] [varchar](100) NOT NULL,
 CONSTRAINT [PK_PROVEEDORESPM] PRIMARY KEY CLUSTERED 
(
	[id_proveedorespm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RECIBO1PM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RECIBO1PM](
	[Id_recibo1pm] [int] IDENTITY(1,1) NOT NULL,
	[nombre_recibo1pm] [varchar](100) NOT NULL,
	[direccion_recibo1pm] [varchar](100) NOT NULL,
	[cantidad_recibo1pm] [int] NOT NULL,
	[descripcion_recibo1pm] [varchar](300) NOT NULL,
	[total_recibo1pm] [decimal](18, 3) NOT NULL,
	[lugar_recibo1pm] [varchar](100) NOT NULL,
	[orden_recibo1pm] [varchar](50) NOT NULL,
	[fecha_recibo1pm] [datetime] NOT NULL,
 CONSTRAINT [PK_RECIBO1PM] PRIMARY KEY CLUSTERED 
(
	[Id_recibo1pm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RECIBO2PM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RECIBO2PM](
	[Id_recibo2pm] [int] NOT NULL,
	[nombre_recibo2pm] [varchar](100) NOT NULL,
	[cantidad_recibo2pm] [int] NOT NULL,
	[tipo_recibo2pm] [varchar](75) NOT NULL,
	[factura_recibo2pm] [varchar](100) NOT NULL,
	[fpago_recibo2pm] [varchar](100) NOT NULL,
	[observaciones_recibo2pm] [varchar](200) NOT NULL,
	[total_recibo2pm] [decimal](18, 3) NOT NULL,
	[fecha_recibo2pm] [datetime] NOT NULL,
 CONSTRAINT [PK_RECIBO2PM] PRIMARY KEY CLUSTERED 
(
	[Id_recibo2pm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SALIDASPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SALIDASPM](
	[Id_salidaspm] [int] NOT NULL,
	[total_salidaspm] [decimal](18, 2) NOT NULL,
	[fecha_salidaspm] [datetime] NOT NULL,
	[CLIENTESPM_Id_clientespm] [int] NULL,
 CONSTRAINT [PK_SALIDASPM] PRIMARY KEY CLUSTERED 
(
	[Id_salidaspm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[USUARIOSPM]    Script Date: 28/06/2019 17:29:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USUARIOSPM](
	[Id_usuariospm] [int] NOT NULL,
	[nombre_usuariospm] [varchar](50) NOT NULL,
	[dpi_usuariospm] [varchar](25) NOT NULL,
	[telefono_usuariospm] [varchar](50) NOT NULL,
	[direccion_usuariospm] [varchar](100) NULL,
	[email_usuariospm] [varchar](100) NULL,
	[permiso_usuariospm] [varchar](50) NOT NULL,
	[usuario_usuariospm] [varchar](50) NOT NULL,
	[contra_usuariospm] [varchar](50) NOT NULL,
 CONSTRAINT [PK_USUARIOSPM] PRIMARY KEY CLUSTERED 
(
	[Id_usuariospm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ALERTASPM]  WITH CHECK ADD  CONSTRAINT [FK_ALERTASPM_CLIENTESPM1] FOREIGN KEY([CLIENTESPM_id_clientespm])
REFERENCES [dbo].[CLIENTESPM] ([id_clientespm])
GO
ALTER TABLE [dbo].[ALERTASPM] CHECK CONSTRAINT [FK_ALERTASPM_CLIENTESPM1]
GO
ALTER TABLE [dbo].[COTIZACIONESPM]  WITH CHECK ADD  CONSTRAINT [FK_COTIZACIONESPM_CLIENTESPM1] FOREIGN KEY([CLIENTESPM_id_clientespm])
REFERENCES [dbo].[CLIENTESPM] ([id_clientespm])
GO
ALTER TABLE [dbo].[COTIZACIONESPM] CHECK CONSTRAINT [FK_COTIZACIONESPM_CLIENTESPM1]
GO
ALTER TABLE [dbo].[COTIZACIONESPM]  WITH CHECK ADD  CONSTRAINT [FK_COTIZACIONESPM_USUARIOSPM] FOREIGN KEY([USUARIOSPM_id_usuariospm])
REFERENCES [dbo].[USUARIOSPM] ([Id_usuariospm])
GO
ALTER TABLE [dbo].[COTIZACIONESPM] CHECK CONSTRAINT [FK_COTIZACIONESPM_USUARIOSPM]
GO
ALTER TABLE [dbo].[ENTRADASPM]  WITH CHECK ADD  CONSTRAINT [FK_ENTRADASPM_PROVEEDORESPM] FOREIGN KEY([PROVEEDORESPM_Id_proveedorespm])
REFERENCES [dbo].[PROVEEDORESPM] ([id_proveedorespm])
GO
ALTER TABLE [dbo].[ENTRADASPM] CHECK CONSTRAINT [FK_ENTRADASPM_PROVEEDORESPM]
GO
ALTER TABLE [dbo].[HISTORIALPM]  WITH CHECK ADD  CONSTRAINT [FK_HISTORIALPM_CLIENTESPM1] FOREIGN KEY([CLIENTESPM_id_clientespm])
REFERENCES [dbo].[CLIENTESPM] ([id_clientespm])
GO
ALTER TABLE [dbo].[HISTORIALPM] CHECK CONSTRAINT [FK_HISTORIALPM_CLIENTESPM1]
GO
ALTER TABLE [dbo].[HISTORIALPM]  WITH CHECK ADD  CONSTRAINT [FK_HISTORIALPM_COLABORADORESPM] FOREIGN KEY([COLABORADORESPM_id_colaboradorespm])
REFERENCES [dbo].[COLABORADORESPM] ([id_colaboradorespm])
GO
ALTER TABLE [dbo].[HISTORIALPM] CHECK CONSTRAINT [FK_HISTORIALPM_COLABORADORESPM]
GO
ALTER TABLE [dbo].[PRODUCTOPM]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTOPM_COTIZACIONESPM] FOREIGN KEY([COTIZACIONESPM_id_cotizacionespm])
REFERENCES [dbo].[COTIZACIONESPM] ([id_cotizacionespm])
GO
ALTER TABLE [dbo].[PRODUCTOPM] CHECK CONSTRAINT [FK_PRODUCTOPM_COTIZACIONESPM]
GO
ALTER TABLE [dbo].[PRODUCTOPME]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTOPME_ENTRADASPM] FOREIGN KEY([ENTRADASPM_Id_entradaspm])
REFERENCES [dbo].[ENTRADASPM] ([Id_entradaspm])
GO
ALTER TABLE [dbo].[PRODUCTOPME] CHECK CONSTRAINT [FK_PRODUCTOPME_ENTRADASPM]
GO
ALTER TABLE [dbo].[PRODUCTOPME]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTOPME_INVENTARIOPM] FOREIGN KEY([INVENTARIOPM_Id_inventariopm])
REFERENCES [dbo].[INVENTARIOPM] ([Id_inventariopm])
GO
ALTER TABLE [dbo].[PRODUCTOPME] CHECK CONSTRAINT [FK_PRODUCTOPME_INVENTARIOPM]
GO
ALTER TABLE [dbo].[PRODUCTOPMS]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTOPMS_INVENTARIOPM] FOREIGN KEY([INVENTARIOPM_Id_inventariopm])
REFERENCES [dbo].[INVENTARIOPM] ([Id_inventariopm])
GO
ALTER TABLE [dbo].[PRODUCTOPMS] CHECK CONSTRAINT [FK_PRODUCTOPMS_INVENTARIOPM]
GO
ALTER TABLE [dbo].[PRODUCTOPMS]  WITH CHECK ADD  CONSTRAINT [FK_PRODUCTOPMS_SALIDASPM] FOREIGN KEY([SALIDASPM_Id_salidaspm])
REFERENCES [dbo].[SALIDASPM] ([Id_salidaspm])
GO
ALTER TABLE [dbo].[PRODUCTOPMS] CHECK CONSTRAINT [FK_PRODUCTOPMS_SALIDASPM]
GO
ALTER TABLE [dbo].[SALIDASPM]  WITH CHECK ADD  CONSTRAINT [FK_SALIDASPM_CLIENTESPM] FOREIGN KEY([CLIENTESPM_Id_clientespm])
REFERENCES [dbo].[CLIENTESPM] ([id_clientespm])
GO
ALTER TABLE [dbo].[SALIDASPM] CHECK CONSTRAINT [FK_SALIDASPM_CLIENTESPM]
GO
USE [master]
GO
ALTER DATABASE [ProyectosMultiples] SET  READ_WRITE 
GO
